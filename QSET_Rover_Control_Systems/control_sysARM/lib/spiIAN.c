/*

PROTIP, DONT CHANGE THIS 

// this was the SPI library modified by Ian to work for the encoders, the issues with the encoders are likely not here 
// the only thing that might needed changed is the location of the spi dev based on the OS version and the hardware used 
 *
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

// header file to incldue this lovely transfer function
#include "spiIAN.h" 

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static void pabort(const char *s)
{
	perror(s);
	abort();
}

static const char *device = "/dev/spidev0.0";
static uint8_t mode = 0;
static uint8_t bits = 8;
static uint32_t speed = 500000;
static uint16_t delay;
int firstRun = 1; 

int fd; // make fd global 

unsigned char transferSPI(unsigned char testVal){
	// this function actually does the SPI transfer this is called from the SPI encoders function 

	
	if(firstRun==1){
		// run setup 
		firstRun++; // so it wont run again 
		setup(); // call the setup 
	}

	int ret;
	uint8_t tx[2]; 
	tx[0] = testVal; // this is the command written to the encdoers 
	tx[1] = testVal;
	//printf("%d\n", ARRAY_SIZE(tx));
	// turn the message into a sctruct with all of the information and settings 
	uint8_t rx[ARRAY_SIZE(tx)] = {0, };
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = ARRAY_SIZE(tx),
		.delay_usecs = delay,
		.speed_hz = 0,
		.bits_per_word = 0,
	};

	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr); // write the message to the device and see what is returned 
	if (ret == 1)
		pabort("can't send spi message");

	//for (ret = 0; ret < ARRAY_SIZE(tx); ret++) {
		ret =0; // our messages are simple for this case so we can just assgine ret as 0 and not parse the bugger 
		// if you are using this library for something else you may need to re parse the buffer proerly or look at the full rx array 
		//if (!(ret % 6))
		//	puts("");
		//printf("%.2X ", rx[ret]);
	//}
	//puts("");
	return rx[ret]; 
}

int setup(){

	// setup the SPI bus
	int ret = 0;
	printf("Setup SPI \n"); 
	fd = open(device, O_RDWR);
	if (fd < 0)
		pabort("can't open device");

	/*
	 * spi mode
	 */
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		pabort("can't get spi mode");

	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("can't get bits per word");

	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("can't get max speed hz");
	
}

// 

void cleanUpSPI(){
	// this function cleans up the SPI 
	close(fd); 
}

/*
int mainTes () // remove test for stand alone run 
{
	
	int fd;
	unsigned char testPass = 0x10; 

	//parse_opts(argc, argv);

	//fd = setup(fd); 

	printf("spi mode: %d\n", mode);
	printf("bits per word: %d\n", bits);
	printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);
	unsigned char testReturn; 

	testReturn = transferSPI(testPass);

	//printf("returned value %d \n",testReturn);

	//std::cout<<std::hex<<testReturn<<std::endl;
	printf("Returned: %04x \n",testReturn);
	close(fd);

	return 0;
}*/