#include <mosquittopp.h>
#include <string>
#include <functional>
#include <unordered_map>
#include <vector>

typedef void (*CallbackFunc)(std::string, std::string);

class MQTT : public mosqpp::mosquittopp {
    public:
        MQTT(const char *id, const char* host_, int port_);
        virtual ~MQTT();

        void emit(std::string topic, std::string msg);
        void on(std::string, CallbackFunc);

    private:

        void on_connect(int resp_code);
        void on_publish(int msg_id);
        void on_message(const mosquitto_message* msg);

        std::string host;
        int port;
        
        CallbackFunc callback_on_message;

        //std::unordered_map<std::string, (*std::vector<CallbackFunc>)> callbacks;

};

extern MQTT* broker;