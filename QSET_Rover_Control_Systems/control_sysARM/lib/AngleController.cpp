#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <fstream>
#include <time.h>   // time calls
#include <thread> // for multi thread 
#include <math.h>
#include "MQTTwrap/MQTT.h"
#include "json.hpp"

using namespace  std;
using json = nlohmann::json;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Linke Local Headers to Other Files 
#include "ControlSystemHeader.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//linear angle control 
float PreAngleDiff[4];
float DerErr=0.0; 
float LastDAPanAngle = 0; 

// this was the main angle controller used for URC 2018 
void linAngleControl(){
	

	// func vars, these should proably be declared outside of the function so we are not declaring them every time this is called 
	// this was comparmentalized when we were testing as there were a billion fucking version of this function 
	float CurrentAngle; 
	float DesiredAngle; 
	float LocalMaxAngle; 
	float LocalMinAngle; 
	float AngleDiff;
	float AngleGain; 
	float AngleIntegralGain; 
	for(int n=0; n<4;n++){
		CurrentAngle = EncoderAngles[n]; 
		DesiredAngle = SPAngle[n]; 
		LocalMaxAngle = MaxAngles[n][0];
		LocalMinAngle = MaxAngles[n][1]; 

	//	cout<<"Linear Control: Current Angle: "<<CurrentAngle<<" Goal Angle: "<<DesiredAngle<<endl; 
		// bound the angle just in case to make sure we are not going to an illegal position 
		// this if chain makes the angles inside the bounds of theangle and then if it needs to reasign it will reasign based on the reset angle 
		if(DesiredAngle>LocalMaxAngle){
			DesiredAngle = LocalMaxAngle;
			SPAngle[n] = DesiredAngle;
			if(n==2){
				// fix the wrist
				WristAngle = EncoderAngles[2] - EncoderAngles[0] + EncoderAngles[1]; // find the wrist angle based on the shoulder and elbow angles 
				// note the sign difference between the shoulder and the elbow as these encdoers are backwards too each other 
			}
//			cout<<JointNames[n]<<" is at max angle"<<endl;
		}else if(DesiredAngle<LocalMinAngle){
			DesiredAngle = LocalMinAngle; 
			SPAngle[n] = DesiredAngle;
			if(n==2){
				// fix the wrist 
				WristAngle = EncoderAngles[2] - EncoderAngles[0] + EncoderAngles[1]; 
			}
//			cout<<JointNames[n]<<" is at max angle"<<endl;
		}

		// deal with the current angle, it will be given a value of 0 --> 360
		// NEW COMMENT: mention this earlier since the encoder thread already does this this is old and reduandant, you can probs get rid of it 
		// we want Local Min Angle --> Local Max Angle (Yes this is greate than 360degs potentially)
		// do for all cases based on where the desired angle is 
		if(n!=3){
			// case for not the pan 
			if(DesiredAngle>180){
				// this means the desired angle is greater than 180, so we want the split to go from Local Max angle - 360 to Local Max Angle 
				if(CurrentAngle>LocalMaxAngle){
					CurrentAngle = CurrentAngle - 360; 
				}
			}else if(DesiredAngle<-180){
				// opposite of above case 
				// so we want to go from min angle to min angle + 360
				if(CurrentAngle<-180){
					CurrentAngle = CurrentAngle + 360; 
				}	
			}else{
				// normal split at 180 case 
				if(CurrentAngle>180){ 
					CurrentAngle = CurrentAngle-360; 
				}
			}
			// do seperation for backside to deal with case when the arm is hangingout back
			// for joints like the shoulder which could ahve had the posiblity to do more than +- 180 
			// the pan was the onyl one in the end that could 
			if(abs(DesiredAngle)>150){
				// near the flip point then we want everything to be 0  to 360 
				if(DesiredAngle<0){
					DesiredAngle+=360.0; // make it positive 
				}
				if(CurrentAngle<0){
					CurrentAngle+=360.0;// make it positive
				}
			}
			
			AngleDiff = DesiredAngle-CurrentAngle; // err value 
		}else{
			// case for the pan
			// this is becasue teh pan was allowed to over rotate in both directions but not do a full circle 
			if(DesiredAngle>LocalMaxAngle){
				DesiredAngle = LocalMaxAngle; 
			} else if(DesiredAngle< LocalMinAngle){
				DesiredAngle = LocalMinAngle; 
			}
			// this pan is left var is something that is set from the fucker checker and lets the control system know which way to turn the arm 
			// this was so if we were at -200 degrees it would not be reset to -160 becasue the arm had apprached this angle from the left side 

			if(PanIsLeft==1){
				// the pan is left of center 
				if(CurrentAngle>360+LocalMinAngle){
					CurrentAngle-=360.0;
				}else if(CurrentAngle < LocalMinAngle){
					CurrentAngle +=360.0; 
				}
			}else{
				// the pan is right of center 
				if(CurrentAngle<(LocalMaxAngle-360)){
					CurrentAngle += 360.0;
				}else if(CurrentAngle>LocalMaxAngle){
					CurrentAngle -= 360.0; 
				}
			}

			AngleDiff = DesiredAngle - CurrentAngle;
			if(fabs(AngleDiff)<3){
				AngleDiff = 0.0;
			}
		}

//		cout<<"Joint: "<<n<<" error: "<<AngleDiff<<endl;


		
//		cout<<"Linear Control: Current Angle: "<<CurrentAngle<<" Goal Angle: "<<DesiredAngle<<" Angle Difference: "<<AngleDiff<<endl; 
		// if the sign of the Angle Diff is different than the CumAngleDiff the Cum needs to be zeroed this was to help git rid of int terms
		// to reduce the overshoot mind you the I and D were not used in this .... since i couldnt get it tuned 
		if(CumAngleDiff[n]*AngleDiff<0){
			// its a different sign here thus 
			CumAngleDiff[n] = 0.0; 
			DerErr = 0.0; 
		}else{
			CumAngleDiff[n] += AngleDiff; // update the cumdiff 
			DerErr = (AngleDiff - PreAngleDiff[n]); 
		}
		// define clockwise as forward? Define positive degrees as forward 
		// Angle gain is equal to the reciprical of the maximum angle the joint can travel.  *** May need to scale this ***
		AngleGain =0.25;// 1.0/((float)LocalMaxAngle-(float)LocalMinAngle); 
		AngleIntegralGain = 0.0; 
		float AngleDerGain =0.0;// 0.2;
		// dead zone 
		if(abs(AngleDiff)<0.5){
			// we are at the angle dead zone so we should zero the angle DIFF 
			// the joints also have a bit of backlash that is larger thanthis 
			SPSpeed[n] = 0.0; 
		}else{
			if(n != 3){
				SPSpeed[n] = AngleDiff*AngleGain+AngleIntegralGain*CumAngleDiff[n] + DerErr * AngleDerGain; 	
			}else{
			// more oumph for pan 
				// this gave a differnt gain for the pan that was less ... the pan was weird, the backlash was seen more for it 
				// the pan is closest to the wrist becasue they have the same gearing and backlash 
				// but the wrist was also fighting against gravity, so expcet when the center of gravity of the wrist was inline with the wrist pivot
				// you did nto see the wrist backlash becasue gravity was helping for once in its fucking life 
				// #Gravity I spin in your general direction #theonlygoodphythonisapythonref
				// also everyone spits in the general direction of gravity #down 
				SPSpeed[n] = AngleDiff*0.2+0.0*CumAngleDiff[n] + DerErr * AngleDerGain; 	
			}		
		}
		if(n<3){
			// fix joints by reversing the speed 
			SPSpeed[n] = -1.0*SPSpeed[n]; // wiring stuff to make things go the right way 
		}	
//		cout<<"Linear Control: Current Angle: "<<CurrentAngle<<" Goal Angle: "<<DesiredAngle<<" Angle Difference: "<<AngleDiff<<"Wrote Speed: "<<SPSpeed[n]<<endl;
		PreAngleDiff[n] = AngleDiff; // record the previous angle diff 
	}
//	AbsEncoderChange ++; // step the value. 
//	cout<<"Wrist Angle SP: "<<SPAngle[2]<<" Wrist Angle Current: "<<EncoderAngles[2]<<endl;
	// call the speed controller to make this go
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////

// this was the thing that was used for the limite dspeed control to chekc that a joint was at limit or not 
// it looked was called from the limited speed control to see if a specific joint was at limit 
// it would only let the joitn spin away from the affending max angle 
int angleLimits(int JointIndex, int Direction){
	// this function checks to make sure a joint is not at limit 
	int AtLimit = 0; // joint is not at limit 
	float JointMax = MaxAngles[JointIndex][0];
	float JointMin = MaxAngles[JointIndex][1]; 
	float JointAngle = EncoderAngles[JointIndex];

	if(JointAngle>180){
		JointAngle -= 360.0; 
	}else if(JointAngle<-180.0){
		JointAngle += 360.0;
	}
	// if either are true we need to stop the arm
	if(JointAngle <= JointMin){
		AtLimit = 1;
		if(Direction==0){
			AtLimit = 0;
		}
	}else if(JointAngle >= JointMax){
		AtLimit = 1;
		if(Direction ==1){
			AtLimit = 0; 
		}
	}
	return AtLimit; // returns an int of 1 or 0 for at limit or not since bools are for people that write good code
}