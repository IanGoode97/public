//  this was not used but has the fancy angle controlers written by Ghani Lawal

/// HEADER

void angleControl(void); 

/// CODE 
//#include "ControlSystemHeader2.h"



// control stuff 

#define PI 3.14159265
#define C  180/PI
#define C2 PI/180

//Robot Link Legnths
#define A1 0.5
#define A2 0.5
#define A3 0.5
#define A4 0.5

//Motor Constants
#define K1 1.0297
#define K2 1.8263
#define V 12

//Feedback Gain Constaints
#define ALPHA 15
#define GAMMA 0.05
#define KAPPA 0.3


float DeadZone = 0.5;//2.0; // 1/8" at a foot ish #QSETish

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

float DeltaAngles[4]; //Difference between the EncoderAngles and SetAngles
float DeltaSpeeds[4]; //Difference between the EncoderSpeed and SetSpeeds
float InitS[4] = {0, 0, 0, 0}; //Initial Error Manifold Value
float Sd[4]; //Derivative Time varying Error Manifold Value
float I[4]; //Integral Time varying Error Manifold Value
float SampleTime = 0.05 ;//Time between reading the encoders
float SetCycles[4]= {0, 0, 0, 0}; 
float PreviousSetAngles[4]; // previous vals of setpoints 
/*
	3 --> Pan Motor  
	0 --> Shoulder 
	1 --> Elbow
	2 --> Wrist
*/

void FeedbackGains(int i){
	float Sq = 0;
	if(SPAngle[i] != PreviousSetAngles[i]){
		InitS[i] = DeltaSpeeds[i] + ALPHA*DeltaAngles[i];
		Sd[i] = InitS[i]*exp(-KAPPA*SampleTime);
		SetCycles[i] = 0;
	}
	else{
		SetCycles[i]++;
		Sd[i] = InitS[i]*exp(-KAPPA*SampleTime*SetCycles[i]);
	}

	Sq = DeltaSpeeds[i]-ALPHA*DeltaAngles[i]-Sd[i];
	if(abs(DeltaAngles[i])<DeadZone){
		I[i] =0.0;// I[i]; // something 
	}else if(Sq<0){
		I[i]--;
	}else if (Sq>0){
		I[i]++;
	}
}

void torqueCommand(){
	float Kd [4] ={15, 15 , 15, 15};
	float T;
	for(int i = 0; i<4; i++){
//		if(i ==2){
			T = -ALPHA*Kd[i]*DeltaAngles[i]-Kd[i]*(-1.0)*(DeltaSpeeds[i]-Sd[i])-GAMMA*Kd[i]*I[i];
/*			cout << " \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ " << endl;
			cout << "Torque: "  << T  << endl;
			cout << "FEEDBACK GAIN VALUES" << endl;
			cout << "Sd: " << Sd[i] << " I: "<<I[i]<< endl;
			cout<< "DELTA VALUES" << endl;
			cout<< "DELTAAGNLES: " << DeltaAngles[i] << " DELTASPEED: " << DeltaSpeeds[i] << endl;
			cout<< "Desired and Actual Angles" << endl;
			cout <<"Encoder Angle: " << EncoderAngles[i]<< " Desired Angle: " << SPAngle[i] << endl;	
*/			//if(DeltaAngles[i] > 4)
				SPSpeed[i] =(K1*V-K2*T)/(PI*1000.0);
			//else
				//SPSpeed[i] = 0;
			cout << "OutputSpeed: " << SPSpeed[i] <<"For Joint: "<<i<< endl;
//		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// angle controller 

float CurrentAngle; 
float DesiredAngle; 
float AngleDiff; 
float AngleGain ; // means that if we are off by 180 degrees we will go the max speed to get back  CHANGES BASED ON MOTORS MAX RANGE OF MOVEMNT 
float AngleIntegralGain = 0.0; 
// local min and max angles for each motor
float LocalMaxAngle; 
float LocalMinAngle; 
float EncSpeed1; 

void angleControl(){
	// angle controller 
	
	// update the abs encoders 
	
	
//	cout<<"Updated Encoder Angles ;)"<<endl;
	for(int n=0; n<4;n++){
		CurrentAngle = EncoderAngles[n]; 
		DesiredAngle = SPAngle[n]; 
		LocalMaxAngle = MaxAngles[n][0];
		LocalMinAngle = MaxAngles[n][1]; 
		//if(n==2){
		//	cout<<"|||||||||||||||||||||||||||| Current Angle: "<<CurrentAngle<<" Goal Angle: "<<DesiredAngle<<endl; 
	//	}
		// bound the angle just in case to make sure we are not going to an illegal position 
		if(DesiredAngle>LocalMaxAngle){
			DesiredAngle = LocalMaxAngle;
		}else if(DesiredAngle<LocalMinAngle){
			DesiredAngle = LocalMinAngle; 
		}
		// deal with the current angle, it will be given a value of 0 --> 360
		// we want Local Min Angle --> Local Max Angle (Yes this is greate than 360degs potentially)
		// do for all cases based on where the desired angle is 
		if(DesiredAngle>180){
			// this means the desired angle is greater than 180, so we want the split to go from Local Max angle - 360 to Local Max Angle 
			if(CurrentAngle>LocalMaxAngle){
				CurrentAngle = CurrentAngle - 360; 
			}
		}else if(DesiredAngle<-180){
			// opposite of above case 
			// so we want to go from min angle to min angle + 360
			if(CurrentAngle<-180){
				CurrentAngle = CurrentAngle + 360; 
			}	
		}else{
			// normal split at 180 case 
			if(CurrentAngle>180){
				CurrentAngle = CurrentAngle-360; 
			}
		}
		EncSpeed1 = EncoderValues[n]; 


		if(abs(DesiredAngle)>150){
			// near the flip point then we want everything to be 0  to 360 
			if(DesiredAngle<0){
				DesiredAngle+=360.0; // make it positive 
			}
			if(CurrentAngle<0){
				CurrentAngle+=360.0;// make it positive
			}
		}
		
		DeltaAngles[n] = DesiredAngle-CurrentAngle;

		if(abs(DeltaAngles[n])<DeadZone){
			DeltaAngles[n] = 0.0; 
			AtAngleDeadZone = 1; 
			DeltaSpeeds[n] = 0.0;
		}else{
			DeltaSpeeds[n] = EncSpeed1 - DeltaAngles[n]/SampleTime;
		}
		//Calculate NonLinear Feedback Gains
		FeedbackGains(n);
		PreviousSetAngles[n] = SPAngle[n];
	}
	// call the speed control 
	// set the speeds 
	torqueCommand();

	// call the speed controller to make this go 
	speedControl(); 
}