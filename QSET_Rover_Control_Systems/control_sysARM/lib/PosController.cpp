#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <fstream>
#include <time.h>   // time calls
#include <thread> // for multi thread 
#include <math.h>
#include "MQTTwrap/MQTT.h"
#include "json.hpp"

using namespace  std;
using json = nlohmann::json;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Linke Local Headers to Other Files 
#include "ControlSystemHeader.h"
//#include "ControlSystemHeader2.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//

float ZOld; 
float XOld;
// set the current value, this was done to have a non zero non null value 
/// this should get updated before anything is called to mvoe a joint 
float XCurrent =30.0; 
float ZCurrent = 30.0;

float UpperArmL = 31.0; //cm
float ForeArmL = 22.5; //cm

float WristCurrent = 0.0; // current wrist angle 
float PanCurrent = 0.0; // current pan angle 
float WristAngleMod = 0.0; // this value was indented to be changed based on the elbow and shoulder angles to keep the wrist at the same angle 
//relative to ground 

// this function is called at the begining of every control loop to give yout eh postion of the arm 

void currentArmPos(){
	// get curent angles 
	float ShoulderCurrent = EncoderAngles[0];
	float ElbowCurrent = EncoderAngles[1]; 


	// make all -180 to 180
//	cout<<"Angles: Shoulder: "<<ShoulderCurrent<< "Elbow: "<<ElbowCurrent<<" Wrist: "<<WristCurrent<<endl; 

	// the following is redudant since this is done in the abs encodersfile 
	if(ShoulderCurrent>180){
		ShoulderCurrent -=360.0; 
	}
	if(ElbowCurrent>180){
		ElbowCurrent-=360.0;
	}

	if(abs(ElbowCurrent)<1){
		// the arm is at about max reach 
		// the arm is near max reach when ever the elbow is almost straight 
		cout<<"WARNING ARM IS NEAR MAX REACH"<<endl; 
	}

	if(WristCurrent>180){
		WristCurrent-=360.0; 
	}

	// make teh shoulder current negative sicne its not followign the angle defintion 
	ShoulderCurrent *= (-1.0); // this is becuase the encoder is backwards comapred to all the others, it should be on the other side of the arm 
//	cout<<"Current Shoulder angle: "<<ShoulderCurrent<<endl; 

	WristCurrent = ShoulderCurrent + ElbowCurrent + EncoderAngles[2]; 
	PanCurrent = EncoderAngles[3]; 
	// this is not redudant as this has been chagned since it has been modified by some unholyfucks 
	if(WristCurrent>180){
		WristCurrent -= 360.0;
	}else if(WristCurrent<-180){
		WristCurrent += 360.0;
	}

	if(PanCurrent > 180){
		PanCurrent -= 360.0;
	}
	WristAngleMod = -ShoulderCurrent - ElbowCurrent;	// same as saying Wristanglemod = WirstAngle - EncoderAngles[3]; 

	// figure out the current pos of the wrist joint from the angles of the joints 

	float ElbowXCurrent; 
	float ElbowZCurrent;  

	// the following if chain is done to see what quadrant the arm is in 

	// also below i should have used a deg to rad function .... mind you typing in pi brings certain joys in life 

	ElbowXCurrent = UpperArmL * cos(ShoulderCurrent*3.1415926/180.0); 
	if(abs(ShoulderCurrent)>90.0){
		// this means the shoulder line is veryical or leaning backwards 
		// the X for the elbow is negative 
		ElbowXCurrent = (-1.0)*abs(ElbowXCurrent);
	}else{
		// Elbow X should be piositive 
		ElbowXCurrent = abs(ElbowXCurrent);
	}
	ElbowZCurrent = UpperArmL * sin(ShoulderCurrent*3.1415926/180.0); 

	if(ShoulderCurrent<0){
		// this means we are below the 0 degree axis so the elbow pos Z is negative 
		ElbowZCurrent = (-1.0)*abs(ElbowZCurrent);
	}else{
		// this means we are above the 0 axis so Z is positive 
		ElbowZCurrent = abs(ElbowZCurrent);
	}
//	cout<<"Current Elbow Postion in cm is X = "<<ElbowXCurrent<<" Z = "<<ElbowZCurrent<<endl; 

	// figure out the wrist position from the elbow postion 
	//redefine the Elbow angle since the zero angle is tilted from the shoulder aangle 
	ElbowCurrent = ElbowCurrent + ShoulderCurrent;  
	// wrist postions in cm 
	// first doing in temp where the origin is taken at the elbow, will move back to shoulder origin once we get the pos and negs right 
	float WristXCurrent; 
	float WristZCurrent; 

	WristXCurrent = abs(ForeArmL * cos(ElbowCurrent*3.1415926/180.0)); 
	if(abs(ElbowCurrent)>90){
		// this means the Wrsit postion is negative relative to the elbow 
		WristXCurrent = ElbowXCurrent - WristXCurrent;
	}else{
		WristXCurrent = ElbowXCurrent + WristXCurrent;
	}

	WristZCurrent = abs(ForeArmL*sin(ElbowCurrent*3.1415926/180.0));

	if(ElbowCurrent<0){
		// the wrist is below the elbow 
		WristZCurrent = ElbowZCurrent - WristZCurrent;
	}else{
		// wrist is above the elbow 
		WristZCurrent = ElbowZCurrent + WristZCurrent; 
	}
	XCurrent = WristXCurrent; 
	ZCurrent = WristZCurrent; 
	if(ControlType !=7){
		XOld = XCurrent; 
		ZOld = ZCurrent; 
	//	SPAngle[2] = WristAngle +  EncoderAngles[0] - EncoderAngles[1];
		WristAngle = EncoderAngles[2]+WristAngleMod; //- EncoderAngles[0] + EncoderAngles[1]; 
//		cout<<"Current Wrist Postion in cm is X = "<<WristXCurrent<<" Z = "<<WristZCurrent<<endl; 
	}
//	cout<<"Fore arm angle relative to ground"<< ElbowCurrent<<endl;
//	cout<<"Current Wrist Angle: "<<WristCurrent<<endl; 
}

//////////////////////////////////////////////////////////////////////////////////////////////

// this is the full posController 
// the main idea of this is only change something if it is asked to chagned 
// before we would see some z drift while moving x and then this z would be the new setpoitn kind of deal 
// so if only once chagnes will the sp be chagned 
 
void posControl4(){
	float StepGain = 0.1; // how the joystick value corisponds to the value, so pushing the stick full forward will step the movement by StepGain cm in a cylce 
	// do angle setpoints 
	// use Xpos and Z pos as setpoints 
	// the following are current based on the joints angles 

	// angle def 
	// UP IS POSITIVE BITCHES 
	// ZERO IS EVERYTHING STICKING STRaight out parelle to ground 
	// each joint is a line from rotation joint to rotation joint 

//	cout<<"IAN POS CONTROL"<<endl; 
//	 currentArmPos(); // update the global arm postion `
	
	float MaxReach = sqrt(UpperArmL*UpperArmL+ForeArmL*ForeArmL)+15.0; 
	float XNew;
	float ZNew;

	// this only changes a value if its asked to change, if this is not used htere is a possiblityu of drift 

	if(fabs(Xpos)>0){
		XNew = XCurrent - Xpos*2.0;
		XOld = XNew; 
	}else{
		// make XNew the same as before 
		XNew = XOld; 
	}
		
	if(fabs(Zpos)>0){
		ZNew = ZCurrent + Zpos*2.0;
		ZOld = ZNew;
	}else{
		ZNew = ZOld; 
	}

//	cout<<"SP: X: "<<XNew<<" Z: "<<ZNew<<" Current: X: " <<XCurrent<<" Z: "<<ZCurrent<<endl;

	 
//	cout<<"ZPos: "<<Zpos<<" ZNew: "<<ZNew<<endl; 	 
	float NewLen = sqrt(XNew*XNew+ZNew*ZNew);
	if(NewLen>MaxReach){
		cout<<"ARM IS Being Moved to Illegal Postion, Will not move"<<endl; 
		cout<<"MaxReach: "<<MaxReach<<" NewLen: "<<NewLen<<endl; 
	}else{
		// move arm to new postion
		float alpha; // angle between line from shoulder to wrist and ground 
		if(XNew<1.8){
			// for small values of XNew, here tangent blows up, so lets not use it 
			alpha = 90.0 - atan(XNew/ZNew)*180.0/3.145826;
		}else{
			// this is when the tagent is not operating around +- 90 
			alpha = atan(ZNew/XNew)*180.0/3.1415926; 
		}

		if(XNew>0 && ZNew>0){
			// alpha is between 0 and 90
			alpha =  abs(alpha);
		}else if(XNew>0 && ZNew<0){
			// alpha is beteen 0 and -90 
			alpha = -1.0*abs(alpha);
		}else if(XNew<0 && ZNew>0){
			// alpha is between 90 and 180 
			alpha =  1.0*abs(alpha);
		}
		// beta is the angle that is formed between the line between the shoulder and the elbow joint and the  fore arm 
		float Beta = acos((ForeArmL*ForeArmL+UpperArmL*UpperArmL-NewLen*NewLen)/(2*UpperArmL*ForeArmL))*180.0/3.1415926; 
		// delta is the resulting angle in this triangle that is formed between the line from the shoulder to elbow, and the line from the shoulder to the wrist 
		float Delta = asin(sin(Beta*3.1415926/180.0)*ForeArmL/NewLen)*180.0/3.1415926;
//		cout<<"Alpha: "<<alpha<<" Beta: "<<Beta<<" Delta: "<<Delta<<endl;
		// should angle 
		SPAngle[0] = (-1.0)*(Delta+alpha);


		SPAngle[1] = (-1.0)*(180.0-Beta);
		if(Beta>180 || Beta<-180){
			// warn for beta
			cout<<"Beta is big"<<endl; 
		}

		// update the old X and Z
	//	XOld = XNew; 
	//	ZOld = ZNew; 
	}

//	cout<<"Moving Arm wrist Joint to [cm] X = "<<XNew<<" Z = "<<ZNew<<endl; 
	// do wrust angles 
	// the adding elbow and shoulder thing but distributed as this made it simpler ? 
	// really it just made it not twitch when switching modes 
	// this is all asyncch so it is easier if the absenc thread has not updated yet to just continue with an old varl 
	SPAngle[2] = WristAngle + WristAngleMod;  


//	cout<<"Setting Shoulder Angle: "<<SPAngle[0]<<" Elbow Angle: "<<SPAngle[1]<<" Wrist Angle: "<<SPAngle[2]<<endl;
//	cout<<"Desired Wrist angle 2 ground: "<<WristAngle<<"Wrist Angle: "<<EncoderAngles[2]<<endl; 

}

////////////////////////////////////////////////////////////////////

//Pos Control 5 is the same as PosControl4  but does nto compensate the wrist angle 

void posControl5(){
	float StepGain = 0.1; // how the joystick value corisponds to the value, so pushing the stick full forward will step the movement by StepGain cm in a cylce 
	// do angle setpoints 
	// use Xpos and Z pos as setpoints 
	// the following are current based on the joints angles 

	// angle def 
	// UP IS POSITIVE BITCHES 
	// ZERO IS EVERYTHING STICKING STRaight out parelle to ground 
	// each joint is a line from rotation joint to rotation joint 

//	cout<<"IAN POS CONTROL"<<endl; 
//	 currentArmPos(); // update the global arm postion `
	
	float MaxReach = sqrt(UpperArmL*UpperArmL+ForeArmL*ForeArmL)+15.0; 
	float XNew;
	float ZNew;

	// this only changes a value if its asked to change, if this is not used htere is a possiblityu of drift 

	if(fabs(Xpos)>0){
		XNew = XCurrent - Xpos*2.0;
		XOld = XNew; 
	}else{
		// make XNew the same as before 
		XNew = XOld; 
	}
		
	if(fabs(Zpos)>0){
		ZNew = ZCurrent + Zpos*2.0;
		ZOld = ZNew;
	}else{
		ZNew = ZOld; 
	}

//	cout<<"SP: X: "<<XNew<<" Z: "<<ZNew<<" Current: X: " <<XCurrent<<" Z: "<<ZCurrent<<endl;

	 
//	cout<<"ZPos: "<<Zpos<<" ZNew: "<<ZNew<<endl; 	 
	float NewLen = sqrt(XNew*XNew+ZNew*ZNew);
	if(NewLen>MaxReach){
		cout<<"ARM IS Being Moved to Illegal Postion, Will not move"<<endl; 
		cout<<"MaxReach: "<<MaxReach<<" NewLen: "<<NewLen<<endl; 
	}else{
		// move arm to new postion
		float alpha; // angle between line from shoulder to wrist and ground 
		if(XNew<1.8){
			// for small values of XNew, here tangent blows up, so lets not use it 
			alpha = 90.0 - atan(XNew/ZNew)*180.0/3.145826;
		}else{
			// this is when the tagent is not operating around +- 90 
			alpha = atan(ZNew/XNew)*180.0/3.1415926; 
		}

		if(XNew>0 && ZNew>0){
			// alpha is between 0 and 90
			alpha =  abs(alpha);
		}else if(XNew>0 && ZNew<0){
			// alpha is beteen 0 and -90 
			alpha = -1.0*abs(alpha);
		}else if(XNew<0 && ZNew>0){
			// alpha is between 90 and 180 
			alpha =  1.0*abs(alpha);
		}
		// beta is the angle that is formed between the line between the shoulder and the elbow joint and the  fore arm 
		float Beta = acos((ForeArmL*ForeArmL+UpperArmL*UpperArmL-NewLen*NewLen)/(2*UpperArmL*ForeArmL))*180.0/3.1415926; 
		// delta is the resulting angle in this triangle that is formed between the line from the shoulder to elbow, and the line from the shoulder to the wrist 
		float Delta = asin(sin(Beta*3.1415926/180.0)*ForeArmL/NewLen)*180.0/3.1415926;
//		cout<<"Alpha: "<<alpha<<" Beta: "<<Beta<<" Delta: "<<Delta<<endl;
		// should angle 
		SPAngle[0] = (-1.0)*(Delta+alpha);


		SPAngle[1] = (-1.0)*(180.0-Beta);
		if(Beta>180 || Beta<-180){
			// warn for beta
			cout<<"Beta is big"<<endl; 
		}

		// update the old X and Z
	//	XOld = XNew; 
	//	ZOld = ZNew; 
	}

//	cout<<"Moving Arm wrist Joint to [cm] X = "<<XNew<<" Z = "<<ZNew<<endl; 
	// do wrust angles 
 


//	cout<<"Setting Shoulder Angle: "<<SPAngle[0]<<" Elbow Angle: "<<SPAngle[1]<<" Wrist Angle: "<<SPAngle[2]<<endl;
//	cout<<"Desired Wrist angle 2 ground: "<<WristAngle<<"Wrist Angle: "<<EncoderAngles[2]<<endl; 

}