// was used for the SAR, not used after 

///arm control system -- HACK for SAR

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// IMPORT 

#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <fstream>
#include <time.h>   // time calls
#include <thread> // for multi thread 
#include <math.h>
#include "MQTTwrap/MQTT.h"
#include "json.hpp"

using namespace  std;
using json = nlohmann::json;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Linke Local Headers to Other Files 
#include "ControlSystemHeader.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Global Variables (Extern, make sure Header file relates to this)
# define StopCommand 64
# define MaxWheelSpeed 250 // max val the encoder can read 
	//int MotorAddress = {{130, 2},{130, 1},{128, 2},{128, 1}};
int MotorAddress[][2] = {{128, 6},{128, 7},{130, 7},{130, 6},{132,6}}; // list of the motor address 
/*
	0 --> Shoulder
	1 --> Elbow
	2 --> Wrist
	3 --> Hand
	4 --> wrist roll
	
*/
float MotorSpeed[]={64,64,64,64,64}; // list of current motor speeds set through serial 

float SetPoints[]={0,0,0,0,0}; // value between -1 and 1, 0 is stop


float EncoderValues[4]; // values of the encoders 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
*/

int GlobalRun; // global run var 


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Local Global Variables
float PreviousSetPoints[4]; // previous vals of setpoints 
/*
	0 --> LEFT Wheels 
	1 --> RIGHT Wheels 
	2 --> XStage
	3 --> Linear Actuator  
*/
int PreviousSpeeds[6]; // previous speeds array Serial values
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// UPDATE THIS !!!!
void messageHandler(string topic, string message){
	cout<<"on "<<topic<<" got "<<message<<endl;	
	if(topic.compare("arm")==0){

		auto data = json::parse(message);

		float shoulder = data["shoulder"].get<float>();
		float elbow = data["elbow"].get<float>();
		float wrist = data["wrist"].get<float>();
		float grip = data["grip"].get<float>();
		float wroll = data["wroll"].get<float>();

		SetPoints[0] = shoulder;
		SetPoints[1] = elbow;
		SetPoints[2] = wrist;
		SetPoints[3] = grip;
		SetPoints[4] = wroll; 

	}

}


void updateInputs(){
	MQTT* broker;

	broker = new MQTT("QSET-ARM", "localhost", 1883);

	broker->emit("system", "Controls Online");

	broker->on("arm", &messageHandler);
	

}


float SP; // setpoint for specific motor 
float Error; // differnce between SP and MotorSpeeds
float EncSpeed; // motor speed from Encoder 
int MotorVal;
float ThrottleVal;

void armControl(void){
	// the actual wheel control system that is called from the control loop 
	// loop over wheel motors to get speeds
	float kp = 0.2;//127.0/1000.0; 
	int MaxSpeed = 127;
	int MinSpeed = 0;
	

    
	for(int n=0; n<5;n++){
		// get setpoint in terms of encoder speed

		
		
		MotorVal = 0;
		ThrottleVal = 0;
		
		if(n<2){
			MotorVal = SetPoints[n]*63.0+64.0; 
		}else{
			MotorVal = SetPoints[n]*63.0+64.0 ; 
		}
		// bound the motor speed 
		if(MotorVal>MaxSpeed){
			MotorVal = MaxSpeed; 
		}else if(MotorVal<MinSpeed){
			MotorVal = MinSpeed; 
		}
		// re assign the motor speed 
		MotorSpeed[n] = MotorVal; 
//		if(n==2){
///		cout<<"Writing Motor Send: "<<MotorVal<<" EncoderValue: "<<EncSpeed<<" Index: "<< n<<" Error: "<< Error << " ThrottleVal:"<<ThrottleVal << " SetPoint:"<< SetPoints[n] <<endl;
//		}
		//set previous motor speed 
		//PreviousSpeeds[n] = MotorSpeed[n];
	}
	// loop over wheels to write
	// these were done seperatly to redunce the delay in writing to all the motors incase of large change so we dont have weird jerks 
	for(int n=0;n<5;n++){
		doSerial(n); 
	}
	
	//cout<<"Done Writing Serial For this Loop"<<endl; 
	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void controlLoop(void){
	// the actual control loop that calls the updates and control system 
	// some in function vars 
	
	int RunAux = 0; // run aux ? 
	int SPDiff; // difference in setpoints 
	float AuxThreshold = 0.1; //difference requeired to run aux 
	int WaitBetweenLoop = 50*1000; // microseconds 
	while(GlobalRun==1){
		// update inputs 
		// check if aux setpoints have changed 
		/*for(int n = 2; n<4;n++){
			// loop over aux setpoints 
			SPDiff = fabs(PreviousSetPoints[n]-SetPoints[n]); 
			if(SPDiff>AuxThreshold){
				RunAux=1;
			}else{
				RunAux=0; 
			}
		}
		if(RunAux=1){
		    //cout<<"Running Aux Control, SHOULD THIS ACTUALLY BE RUNNING? NO IT SHOULDNT FUCK "<<endl;
			//auxControl(); 
		}
		*/
		// call wheel control system 
		armControl(); 

		// delay before next run 
		//cout<<"Waiting for Next Loop"<<endl; 
		usleep(WaitBetweenLoop); 

	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void){
	// main function that does setup 

	//setup serial 
	setupSerial(); 

	// Asign Motor Address 
	//MotorAddress[0] = 128; MotorAddress[1] = 129; MotorAddress[2] = 130; MotorAddress[3] = 131; MotorAddress[4] = 132; MotorAddress[5] = 133; 

	//int MotorAddress = {{130, 2},{130, 1},{128, 2},{128, 1}};

	// zero all motors 
	for(int n=0; n<6;n++){
		// call do serial function to write serial values 
		MotorSpeed[n] = StopCommand; 
		// zero Previous Speeds
		PreviousSpeeds[n] = 0;
		doSerial(n); 
		if(n<4){
			// update previous setpoints 
			PreviousSetPoints[n] = 0;  
		}
	} 

	//drive?
	thread t2(updateInputs);
	
	// launch the control sytem 
	cout<<"Launcing Control system"<<endl; 
	// set global run var
	GlobalRun=1; 
	controlLoop(); 

	


}
