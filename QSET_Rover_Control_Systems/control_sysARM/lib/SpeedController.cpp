// this is the amin speed controller and is very similar to the speed controller for the wheels 

#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <fstream>
#include <time.h>   // time calls
#include <thread> // for multi thread 
#include <math.h>
#include "MQTTwrap/MQTT.h"
#include "json.hpp"

using namespace  std;
using json = nlohmann::json;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Linke Local Headers to Other Files 
#include "ControlSystemHeader.h"



float SP; // setpoint for specific motor 
float Error; // differnce between SP and MotorSpeeds
float EncSpeed; // local value of the enc speed 
int MotorVal; // local value for the motor voltage (7bit)
float ThrottleVal;
float MaxArmSpeed = 12.6;/// same idea s the max wheel speed this was measured as the max unloaded speed 
void speedControl(void){
	// the actual wheel control system that is called from the control loop 
	// loop over wheel motors to get speeds
	float kp = 2.1;//127.0/1000.0; 
	int MaxSpeed = 127;
	int MinSpeed = 0;
	

	for(int n=0; n<4;n++){

		// get setpoint in terms of encoder speed

		SP = 0.0; // setpoint for specific motor 
		Error = 0; // differnce between SP and MotorSpeeds
		EncSpeed = 0; // motor speed from Encoder 
		MotorVal = 0;
		ThrottleVal = 0;
		
	//	cout<<"Working on Wheel: "<<n<<endl;
		SP = SPSpeed[n]*(float)MaxArmSpeed; 

		// since the arm is all worm driven, if the SP is 0 everything should be zero
		// this was to get around an issue with the wheels where it allowed to write a value even whent he motors are stalled to hold it on a hill 
		// this is bad for the motors tho, here since we have worm drives to do the locking we can just write zero voltage 
		if(fabs(SP)<0.01){
			MotorSpeed[n] = StopCommand;
		}else{
			 
			if(n>4){
				// outside of bounds of encoder function, set enc speed to zero and things should work 
				EncSpeed = 0.0; // this is reduntant 
/*			}else if(n==1 || n ==2){
				// adjust based on the way this code was written for the wheels
				EncSpeed = -1.0*EncoderValues[n]; 
			}*/}else if(n==3 || n==2 || n==1){
				// the pan 
				EncSpeed = (-1.0)*EncoderValues[n]; // case for all that are not the shoulder, this was intially the other way but the shoulder motor was moved
			}else{
				EncSpeed = EncoderValues[n];  // the shoulder motor was on the other sdie of the arm 
			}

					
//			cout << "Encoder: "<<n<< " Value is: "<< EncSpeed<< " SP is: "<<SP<<endl; 
			//Find the difference between the setpoint and where the encoders are 
			Error = SP - EncSpeed; 
			// assign the throttle based on the difference 
			ThrottleVal = kp * Error; 
			MotorVal = MotorSpeed[n] - ThrottleVal;
			// bound the motor speed 
			if(MotorVal>MaxSpeed){
				MotorVal = MaxSpeed; 
			}else if(MotorVal<MinSpeed){
				MotorVal = MinSpeed; 
			}
		
			MotorSpeed[n] = MotorVal;
			
			 
//			cout<<"Writing Motor Send: "<<MotorVal<<" EncoderValue: "<<EncSpeed<<" Index: "<< n<<" Error: "<< Error << " ThrottleVal:"<<ThrottleVal << " SetSpeed:"<< SP<<endl;
		}
	}
	// do stuff to handle sp 5 and 6 
	// these were the wrist and ee motors and just did voltage control 
	// this is note needed as it was done when a new value of the sp for the joints was sent in the MQTT thread 
/*
	for(int n=4; n<6;n++){
		MotorVal = 0;
		ThrottleVal = 0;
		if(n<2){
			MotorVal = SetPoints[n]*63.0+64.0; 
		}else{
			MotorVal = SetPoints[n]*63.0+64.0 ; 
		}
		// bound the motor speed 
		if(MotorVal>MaxSpeed){
			MotorVal = MaxSpeed; 
		}else if(MotorVal<MinSpeed){
			MotorVal = MinSpeed; 
		}
		// re assign the motor speed 
		MotorSpeed[n] = MotorVal; 
	}	
*/
	// do serial 
	for(int n=0;n<4;n++){
		doSerial(n); 
	}
	
	//cout<<"Done Writing Serial For this Loop"<<endl; 
	
}

///////////////////////////////////////////////////////////////////////////

//same as the above version but does locking otu so that the arm cannot move to an illegal angle 
// this was tested to work 

void speedControl2(void){
	// the actual wheel control system that is called from the control loop 
	// loop over wheel motors to get speeds
	float kp = 2.1;//127.0/1000.0; 
	int MaxSpeed = 127;
	int MinSpeed = 0;
	

	for(int n=0; n<4;n++){

		// get setpoint in terms of encoder speed

		SP = 0.0; // setpoint for specific motor 
		Error = 0; // differnce between SP and MotorSpeeds
		EncSpeed = 0; // motor speed from Encoder 
		MotorVal = 0;
		ThrottleVal = 0;
		
	//	cout<<"Working on Wheel: "<<n<<endl;
		SP = SPSpeed[n]*(float)MaxArmSpeed; 

		// since the arm is all worm driven, if the SP is 0 everything should be zero
		if(SP==0){
			MotorSpeed[n] = StopCommand;
		}else{

			// get encoder speed 
			 
			if(n>4){
				// outside of bounds of encoder function, set enc speed to zero and things should work 
				EncSpeed = 0.0; 
/*			}else if(n==1 || n ==2){
				// adjust based on the way this code was written for the wheels
				EncSpeed = -1.0*EncoderValues[n]; 
			}*/}else if(n==3 || n==2 || n==1){
				// the pan 
				EncSpeed = (-1.0)*EncoderValues[n]; 
			}else{
				EncSpeed = EncoderValues[n]; 
			}

					
//			cout << "Encoder: "<<n<< " Value is: "<< EncSpeed<< " SP is: "<<SP<<endl; 
			//Find the difference between the setpoint and where the encoders are 
			Error = SP - EncSpeed; 
			// assign the throttle based on the difference 
			ThrottleVal = kp * Error; 
			MotorVal = MotorSpeed[n] - ThrottleVal;
			// bound the motor speed 
			if(MotorVal>MaxSpeed){
				MotorVal = MaxSpeed; 
			}else if(MotorVal<MinSpeed){
				MotorVal = MinSpeed; 
			}
			// re assign the motor speed 
			int dir =1; // 1 is moving joint up 
						// 0 is moving joint down 
			// the below if statment gives the same result for either case, I will fix this .... 
		//	if(n==1){
				// elbow aka bakwards case 
			if(SP>0){
				dir = 1;
			}else{
				dir = 0;  
			}
		/*	}else{
				if(SP>0){
					dir =1; 
				}else{
					dir = 0;	
				} 
			}*/

			if(angleLimits(n,dir)){ // this function call decides if the arm is at a limit or if it is moving towards a limit 
				// it will let it move away or out of a limit back to safety 
				// this function is located in the anglecontroller file. 
				MotorSpeed[n] = StopCommand; 
				cout<<"Joint: "<<n<<" is at limit"<<endl; 
			}else{
				MotorSpeed[n] = MotorVal;
			}
			 
//			cout<<"Writing Motor Send: "<<MotorVal<<" EncoderValue: "<<EncSpeed<<" Index: "<< n<<" Error: "<< Error << " ThrottleVal:"<<ThrottleVal << " SetSpeed:"<< SP<<endl;
		}
	}
	// do stuff to handle sp 5 and 6 

	// again this was not used 
	// done in the MQTT thread 
	/*

	for(int n=4; n<6;n++){
		MotorVal = 0;
		ThrottleVal = 0;
		if(n<2){
			MotorVal = SetPoints[n]*63.0+64.0; 
		}else{
			MotorVal = SetPoints[n]*63.0+64.0 ; 
		}
		// bound the motor speed 
		if(MotorVal>MaxSpeed){
			MotorVal = MaxSpeed; 
		}else if(MotorVal<MinSpeed){
			MotorVal = MinSpeed; 
		}
		// re assign the motor speed 
		MotorSpeed[n] = MotorVal; 
	}	
// increae to 6 when all motors are present 
*/
	for(int n=0;n<4;n++){
		doSerial(n); 
	}
	
	//cout<<"Done Writing Serial For this Loop"<<endl; 
	
}
