// a way of having a fance encoder zerorer 
// this doesnt work in acutallity and needs fixed 
// this was not used 
#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <fstream>
#include <time.h>   // time calls
#include <thread> // for multi thread 
#include <math.h>
#include "MQTTwrap/MQTT.h"
#include "json.hpp"

using namespace  std;
using json = nlohmann::json;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Linke Local Headers to Other Files 
#include "ControlSystemHeader.h"

#define numEncoders 3 // number of encoders 






unsigned char transferHZ(unsigned char msg,int cs){
	// transfer function 
	unsigned char msgRx =0; 
	digitalWrite(cs,0); //select the device 
//	msg >>8;
	msgRx = transferSPI(msg); 
	digitalWrite(cs,1); 
	return msgRx; 
}
////////////////////////////////////////////////////////////////////////////////////////////////

float ReadEncoderZ(int cs){
	// this function reads encoder 

//	cout<<"ReadEncoder #: "<<endl; 

	
	// the code that goes in here will be SPI based to read the AMT CUI 203 encoders 

	digitalWrite(cs,0); // select 
//	usleep(1000); 
	transferHZ(0x70,cs); //  issue zero command 

	unsigned char recieved = 0xA5;    //just a temp vairable
//	usleep(500); 
	recieved = transferHZ(0x00,cs);    //issue NOP to check if encoder is ready to send
//	printf("Returned: %04x \n",recieved);
	while (recieved != 0x80){
	    //loop while encoder is not ready to send
		recieved = transferHZ(0x00,cs);    //check again if encoder is still working 
//		printf("Returned in loop: %04x \n",recieved);
//		usleep(500);    //wait a bit
   }


	//write chip select high 
	digitalWrite(cs,1); 

	//cout<<"Encoder Zeroed"<<endl; 
	//cout<<"Rememeber to Power Cylce the Encocder for the New Value to stick"<<endl;  



}

//////////////////////////////////////////////////////////////////////////////////////////////

void setEncZero(int EncNum){
	// main function 
	
	cout<<"Zeroing Encoder: "<<EncNum<<endl; 
	int PinHere; 
	if(EncNum == 0){
		PinHere = 0;
	}else if(EncNum ==1){
		PinHere = 2; 
	}else if(EncNum ==2){
		PinHere = 3;
	}	
	ReadEncoderZ(PinHere); 
	
	cleanUpSPI(); // cleanup the SPI bus 
	cout<<"DONE Zeroing Encoder: "<<EncNum<<endl;  

}

int main(){
	// finds the zeros for the encoders 
	cout<<"Welcome to the QSET zero encoders function this zeros the arm encoders"<<endl;

MotorSpeed[n] = StopCommand; 
	for(int n=0; n<4;n++){
		// call do serial function to write serial values 
		MotorSpeed[n] = StopCommand; 
		SPAngle[n] = encoderAngle[n]; //set Spangle to current angle, to not move if we are in a weird place. 
		doSerial(n); 
	} 

	thread t1(runEncoders); 
	cout<<"Launched Encoders"<<endl; 

	if(AbsEncoderThread==1){
		// run abs encoders in their own thread 
		thread t2(encoderAngle()); 
	}
	cout<<"Set system homes"<<endl; 
	int MoNum; // motor number to zero 
	cout<<"Which joint do you want to zero?"<<endl;
	cout<<"(0) Shoulder"<<endl;
	cout<<"(1) Elbow "<<endl; 
	cout<<"(2) Wrist"<<endl;
	cout<<"(3) Pan "<<endl; 
	cin>>MoNum; 

	if(MoNum>-1 && MoNum<4){
		// good
		while(true){
			// loop through until we get to a good value 
			cout<<"Enter number of degrees to move stage"<<endl;
			cout<<"Angle safties are on, so if it wont go to the point move it manually and then re run and zero"<<endl; 
			cout<<"Enter 0 when you are satisfied"<<endl; 
			float MoveBy; // angle to move by
			cin>>MoveBy;
			if(MoveBy==0){
				// we are at the zero point
				setEncZero(MoNum);
				break;  
			}else{
				// Set the SP for that motor to the move amount 
				SPAngle[MoNum]+= MoveBy; 
				// run the control system 
				AtAngleDeadZone = 0; // we are not at the dead zone 
				while(AtAngleDeadZone==0){
					// while not in dead zone; 
					angleControl();
					usleep(50*1000); // wait between loops
				}
				MotorSpeed[n] = StopCommand; // stop the motor 
			}
		}
	}
	cout<<"Done Zeroring Joint "<<MoNum<<endl; 
	cout<<"Will exit now since we need to reboot the encoders anways. Just re run this or the control system for more fun"<<endl; 
	cout<<"Have a gread fucking day"<<endl; 
}


