// file not used

#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <fstream>
#include <time.h>   // time calls
#include <thread> // for multi thread 
#include <math.h>
#include "MQTTwrap/MQTT.h"
#include "json.hpp"

using namespace  std;
using json = nlohmann::json;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Linke Local Headers to Other Files 
#include "ControlSystemHeader.h"

// Global Variables (Extern, make sure Header file relates to this)
int StopCommand = 64; 

 
	//int MotorAddress = {{130, 2},{130, 1},{128, 2},{128, 1}};
int MotorAddress[][2] = {{128, 7},{128, 6},{130, 7},{130, 6},{132,7},{132,6}}; // list of the motor address 
/*
	0 --> Shoulder
	1 --> Elbow
	2 --> Wrist
	3 --> PAN
	4 --> wrist roll
	5 --> EE 
	
*/
float MotorSpeed[]={64,64,64,64,64,64}; // list of current motor speeds set through serial 

float SetPoints[]={0,0,0,0,0,0}; 
float SPSpeed[] ={0,0,0,0,0,0};// value between -1 and 1, 0 is stop
float SPAngle[] = {0,0,0,0};


float EncoderValues[4]; // values of the encoders 
float EncoderAngles[4]; // Angle values of the encoders 
/*
	0 --> Shoulder
	1 --> Elbow
	2 --> Wrist
	3 --> PAN
*/
int CSPins[] = {23,24,25,28}; // same order 

int GlobalRun; // global run var 

float PreviousSetPoints[4]; // previous vals of setpoints 
/*
	0 --> LEFT Wheels 
	1 --> RIGHT Wheels 
	2 --> XStage
	3 --> Linear Actuator  
*/
int PreviousSpeeds[6]; // previous speeds array Serial values
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/

// max and min angles (this depends on the joint )
int MaxAngles[][2] = {{180,-180},{180,-180},{180,-180},{180,-180}}; // cols Max then min 
/*
	3 --> Pan Motor  
	0 --> Shoulder 
	1 --> Elbow
	2 --> Wrist
*/
float CumAngleDiff[] = {0,0,0,0}; // Cumulative angle difference for integral control 



int ControlType = 2; // control the control type 
/*
	0--> Speed Control 
	1--> Angle Control 
	2--> Pos Control 
	3--> Ye old shit is fucked control 
	4--> E-stop
	5--> Wait
*/

int AtAngleDeadZone = 0; // we are not at the angle deadzone

// position controll 
float XHome = 0.6; 
float YHome = 0;
float ZHome = 0.0;
float Xpos = XHome;
float Ypos = YHome;
float Zpos = ZHome; 
float WristAngle = 0.0; 

// the abs encoder thread is legacy we are now using them in a thread 
 int AbsEncoderThread =1;  //0= use serries 

 float HomeAngles[] = {0,0,0,0}; 




// joint names 
string JointNames[6] = {"Shoulder","Elbow","Wrist","Pan","WristRotate","End Effector"};
