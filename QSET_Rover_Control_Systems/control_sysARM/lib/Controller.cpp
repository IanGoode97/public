// file not used 

#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <wiringPi.h> 
#include <math.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
using namespace std;
#include "spiIAN.h" 
#include "Functions.h"



// control stuff 

#define PI 3.14159265
#define C  180/PI
#define C2 PI/180

//Robot Link Legnths
#define A1 0.5
#define A2 0.5
#define A3 0.5
#define A4 0.5

//Motor Constants
#define K1 1.0297
#define K2 1.8263
#define V 12

//Feedback Gain Constaints
#define ALPHA 0.5
#define GAMMA 0.4
#define KAPPA 0.3

// position controll 
float XHome = 0; 
float YHome = 0;
float ZHome = 0;
float Xpos = XHome;
float Ypos = YHome;
float Zpos = ZHome; 

