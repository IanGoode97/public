
#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <wiringPi.h> 
#include <math.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
using namespace std;
#include "spiIAN.h" 
#include "ControlSystemHeader.h"
//#include "ControlSystemHeader2.h"
#include "MQTTwrap/MQTT.h"


int PanIsLeft=0; // 0 means at center or 

int CS; // CS PIN make it global for easy 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int runSetupABS =1; 
void spiSetup(void){
	// spi setup function only run it once 
	cout<<"In chip select setup"<<endl; 
	// setup the chip select 
	wiringPiSetup(); 
	for(int n=0;n<4;n++){
		pinMode(CSPins[n],OUTPUT); 
		// waggle the cspin to power cylce 
		// here a bunch of delays are added to give everything a time to realize the standard delay is 1ms
		usleep(1000); 
		digitalWrite(CSPins[n],0);
		usleep(1000); 
		digitalWrite(CSPins[n],1);
	}


}

void tidyUpSPI(){
	// clean SPI 
	// this just puts the pins to intputs, this is good practice 
	cleanUpSPI(); 
	for(int n =0; n<3; n++){
		pinMode(CSPins[n],INPUT);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// this function actually sends and recives messages from the encoder. This sends out on the SPI and Rxs on the same 
// the chip select needs to work right for this to get good data, this does not care it it will just return whatever 
// these encoders are active low which is fucking stupid 
// this chirps the chip selct pin for you 
unsigned char transferH(unsigned char msg, int cspin){
	// transfer function 
	unsigned char msgRx =0; 
	digitalWrite(cspin,0); //select the device 
//	msg >>8;
	msgRx = transferSPI(msg); 
	digitalWrite(cspin,1); 
	return msgRx; 
}
////////////////////////////////////////////////////////////////////////////////////////////////

unsigned char temp[2];

float ReadEncoder(int Index){
	// this function reads encoder 
	int cspin = CSPins[Index]; 
	if(runSetupABS==1){
		// run the setup and set the runsetup command to not be 1 
		runSetupABS++; 
		spiSetup(); 
		cout<<"Setup the SPI Bus"<<endl; 
	}

//	cout<<"ReadEncoder #: "<<endl; 

	int EncoderPos; // this is the index it has 4096 bins 

	// the code that goes in here will be SPI based to read the AMT CUI 203 encoders 

	digitalWrite(cspin,0); // select 
//	usleep(1000); 
	transferH(0x10,cspin); //  issue read command this says hey i wanna read you 

	unsigned char recieved = 0xA5;    //just a temp vairable, this just sets the rx var to something so it is not null 
//	usleep(500); 
	recieved = transferH(0x00,cspin);    //issue NOP to check if encoder is ready to send
//	printf("Returned: %04x \n",recieved);
	// do the above untile the response is not 10, once it is not 10 we can parse the data 
	while (recieved != 0x10){
	    //loop while encoder is not ready to send
		recieved = transferH(0x00,cspin);    //check again if encoder is still working 
//		printf("Returned in loop: %04x \n",recieved);
//		usleep(500);    //wait a bit
   }
   // this parses the data in into an array of chars, you can see it is a 2 byte bigian thing 
	temp[0] = transferH(0x00,cspin);    //Recieve MSB
	temp[1] = transferH(0x00,cspin);    // recieve LSB

	//write chip select high 
	digitalWrite(cspin,1); // this turns the encoder off 


	temp[0] &=~ 0xF0;    //mask out the first 4 bits

	EncoderPos = temp[0] << 8;    //shift MSB to correct ABSposition in ABSposition message
	EncoderPos += temp[1];    // add LSB to ABSposition message to complete message


	//EncoderPos= 8; 
	// this returns the encoer angle 
	return EncoderPos; 
}

// this array is used to track the change in postion of an encoder this was indented to be used as stalls 

float EncAngleChange[4];

// main function of this file 
// this funtion is called in a thread it runs until the program dies 

void encoderAngle(void){
	// read incoders 
	float pos; 
	float angle;  
	float LastAngle; 
	
	cout<<"Spwaned Encoder Reader"<<endl;
	while(GlobalRun ==1){
		// thsi loops until we stop the program 
		for(int Index = 0; Index<4; Index++){
			// loop over each encoder and get the current postion 
			pos = ReadEncoder(Index); 
			LastAngle = EncoderAngles[Index]; // save previous angle to see if we are moving 
			angle = 0.087890625*(float)pos; // that first number numper is (float)(360/2^12) #funfact 

			// easy zero correction 
			// this zero correction was done to move the zero point from straight horizontal to the angles where the marks on the arm are to make 
			// it easy to zero the arm and leave the arm in its zero postion for transport so you could zero the moment you turned the rover on 
			if(Index==0){
				// shoulder 
				angle = angle - 100.0; 
			}else if(Index==1){
				// elbow 
				angle = angle - 152.2;
			}else if(Index==2){
				// wrist
				angle = angle + 53.6;
			}

			// this code is used to do the 180 neagive 180 break right off the bat 
			// the data the encoder gives is 0 to 4095 or 0 to 360 less whatever the angle step is 
			// since most joints cannot rotate more than 300 degrees it was decided to define angles as positive and minus since it would be operating around 0
			// and the -180 and 180 angles would be in the fringes 
			// way to many times in the rest of this code do we check this bound 
			if(angle>180){
				angle -= 360.0; 
			}


			EncoderAngles[Index] = angle; // set the angle 
			EncAngleChange[Index] = angle - LastAngle; // see how much we ahve changed, this array is used in other places 
			// make it between -360 and 360 as a value of last angle could be 180 and the current angle could be -180 or vice versa hence the  need to have this go that far 
			// the goal was to make it so that if the thing moved it could not give  a zero result 
			// since it is not cumulative it does nto really matter it is just looking at the differnce between this run and the one 50ms ago 
			
			// this last change was used to make sure the encoder thread was running and not stalled 
			// if we loose an encoder the thread will useually fuck off 
			if(EncAngleChange[Index]>360){
				// break point 
				EncAngleChange[Index] -=360.0;
			}else if(EncAngleChange[Index]<-360){
				// break point 
				EncAngleChange[Index] += 360.0; 
			}
		
		}

		// encoders have been updated 
		AbsEncoderChange = 0; // re zero this 
		// send out arm angles 
//		cout<<"In encoders"<<endl;
	//	broker->emit("armAngles","{\"shoulder\" :EncoderAngles[0]}");

//		broker->emit("armAngles","{\"shoulder\" :"+to_string(EncoderAngles[0])+",\"elbow\" :" + to_string(EncoderAngles[1])+",\"wrist\" :" +to_string(EncoderAngles[2])+"}");


		

		usleep(50*1000); // sleep between loops
	}
	cout<<"Nice Encoder Exit"<<endl; 
}
