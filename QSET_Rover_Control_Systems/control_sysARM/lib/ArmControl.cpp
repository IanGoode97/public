////Main used at URC 2018 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// IMPORT 

#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <fstream>
#include <time.h>   // time calls
#include <thread> // for multi thread 
#include <math.h>
#include "MQTTwrap/MQTT.h"
#include "json.hpp"
#include <signal.h>
using namespace  std;
using json = nlohmann::json;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Linke Local Headers to Other Files 
#include "ControlSystemHeader.h"
//#include "ControlSystemHeader2.h"
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Global Vars

// Global Variables (Extern, make sure Header file relates to this)
int StopCommand = 64; 

 
	//int MotorAddress = {{130, 2},{130, 1},{128, 2},{128, 1}};
//int MotorAddress[][2] = {{128, 7},{128, 6},{130, 7},{130, 6},{132,7},{132,6}}; // list of the motor address 
int MotorAddress[][2] = {{130, 7},{128, 6},{130, 6},{128, 7},{129,6},{129,7}}; // list of the motor address 
// please see the sabertooth docs for how these work but the relation between index of this array and joints is seen below 
// there was a goal to put the shoulder and elbow on seperate controllers to try and spread the heat waround 
/*
	0 --> Shoulder
	1 --> Elbow
	2 --> Wrist
	3 --> PAN
	4 --> EE
	5 --> wrol 
	
*/
float MotorSpeed[]={64,64,64,64,64,64}; // list of current motor speeds set through serial 

float SetPoints[]={0,0,0,0,0,0}; 
float SPSpeed[] ={0,0,0,0,0,0};// value between -1 and 1, 0 is stop
float SPAngle[] = {0,0,0,0};// set angles for the first four joints 


float EncoderValues[4]; // values of the encoders 
float EncoderAngles[4]; // Angle values of the encoders 
/*
	0 --> Shoulder
	1 --> Elbow
	2 --> Wrist
	3 --> PAN
*/
int CSPins[] = {23,24,25,28}; // same order 

int GlobalRun; // global run var 

float PreviousSetPoints[4]; // previous vals of setpoints 
/*
	0 --> LEFT Wheels 
	1 --> RIGHT Wheels 
	2 --> XStage
	3 --> Linear Actuator  
*/
int PreviousSpeeds[6]; // previous speeds array Serial values
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/

// max and min angles (this depends on the joint )
int MaxAngles[][2] = {{60,-110},{97,-148},{95,-125},{210,-210}}; // cols Max then min 
// from the zero point these worked well for the arm not totally fuckign itself 
/*
	3 --> Pan Motor  
	0 --> Shoulder 
	1 --> Elbow
	2 --> Wrist
*/
float CumAngleDiff[] = {0,0,0,0}; // Cumulative angle difference for integral control 



int ControlType = 5; // control the control type 
/*
	0--> Speed Control 
	1--> Angle Control 
	2--> Pos Control 
	3--> Ye old shit is fucked control 
	4--> E-stop
	5--> Wait
*/

int AtAngleDeadZone = 0; // we are not at the angle deadzone

// position controll 
// default alues 
float XHome = 0.6; 
float YHome = 0;
float ZHome = 0.0;
float Xpos = XHome;
float Ypos = YHome;
float Zpos = ZHome; 
float WristAngle = 0.0;
float PanAngle = 0.0; 

// the abs encoder thread is legacy we are now using them in a thread <-- good comment that makese sense, run them in a thread .... 
 int AbsEncoderThread =1;  //0= use serries 

 float HomeAngles[] = {0,0,0,0}; 
 int AbsEncoderChange = 0; // a value to check to make sure the abs encoders are updating 




// joint names list for logging 
string JointNames[6] = {"Shoulder","Elbow","Wrist","Pan","WristRotate","End Effector"};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MQTT* broker;
//int PreArmControlMode = 2; 



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// UPDATE THIS !!!! <---- pretty sure i did, just didnt update the comment 
// this is what was used at URC 2018
// i am going to leave this for the most part as V is going to change it for ROS 
void messageHandler(string topic, string message){
	cout<<"ARM CPPon "<<topic<<" got "<<message<<endl;	
	// ESTOP Handler 
	if(topic.compare("estop")==0){
		for(int n=0; n<6;n++){
			MotorSpeed[n] = 64; 
			doSerial(n); 
		}
		ControlType =5; // makes us stay in estop mode until we change it back 
	}
	// Normal Arm Handler 
	if(topic.compare("arm")==0){
//		ControlType = PreArmControlMode; // set this incase we just did a angle control  
		auto data = json::parse(message);

		float shoulder = data["shoulder"].get<float>();
		float elbow = data["elbow"].get<float>();
		float wrist = -data["wrist"].get<float>();
		float grip = data["grip"].get<float>();
		float wroll = data["wroll"].get<float>();
		float pan = data["pan"].get<float>();

		SetPoints[0] = shoulder;
		SetPoints[1] = elbow;
		SetPoints[2] = wrist;
		SetPoints[3] = pan; 
		//SetPoints[5] = grip;
		//SetPoints[4] = wroll; 

		// directly write the grip and wroll values like the lin act 
		MotorSpeed[4] = grip*63+64; 
		MotorSpeed[5] = wroll*63+64; 
		doSerial(4);
		doSerial(5);		
	}else if(topic.compare("armMode")==0){
		// change the arm control system mode 
		int newMode = stoi(message); 

		ControlType = newMode; 
//		PreArmControlMode = ControlType; // this is used to remember the state before we change it when we go into agnle control. 
		cout<<"Switched MODE !!!!, now on MODE: "<<ControlType<<endl; 
		if(ControlType ==5){
			// we are going into standby, give an estop first
			cout<<"Entering Standby Sent and Estop to Be Safe"<<endl;
			for(int n=0; n<6;n++){
				MotorSpeed[n] = 64; 
				doSerial(n); 
			}
		}else if(ControlType ==2||ControlType==7){
			currentArmPos(); // update the global arm postion `
			// this means we are trying to to switch into postion control, check the angle of the elbow joint 
			// if the angle of the elbow joint is greater than about a couple of degrees it is inverted and the pos control
			// will move the arm to accoutn for this 
			float ElbAngle = EncoderAngles[1]; 
			if(ElbAngle>180){
				ElbAngle -= 360.0; 
			}
			if(ElbAngle> 4){
				// put arm into standby 
				ControlType = 0; 
				cout<<"Arm was in bad pos for pos control, reverted to Standby"<<endl; 
				broker->emit("armModeNEW","0");
			}// the else case leave sthe arm the same 
			
		}
		/*
			0--> Speed Control 
			1--> Angle Control 
			2--> Pos Control 
			3--> Voltage control  
			4--> E-stop
			5--> Wait
		*/
	}
	if(topic.compare("arm2Angle")==0){
		// run angle control 
		auto data = json::parse(message);
		ControlType = 1; // Set angle control 
	
		SPAngle[0] = data["shoulder"].get<float>();
		SPAngle[1] = data["elbow"].get<float>();
		SPAngle[2] = data["wrist"].get<float>();
		SPAngle[3] = data["pan"].get<float>();

	}
}

//MQTT crap Valen will change 
void updateInputs(){
	string HostName = "QSET-ARM-" + to_string(rand());
	broker = new MQTT(HostName.c_str(), "192.168.1.21", 1883);
//	broker = new MQTT("QSET-ARM", "localhost", 1883);

	broker->emit("system", "Arm Controls Online");


	broker->on("arm", &messageHandler); // defualt arm controls 
	broker->on("armMode", &messageHandler); // change the arm mode 
	broker->on("estop",&messageHandler); // send and estop, control mode 4 will also do this 
	broker->on("arm2Angle",&messageHandler); // this is the mode to set the arm to an angle 

}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void controlLoop(void){
	// the actual control loop that calls the updates and control system 
	// some in function vars 
	int WaitBetweenLoop = 45*1000; // microseconds so 45ms
	usleep(50*1000); 
//	SPAngle[3] = EncoderAngles[3]; // get the intial value of the pan. This is to try to get the pan to stay put at start
//	SPAngle[2] = EncoderAngles[2];
//	WristAngle = SPAngle[2]-EncoderAngles[1]+EncoderAngles[0]; 
//	cout<<"Wrist Intial Angle:" <<WristStart<<endl; 
	// intiallize the wrist postion 
	currentArmPos(); // get arm pos 
//	broker->emit("poop","HI"); // a mature test 
	WristAngle = WristCurrent; 

	// at boot give these a value 

	XOld = XCurrent; 
	ZOld = ZCurrent; 

	// main control loop 

	while(GlobalRun==1){
		fuckeryCheck(); // check for fuckery
		// print current status for joint angles 
		// if the encoder thread dies this keeps going 
		broker->emit("armAngles","{\"shoulder\" :"+to_string((1.0)*EncoderAngles[0])+",\"elbow\" :" + to_string(EncoderAngles[1])+",\"wrist\" :" +to_string(EncoderAngles[2])+",\"pan\" :"+to_string(EncoderAngles[3])+"}");	// set the wrist angle to where it currently is. 
		currentArmPos(); // update the current arm position pos control 
		//cout<<"looping control"<<endl;		
//		ControlType =1;
//		cout<<"Control Type: "<<ControlType<<endl;

		// how this loop then does one of a vairety of things based on the set control type
		// see each arm of the if statment for comments 
		
		if(ControlType==1){
			// angle control 
			// this will put the arm to an angle 
			// the first line figures out how fast each joint should be going to get the arm to the angles it wants to be at 
			// the second line turns this speed into a voltage based on the speed encoders on the but of the back of each motor 
			linAngleControl(); // pleb angle control versus teh better control that was never tuned 
			speedControl(); 
		}else if(ControlType==2 ){
			// pos control does not do the wrist 
			// do setpoint stuff for x and z using the arm control for elbow and wrist
			AbsEncoderChange ++; // step the abs encoder chang e
			Xpos = SetPoints[0]; // in this mode this is where we get the postion from, see the MQTT update 
			Zpos = SetPoints[1]; // ditto 
			posControl5(); // run pos controller 5 which gives us angles each joint needs to be at 
			// after the pos controller runs we need to call the angle controller 
			linAngleControl(); // run the angle controller to get speeds 
			SPSpeed[3] = SetPoints[3]/3.4; // just do speed for the pan // set speeds for the pand and wrist 
			SPSpeed[2] = SetPoints[2]; // just do speed for the wrist 
 			speedControl(); // run the speed controller 
		}else if(ControlType==7 ){
			// pos control that does the wrist 
			// do setpoint stuff for x and z using the arm control for elbow and wrist
			AbsEncoderChange ++; // step the abs encoder chang e
			Xpos = SetPoints[0]; // in this mode this is where we get the postion from, see the MQTT update 
			Zpos = SetPoints[1]; 
			WristAngle -= SetPoints[2]*1.3; // a little bit of gain to make it not painful to move 
			posControl4(); // call this pos controller which decides joint angles 
			// after the pos controller runs we need to call the angle controller 
			linAngleControl(); // convert angles to speeds 
			SPSpeed[3] = SetPoints[3]*0.8; // just do speed for the pan 
			speedControl(); // convert speeds to voltages 
		}else if(ControlType==3){
			// just write voltage controls
			// ye old shit is fucked voltage control #oldschoolQSETcontrol 

			for(int n=0;n<4;n++){
				if(n==0 || n ==2 || n ==3){
					MotorSpeed[n] = 64 - SetPoints[n]*63.0;
				}else{
					MotorSpeed[n] = 64 + SetPoints[n]*63.0; // the elbow motor was wired backwards by some person 
				}

				// bound it so we dont write crap 
				if(MotorSpeed[n]>127){
					MotorSpeed[n] = 127;
				}else if(MotorSpeed[n]<0){
					MotorSpeed[n] = 0; 
				}
			}
			// do serial for this 
			// normally the do serial is called from the speed controller so here we need to force it manually 
			for(int n=0;n<4;n++){
				doSerial(n); 
			}
		}else if(ControlType==0){
			// speed control
//			cout<<"Speed control"<<endl;
			for(int n=0;n<4;n++){
				if(n ==1){
					SPSpeed[n] = -1.0*SetPoints[n]; // reverse for elbow 
				}else if(n==3){
					// slow the pan 
					SPSpeed[n] = SetPoints[n] / 1.8; // make the pan slow 
				}else{
					SPSpeed[n] = SetPoints[n]; // normal case 
				}
			}
			speedControl2(); // this is limited speed control 
			// what is meant by "limited" is it looks for the angles and stops when we get close to a stop pints 
		}else if(ControlType==4){
			// estop
			//STOP 
			// STAHP 
			// #FREEZETAG 
			// this stops the motors 
			// then we write the stop command to each motor 
			for(int n=0;n<6;n++){
				MotorSpeed[n] = 64; 
				doSerial(n); 
			}
		}else if(ControlType ==6){
			// speed control without limits 
			// this is speed control again but does not take into acoutn limits
			for(int n=0;n<4;n++){
				if(n ==1){
					SPSpeed[n] = -1.0*SetPoints[n];
				}else if(n==3){
					// slow the pan 
					SPSpeed[n] = SetPoints[n] / 1.8; // make the pan slow 
				}else{
					SPSpeed[n] = SetPoints[n]; 
				}
			}
			speedControl(); // call the speed controller 
		}
		else{
			// fuck off and do fuck all 
			//  add extra sleep 
			// this is just a standby mode 
			// the ELSE CASE SHOULD BE THE ESTOP MODE ....... 
			usleep(WaitBetweenLoop); 
		}
		// delay before next run 
		//cout<<"Waiting for Next Loop"<<endl; 
		usleep(WaitBetweenLoop); 

	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int PanLeftCount = 0; 
// check for joint fuckery 
void fuckeryCheck(){
	// check for fuckery 
	float AngleChange;
	float EncSpd; 
	float MotorVoltage; 

	
	/*	for(int n = 0;n<4;n++){
			AngleChange = EncAngleChange[n]; // degrees 
			EncSpd = EncoderValues[n]; // bounded roughyl around -12 to 12 units: LOL
			MotorVoltage = MotorSpeed[n]-64; // this means a zero valeu is 0 and 63 or - 63 is full chooch 

			if(abs(MotorVoltage)>0 && abs(EncSpd)<0.1){
				// this means we are giving voltage but the motor is not moving
				cout<<"WARNING: the "<<JointNames[n]<<" joint Appears to Be Stalled"<<endl; 
				// log the output 
				cout<<JointNames[n]<<" Joint reports : \n"<<"Angle Change: "<<AngleChange<<" deg \n"<<
					"Motor Speed: "<<EncSpd<<"\n"<<"Motor Voltage: "<<MotorVoltage<< " re-centered around zero"<<endl;
			}
			if(abs(EncSpd)>0.1 && abs(AngleChange)<0.2){
				// this means the motor is moving but encoder is not registering
				cout<<"WARNING: the "<<JointNames[n]<<" joint is either slippign or having abs encoder problems"<<endl; 
				cout<<JointNames[n]<<" Joint reports : \n"<<"Angle Change: "<<AngleChange<<" deg \n"<<
					"Motor Speed: "<<EncSpd<<"\n"<<"Motor Voltage: "<<MotorVoltage<< " re-centered around zero"<<endl;
			}else if(abs(EncSpd)<0.1 && abs(AngleChange)>0.2){
				// other case, joint is free wheeling 
				cout<<"WARNING: the "<<JointNames[n]<<" joint looks like it is free wheeling, or speed encoder is broken"<<endl; 
				cout<<JointNames[n]<<" Joint reports : \n"<<"Angle Change: "<<AngleChange<<" deg \n"<<
					"Motor Speed: "<<EncSpd<<"\n"<<"Motor Voltage: "<<MotorVoltage<< " re-centered around zero"<<endl;
			}

			// check for joint sliipage
			if((EncSpd*AngleChange)<0 && n ==0){
				// the shoulder joint is having a belt slip issue 
				cout<<"WARNING: Shoulder Joint Belt Slip"<<endl; 
				cout<<JointNames[n]<<" Joint reports : \n"<<"Angle Change: "<<AngleChange<<" deg \n"<<
					"Motor Speed: "<<EncSpd<<"\n"<<"Motor Voltage: "<<MotorVoltage<< " re-centered around zero"<<endl;
			}else if((EncSpd*AngleChange)>0 && n ==1){
				// the elbow joitn is having belt slip problems 
				cout<<"WARNING: Elbow Joint Belt Slip"<<endl;
				cout<<JointNames[n]<<" Joint reports : \n"<<"Angle Change: "<<AngleChange<<" deg \n"<<
					"Motor Speed: "<<EncSpd<<"\n"<<"Motor Voltage: "<<MotorVoltage<< " re-centered around zero"<<endl;
			}

		}*/
		// check to make sure Boat knew where its fucking arm was so it wouldnt tear the crap out of the wires 
		// this works 
		float LocPanAngle = EncoderAngles[3];

		// break the pan angles at -180 -->180
		if(LocPanAngle>180){
			LocPanAngle -= 360.0; 
		}

		if(PanLeftCount==0){
			// This is the first time we have run this so we can say if the pan is is left of 0 or not this is the inital case 
			// THIS ASSUMES THE ARM IS POINT VAUGLY FORWARD AT BOOT 
			if(LocPanAngle<0){
				PanIsLeft = 1; // the pan is left of center 
			}else{
				PanIsLeft = 0; // the pan is right of center 
			}
			PanLeftCount++; // step the count so we dont run the setup again 
		}else{
			// everycase that is not the first case 
			// for the pan to go from left to right it must pass throguh zero, sicne this loops considerably faster than the arm can move if we set a range of values near 
			// zero this will be the onyl place that the arm has the potential of going from left to right or vice versa 
			if(LocPanAngle<0 && LocPanAngle > -5 && PanIsLeft ==0){
				PanIsLeft = 1; 
				cout<<"Pan is LEFT of Center"<<endl; 
			}else if(LocPanAngle>0 && LocPanAngle <5 && PanIsLeft ==1){
				PanIsLeft = 0; // the pan is right.  
				cout<<"Pan is RIGHT of Center"<<endl; 
			}
			// the else case is do not modify the PanIsLeft Value 
		}
		//broker->emit("armAngles","{\"shoulder\" :"+to_string(EncoderAngles[0])+",\"elbow\" :" + to_string(EncoderAngles[1])+",\"wrist\" :" +to_string(EncoderAngles[2])+"}");
		
		// use this thread to see if the arm is left or not 

		if(AbsEncoderChange>3){
			cout<<"WARNING: the abs encoders have not updated. The angle controller has looped:"<<AbsEncoderChange<<" more times than the abs encoders"<<endl;
			ControlType = 6; // roll back to speed control with out angle limits 
			broker->emit("armModeNEW","6");// emit that we just changed so we know what is goihgn on 
			// probably shoudl emit a comment 
		}
 
	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void cleanKill(int signum){
	cout<<"Clean Kill Func Value: "<<signum<<endl; 
	if(signum ==SIGINT){
		cout<<"Handle Clean Exit"<<endl;		
		for(int n = 0;n<6;n++){
			MotorSpeed[n] = 64; 
			doSerial(n);
		}
		// try to joing the abs encoders so they dont get killed half calked 
		GlobalRun = 0; 
		cout<<"Set GlobalRun to 0 all while loops should stop"<<endl; 
	
	//	t[2].join(); // the thread should stop and join before we leave. 
//		t[4].join(); // join the control looops
	//	t[3].join(); // join the fucker check
		// the MQTT thread, and the speed encoder threads are not joined in this way since they are not obviously loops 


		// should go back to main thread now as the controlLoop should fuck off 
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// main function 


int main(void){
	signal(SIGINT,cleanKill); // create at clean kill function to listen for a Ctrl+C 
	// set global run var
	GlobalRun=1; 
	// main function that does setup 
	cout<<"Spawned CPP Arm Control System :)"<<endl; // the smiley face is a "Don't Pannick, hold your towel"
	//setup serial 
	setupSerial(); // setup the serial port 

	// zero all motors 
	// this stops all the motors 
	for(int n=0; n<6;n++){
		// call do serial function to write serial values 
		MotorSpeed[n] = StopCommand; 
		// zero Previous Speeds
		PreviousSpeeds[n] = 0;
		doSerial(n); 
		if(n<4){
			// update previous setpoints 
			PreviousSetPoints[n] = 0;  
			SPAngle[n] = HomeAngles[n]; // set each joint to home point 
		}
	} 
	// this thread runs the speed encoders 
	thread t1(&runEncoders); 
	cout<<"Launched Encoders"<<endl; 

	// this calls the MQTT thread to look for updates 
	thread t2(&updateInputs);

	// this runs the abs pos encoders in their own thread 
    thread t3(&encoderAngle); 
//	}

	// launch the control sytem in the main thread 
	cout<<"Launcing Control system"<<endl; 

	controlLoop(); // call the control loop function 
	// if we get a global run of zero we will come hre 
	cout<<"Control Loop has died :("<<endl;
	t3.join(); // the control loop and abs encoders loops both die when Global val gets set to not 1 so when the loop dies for the control loop we will come here 
	// then we will go to looking at the status of the absolute encoders thread which will also be dead, if it sucessfully died we can join
	// the only case where this will not work is if the abs encoders are fucked and the thread is frozen. 
	cout<<"Joined the Abs Encoders Thread"<<endl;

	delete broker; // get rid fo the broker, this stops the update thread 
	cout<<"Broker been deleted"<<endl;
	t2.join(); // join the stopped thread 
	cout<<"Joined the MQTT Thead"<<endl; 
	cleanPhidget(); // this stops the looping for the phidgets 
	t1.join();// join the stopped thread  
	cout<<"Joined the phidget thread"<<endl;

	// clean up the SPI mess #NixonBurnTheTapes
	// you know what they say about the SPI, they are the FBI of the sea. 
	tidyUpSPI();
	// comment that we have a clean exit to the stdout just cuz 
	cout<<"Clean Exit"<<endl;

	return 0; 
}
