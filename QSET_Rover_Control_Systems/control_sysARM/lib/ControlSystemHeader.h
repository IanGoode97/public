// Control System header

//////////////////////////////////////
// GLOBAL FUNCTIONS 

int doSerial( int); // the first int is the device, the second int is the speed value 
void setupSerial(void); // setup serial command 
void runEncoders(void); // the encoder function (only call once)
void encoderAngle(void); // thread to get encoder angles
void speedControl(void);
void speedControl2(void);
void cleanPhidget(void); 
int angleLimits(int,int); 
void posControl4(void); 
void posControl5(void); 
void currentArmPos(void); 
void linAngleControl(void); 
void tidyUpSPI(void);
void fuckeryCheck(void); 


///////////////////////////////////////////////////////////////////////////////////////////////////////

// External Vars to be Shared over all files 

extern int MotorAddress[6][2]; // list of the motor address 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/
extern float MotorSpeed[6]; // list of current motor speeds set through serial 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
/	4 --> X Stage 
	5 --> Linear Actuator 
*/
extern float SetPoints[6]; // value between -1 and 1, 0 is stop
/*
	0 --> LEFT Wheels 
	1 --> RIGHT Wheels 
	2 --> XStage
	3 --> Linear Actuator  
*/

extern float EncoderValues[4]; // values of the encoders speed
 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
*/

extern int GlobalRun; // global run var 

extern float EncoderAngles[4]; // Angle values of the encoders
extern int CSPins[4]; //CS pins for angle encoders 

extern float PreviousSetPoints[4]; // previous vals of setpoints 
/*
	0 --> LEFT Wheels 
	1 --> RIGHT Wheels 
	2 --> XStage
	3 --> Linear Actuator  
*/
extern int PreviousSpeeds[6]; // previous speeds array Serial values
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/

// max and min angles (this depends on the joint )
extern int MaxAngles[4][2]; // cols Max then min 
/*
	3 --> Pan Motor  
	0 --> Shoulder 
	1 --> Elbow
	2 --> Wrist
*/
extern float CumAngleDiff[4]; // Cumulative angle difference for integral control 
extern float SPAngle[4]; 
extern float SPSpeed[6]; 
extern float SPPos[4]; 

extern int ControlType; // control the control type 
/*
	0--> Speed Control 
	1--> Angle Control 
	2--> Pos Control 
	3--> Ye old shit is fucked control 
*/
extern int AtAngleDeadZone;

// position controll 
extern float XHome; 
extern float YHome;
extern float ZHome;
extern float Xpos;
extern float Ypos;
extern float Zpos;
extern float WristAngle;
extern float PanAngle; 



extern int AtAngleDeadZone;
extern int AbsEncoderThread; 
extern int StopCommand;
extern float HomeAngles[4]; 
extern float XCurrent; 
extern float ZCurrent;

extern float EncAngleChange[4]; // change from previous in angle; 


extern int AbsEncoderChange; 
extern float WristCurrent; // wirst current angle 

extern float XAbs;
extern float ZAbs; 
extern float XOld; 
extern float ZOld; 
extern int PanIsLeft; //  is the arm left or right of center // 1 is it is left, 0 it is not 