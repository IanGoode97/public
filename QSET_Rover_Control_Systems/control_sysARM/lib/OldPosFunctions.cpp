// this is all the development control stuffff 
// this was not used in the end but it was kept as a refr 

// header stuff 
void posControl(void); 
void posControl2(void); 
void posControl3(void); 
// postion control 
void InverseKinP(float length, float angle1, float z, float gamma);
void ForwardKin(float q1, float q2, float q3, float q4, float *coords);
void FKMovement(float *jCurrent, float *j);
float AngleChange(float &desiredQ, float &previousQ, bool &done);
float EndEffectorX(float q1, float q2, float q3, float q4);
float EndEffectorY(float q1, float q2, float q3, float q4);
float EndEffectorZ(float q1, float q2, float q3, float q4);


// code 


#define PI 3.14159
float C = 180/PI;
float C2 =  PI/180;
float step = 3;
bool VALID =  true;

//length of the robotic links
float a2 = .32;
float a3 = .23;
float a4 = .1;


/*Calculates the required angle of each joint in order for the arm to reach the
	desired configuration
	lenght - distance of EE from the base in the XY plane (Plane parrallel to the ground)
	angle1 - angle of the end effector from the X axis (from the front of the rover)
	z 	- 	  the height of the EE in the Z axis
	gamma  - the angle of the EE from the XY plane
	joints - the array of robot joint angeles (4)
*/
void InverseKinP(float length, float angle1, float z, float gamma){
	//Contants to transfrom angles to and from degrees and radians
	float C = 180/PI;
	float C2 = PI/180;
	//end Effector Angle in radians
	float gammaP = gamma*C2;
	//Intermediate values
	float  dWx, dWz, d, alpha;
	float x = length*cos(angle1*C2);
	cout << "The value x: " << x << endl;
	float joints[4]; 
	//Pan
	joints[3] = angle1;
	dWx = x/cos(joints[3]*C2) - a4*cos(gammaP);
	dWz = z - a4*sin(gammaP);
	d = pow(dWx,2) +pow(dWz,2);
	alpha = atan2(dWz, dWx);
	/*
	cout << "The value dWx: " << dWx << endl;
	cout << "The value dWz: " << dWz << endl;
	cout << "The value d: " << d << endl;
	cout << "The value alpha: " << alpha << endl;
	*/
	//Calculate Angle of the 3rd joint
	float input = (pow(a2,2) + pow(a3,2) - d)/(2*a2*a3);


	if(input <= -1)//for some reason acos == nan when input =-1 so...
		input = -1;
	if(input >= 1)
		input = 1;
	//Elbow
	joints[1] = 180 - acos(input)*C;
		
	//Shoulder
	joints[0] = (alpha - acos((d+pow(a2,2)-pow(a3,2))/(2*a2*sqrt(d))))*C;
	//Wrist
	joints[2] = gamma - joints[1] - joints[0];

	if(isnan(joints[1]) || isnan(joints[2]) || isnan(joints[3])){
		VALID = false;
		cout << "This position is not configurable \n";
	}
	for(int n=0;n<4;n++){
		if(n==0){
			// shoulder
			SPAngle[n] = -joints[n];
		}else{
			SPAngle[n] = joints[n];
		} 
	}
}
/*Calculates the location of the EE in 3-D carteisan space, the first joint
which pans the arm is taken as the origin
	q1-q4 - the angles of the arm joints
	coords - represents the EE's location in 3-D space (XYZ)
*/

void ForwardKin(float q1, float q2, float q3, float q4, float *coords){
	//Contants to transfrom angles to and from degrees and radians

	q1 = q1*C2;
	q2 = q2*C2;
	q3 = q3*C2;
	q4 = q4*C2;
	//X
	coords[0] = -cos(q1)*(a2*sin(q2) + a3*sin(q2 + q3) + a4*sin(q2 + q3 + q4));
	//cout << "X:" << coords[0] << endl;
	//Y
	coords[1] = -sin(q1)*(a2*sin(q2) + a3*sin(q2 + q3) + a4*sin(q2 + q3 + q4));
	//cout << "Y:" << coords[1] << endl;
	//Z
	coords[2] = a2*cos(q2) + a3*cos(q2 + q3) + a4*cos(q2 + q3 + q4);
	//cout << "Z:" << coords[2] << endl;
}
/* Calculates the intermediate angles needed to move arm from its current configuration
to the desired configuration
	jCurrent - array of the current joint angles
	j 		- array of the desired joint angles

*/
void FKMovement(float *jCurrent, float *j){
	
	int numSteps;
	//Joint position status flage, false: still moving, true: done moving
	bool DONE1(false);
	bool DONE2(false);
	bool DONE3(false); 
	bool DONE4(false);
	//Find max difference between current and desired positions
	int diff[4];
	int maxDiff(0);
	for(int i = 0; i < 4; i++){
		diff[i] = ceil(abs(jCurrent[i]-j[i]));
	} 
	maxDiff = max(diff[0], diff[1]);
	maxDiff = max(maxDiff, diff[2]);
	maxDiff = max(maxDiff, diff[3]);

	numSteps = (maxDiff/(int)step);
	if (maxDiff%(int)step != 0){//doesn't divide evenly
		//cout << "Doesn't divide evenly \n\n";
		numSteps = numSteps + 2;
	}
	else{//Divides evenly, currently a problem
		//cout << "Divides evenly \n\n";
		numSteps++;
	}

	cout << "The number of steps is: " << numSteps << endl;

	//Array of intermediate joint angles and cartesian EE positions
	float q1[numSteps], q2[numSteps], q3[numSteps], q4[numSteps];
	float xE[numSteps], yE[numSteps], zE[numSteps];

	//Initialize first value of joint anges with crrent position of joint angles
	q1[0] = jCurrent[0];
	q2[0] = jCurrent[1];
	q3[0] = jCurrent[2];
	q4[0] = jCurrent[3];
	//Initialize first value of EE postion 
	xE[0] = EndEffectorX(q1[0], q2[0], q3[0], q4[0]);
    yE[0] = EndEffectorY(q1[0], q2[0], q3[0], q4[0]);
    zE[0] = EndEffectorZ(q1[0], q2[0], q3[0], q4[0]);

    cout << setprecision(3) <<"Start position - X: " << xE[0] << 
		" Y: " << yE[0] << " Z: " << zE[0] << endl;
    //Calculate all intermediate joint values
    int i = 0;
    while(!DONE1 || !DONE2 || !DONE3 || !DONE4){
    	i++;
    	q1[i] = AngleChange(j[0], q1[i-1], DONE1);
    	q2[i] = AngleChange(j[1], q2[i-1], DONE2);
    	q3[i] = AngleChange(j[2], q3[i-1], DONE3);
    	q4[i] = AngleChange(j[3], q4[i-1], DONE4);
    	xE[i] = EndEffectorX(q1[i], q2[i], q3[i], q4[i]);
    	yE[i] = EndEffectorY(q1[i], q2[i], q3[i], q4[i]);
    	zE[i] = EndEffectorZ(q1[i], q2[i], q3[i], q4[i]);
	}

	for(int j = 0; j < numSteps; j++){
		cout << setprecision(2) << "Joint 1: " << q1[j] << " Joint 2: " 
			<< q2[j] << " Joint 3: " << q3[j] << " Joint 4: " << q4[j] << endl;
	}
	cout <<endl;
	cout << setprecision(3) <<"Final Position - X: " << xE[i] << " Y: " << yE[i] << 
		" Z: " << zE[i] << endl;

	cout<<setprecision(4)<<"Final Joint Angles - "<< "Joint 1: " << q1[i] 
		<< " Joint 2: " << q2[i] << " Joint 3: " << q3[i] << " Joint 4: " 
		<< q4[i] << endl;

}
//Support functions
////////MIGHT NEED TO CHANGE DEPEDING ON ANGLE MAPPING
float AngleChange(float &desiredQ, float &previousQ, bool &done){
	//increment value
    float delta = step;
    float currentQ;
	if (done == false){
		if(abs(desiredQ -  previousQ) <= delta){
			currentQ = desiredQ;
        	done = true;
		}
    	else if(desiredQ > previousQ)
        	currentQ = previousQ + delta;
    	else if(desiredQ < previousQ)
        	currentQ = previousQ - delta;
    }
	else // if Done == 1
    	currentQ = previousQ;

    return currentQ;
}

//Calculate the XYZ-positions of the End-Effector
float EndEffectorX(float q0, float q1, float q2, float q3){
	float PositionX = cos(q3*C2)*(a2*cos(q0*C2) + a3*cos((q0 + q1)*C2)
		+ a4*cos((q0 + q1 + q2)*C2));
	return PositionX;
}

float EndEffectorY(float q0, float q1, float q2, float q3){
	float PositionY = sin(q3*C2)*(a2*cos(q0*C2) + a3*cos((q0 + q1)*C2)
		+ a4*cos((q0 + q1 + q2)*C2));
	return PositionY;
}

float EndEffectorZ(float q0, float q1, float q2, float q3){
	float PositionZ = a2*sin(q0*C2) + a3*sin((q0 + q1)*C2)
		+ a4*sin((q0 + q1 + q2)*C2);
	return PositionZ;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float Xpos1 = 0.65;
float Zpos1 = 0.0; 
void posControl(){
	// position controller 
	//oid InverseKinP(float length, float angle1, float z, float gamma){
	 Xpos1 += Xpos*0.01/2.5;
	 Zpos1 += Zpos*0.01/2.5; 
	
	InverseKinP(Xpos1,0.0,Zpos1,0.0);
}


void posControl2(){
	// update the arm postion 
	currentArmPos();
	float StepGain = 0.1; // how the joystick value corisponds to the value, so pushing the stick full forward will step the movement by StepGain cm in a cylce 
	// do angle setpoints 
	// use Xpos and Z pos as setpoints 
	// the following are current based on the joints angles 

	// angle def 
	// UP IS POSITIVE BITCHES 
	// ZERO IS EVERYTHING STICKING STRaight out parelle to ground 
	// each joint is a line from rotation joint to rotation joint 

//	cout<<"IAN POS CONTROL"<<endl; 
	 
	
	float MaxReach = sqrt(UpperArmL*UpperArmL+ForeArmL*ForeArmL)+15.0; 
	float XNew;
	float ZNew;

//	if(fabs(Xpos) < 0.01){
//		XNew = XCurrent + XChange;
//	}else{
		XNew = XCurrent - Xpos*2.0;
//	}

//	if(fabs(Zpos) < 0.01){
//		ZNew = ZCurrent + ZChange;
//	}else{
		ZNew = ZCurrent + Zpos*2.0;
//	}
	 
//	cout<<"ZPos: "<<Zpos<<" ZNew: "<<ZNew<<endl; 	 
	float NewLen = sqrt(XNew*XNew+ZNew*ZNew);
	if(NewLen>MaxReach){
		cout<<"ARM IS Being Moved to Illegal Postion, Will not move"<<endl; 
		cout<<"MaxReach: "<<MaxReach<<" NewLen: "<<NewLen<<endl; 
	}else{
		// move arm to new postion
		float alpha; // angle between line from shoulder to wrist and ground 
		if(XNew<1.8){
			// for small values of XNew, here tangent blows up, so lets not use it 
			alpha = 90.0 - atan(XNew/ZNew)*180.0/3.145826;
		}else{
			// this is when the tagent is not operating around +- 90 
			alpha = atan(ZNew/XNew)*180.0/3.1415926; 
		}

		if(XNew>0 && ZNew>0){
			// alpha is between 0 and 90
			alpha =  abs(alpha);
		}else if(XNew>0 && ZNew<0){
			// alpha is beteen 0 and -90 
			alpha = -1.0*abs(alpha);
		}else if(XNew<0 && ZNew>0){
			// alpha is between 90 and 180 
			alpha =  1.0*abs(alpha);
		}
		// beta is the angle that is formed between the line between the shoulder and the elbow joint and the  fore arm 
		float Beta = acos((ForeArmL*ForeArmL+UpperArmL*UpperArmL-NewLen*NewLen)/(2*UpperArmL*ForeArmL))*180.0/3.1415926; 
		// delta is the resulting angle in this triangle that is formed between the line from the shoulder to elbow, and the line from the shoulder to the wrist 
		float Delta = asin(sin(Beta*3.1415926/180.0)*ForeArmL/NewLen)*180.0/3.1415926;
//		cout<<"Alpha: "<<alpha<<" Beta: "<<Beta<<" Delta: "<<Delta<<endl;
		// should angle 
		SPAngle[0] = (-1.0)*(Delta+alpha);


		SPAngle[1] = (-1.0)*(180.0-Beta);
	}

//	cout<<"Moving Arm wrist Joint to [cm] X = "<<XNew<<" Z = "<<ZNew<<endl; 
	// do wrust angles 

	SPAngle[2] = WristAngle +  SPAngle[0] - SPAngle[1];
	SPAngle[3] = PanAngle + PanCurrent; 
//	cout<<"Setting Shoulder Angle: "<<SPAngle[0]<<" Elbow Angle: "<<SPAngle[1]<<" Wrist Angle: "<<SPAngle[2]<<endl;
//	cout<<"Desired Wrist angle: "<<SPAngle[2]<<"Wrist Angle: "<<EncoderAngles[2]<<endl; 


}