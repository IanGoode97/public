// test the spi calling from C++ 

#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <fstream>

#include "spiIAN.h" 

using namespace std; 

int main() {
	

	unsigned char testPass = 0x00; 

	unsigned char testReturn; 

		printf("Sent: %04x \n", testPass);
		testReturn = transferSPI(testPass);
		//printf("Called the transferSPI, back in main \n");
		printf("Returned: %04x \n",testReturn);
	

	cleanUpSPI(); 
	cout<<"DONE"<<endl; 

	return 0;
}
