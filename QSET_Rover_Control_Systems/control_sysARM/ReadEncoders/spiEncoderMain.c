// this is a test main to call the external spiEncoder function 

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>


// header file to incldue this lovely transfer function
#include "spiIAN.h" 


int main() {
	

	unsigned char testPass = 0x10; 

	unsigned char testReturn; 
	int n; 
	for(n = 0; n<500;n++){
		testReturn = transferSPI(testPass+n);
		//printf("Called the transferSPI, back in main \n");
		printf("Returned: %04x \n",testReturn);
	}

	cleanUpSPI(); 
	printf("Cleaned SPI\n");

	return 0;
}