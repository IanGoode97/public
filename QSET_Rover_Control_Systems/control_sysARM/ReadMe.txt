This is a read me for the arm control system written by Ian... 

This is a mess with a bunch of C++ and C files and header files in the lib folder of this folder. 

The main function is in: ArmControl.cpp

Complier notes: 

This uses std=c++14 for the majority of the program and I complied it with g++, for the encoders these are written in straight C so gcc is used. 
The only C program is a library written for the encoders. This has been tested to work well. The marjority of the testing of this library was actually done with my antenna test chamber, so in all likley hood the encoder errors are not comming from here but with how I dealt with them in the rest of the program. I tried to do as little as possible in the C file, hence why it does not specifiy a newer release of C in the complier. Most of the C code i write uses C99 so you can delcare index vars in forloops.  

The complier is set up to the way I like it, where you have a directory for soruce, a directory for build files (object files) and the parrent directory just contains the executable and the makefile and this lovely file. 
The raw files are in the lib dir and the build files are in the build dir


Encoder Notes: 

Since QSET, I found a better way to open and close the encoder call, this could easily be added to to be able to write a thing that tries to restart the encoder thread when there is a freeze. I do not want to implement this without the ability to complie and test, so we can look at this later but it would be a good idea if this is the ahrdware route that QSET is going to stick with. 

File Notes: 

This section discusses the lib dir 

 The files are genarlly split based on what they do and are named (at least to me) clearly. Anything that has Old or USEDForXXXX in the name means it is old and I wanted it easily avalaible for looking at since Im bad at GIT. 

In this dir there is a dir called MQTTwrap, this was used for the code that Valen wrote to deal with the MQTT stuff for 17/18. This discussion does nto focus on the MQTT crap but rather what it does to my code, and how you could get my code to do shit 

 There are four header files in this control system. The only one that should need to be touched is the one labled ControlSystemHeader

 What really needs done to this: 

 the controller itself needs optimized so files like Controller.cpp, PosController.cpp, SpeedController.cpp, and AngleController.cpp
 Need to be updated, it iselft is not good 

 The zeroing of encoders needs to be more user friendly, currently a seperate program was compiled that zeroed all encoders where they stand. I have since written a real time encoder zeroing program that I used on my chamber but this has not been updated as seen before. 

 The automated mode switching could be improved to remove the number of modes the user needs to change between. 

 Most of this code was a shit show and is bad C++ so if it can be written better then go for it. 


Stuff that Should be Stable-ish or you Should Get Me to Change since I have written it: 

Abs Encoders get me to change 
Speed encoders leave unless you are updated to libphidget newer than 21 
As long as you keep Sabertooths leave the serial writer 
The main file called ArmControl.cpp is probably okay it just handles mode chagnes and calls things accordinally. The code is written poorly but the logic is functional ish. 

Interesting Things: 

In the files OldAngleControl and OldPosControl there are non linear control systems that were never tuned, I am told these would work better. So be warned. 

Bad Assumptions I made: 

For the pos control I assumed that the elbow angle would never be inverted. Inverted as defined from the oposite way it is in in the rest postion. This made writing this easier but there were a few cases where if we could have done that it would ahve helped. 

Cooridinate System:

Every link was defined as a straight line from its rotation axis from one end to another, so the upper arm was a straight line ignoring the kink. The zero position or angle for each joint was then defined as a the arm pointing straight forward (for the pan angle) and then the shoulder, elbow and wrist were all set so the arm was a striaght line parrelele to the axis of the rover. Angles were defined as a joints angle from the previouss angle axis. With the shoulder angle being defined as the upper arm's angle from the rover axis. The elbow angle was the forearms angle relative to the upper arm, and then the wrist angle was the wrist joitn relative to the forearm. "Up" from thsi point was defined as positive. 

Because of the layout of the arm the shoulder encoder was backwards compared to the elbow and wrist encoders, thus it had to be flipped in code to match this up is positive. 


GLOBAL VARS: 

I use a lot of global vars 
As a note look at the header file to see what is actuall global, but if you are in a file other than the main file and there is an out of function var declared it is likley only a global for that file. 