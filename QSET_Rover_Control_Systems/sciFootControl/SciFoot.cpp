////Wheel control system // This is run in C++ // Use Mosquito for IO with JS // values that we will bring in 
//are left and right set points from the joy stick // This will do the two sabertooths for the wheel motors and 
//the one for the linear actuator and x stage

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// IMPORT 

#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <fstream>
#include <time.h>   // time calls
#include <thread> // for multi thread 
#include <math.h>
#include "MQTTwrap/MQTT.h"
#include "json.hpp"
#include <wiringPi.h> 
#include <signal.h>
using namespace  std;
using json = nlohmann::json;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Linke Local Headers to Other Files 
#include "ControlSystemHeader.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Global Variables (Extern, make sure Header file relates to this)
# define StopCommand 64
# define MaxWheelSpeed 250 // max val the encoder can read 
	//int MotorAddress = {{130, 2},{130, 1},{128, 2},{128, 1}};
int MotorAddress[][2] = {{129, 6},{128, 6},{130, 6},{128, 7},{132,7},{132,6}}; // list of the motor address 
/*
	0 Tilt 
	1 Chain 
	2 Sensor Dick 
*/
float MotorSpeed[]={64,64,64,64,64,64}; // list of current motor speeds set through serial 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/
float SetPoints[3]; 
string MotorNames[]={"Tilt","Chain","Sensors Dick"};
int GlobalRun =1;
int ControlType = 0; 
/*
	- 0 Drive Everything as a speed 
	- 1 Up/Down the Sensors 
	- 2 standby 
	- 3 estop 
*/
// global MQTT thing so we can emit from anywhere ;)
MQTT* broker;

int sensorPos =1; // 1 is up, 0 is down 

int HomeCount = 0; // counter for homign process 

void messageHandler(string topic, string message){
	cout<<"on "<<topic<<" got "<<message<<endl;	
	if(topic.compare("estop")==0){
		for(int n=0; n<6;n++){
			MotorSpeed[n] = 64; 
			doSerial(n); 
		}
		ControlType = 2; 
	}
	if(topic.compare("sciFoot")==0){

		auto data = json::parse(message);

		float tilt = data["tilt"].get<float>();
		float chain = data["chain"].get<float>();
		float sensor = data["sensor"].get<float>();
//		float sensors = data["sensors"].get<float()>;
		// left wheels 
		SetPoints[0] = tilt;
		SetPoints[1] = chain; 
		SetPoints[2] = sensor; 
	}else if(topic.compare("sensor")==0){
		auto data = json::parse(message);
		sensorPos = data["dir"].get<int>();
		ControlType = 1; 
	}
	if(topic.compare("sciFootMode")==0){
		// change the sci foot mode 
		int newMode = stoi(message); 

		ControlType = newMode; 
//		PreArmControlMode = ControlType; // this is used to remember the state before we change it when we go into agnle control. 
		cout<<"Switched MODE !!!!, now on MODE: "<<ControlType<<endl; 
		if(ControlType ==4){
			// we are going into standby, give an estop first
			cout<<"Entering Standby Sent and Estop to Be Safe"<<endl;
			for(int n=0; n<6;n++){
				MotorSpeed[n] = 64; 
				doSerial(n); 
			}
		}else if(ControlType ==2){
			// we are using the home the sensor thing so reset the counter for that 
			HomeCount = 0; 
		}
	}

}


void updateInputs(){
	

	broker = new MQTT("QSET-SCIFOOT","192.168.1.21", 1883);

	broker->emit("system", "Controls Online");

	broker->on("sciFoot", &messageHandler);
	broker->on("estop",&messageHandler);
	broker->on("sensor",&messageHandler); 
	broker->on("sciFootMode",&messageHandler); 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int LimitSwitchStatus = 0; // 0 means no press, 1 means press 
int WaitBetweenLoop = 50*1000; // microseconds 
void limitSwtich(void){
	// read the limit switch 
	// setup the limit switch 
	int SwitchPin = 7; 
	wiringPiSetup(); 
	pinMode(SwitchPin,INPUT); 
	int LastSwitchStatus = digitalRead(SwitchPin); 
	int SwtichStatus = LastSwitchStatus; 
	int Count = 0; 
	while(GlobalRun==1){
		SwtichStatus = digitalRead(SwitchPin);
		// debounce the output based on loops 
		if(SwtichStatus!=LastSwitchStatus){
			// the switch state was changed 
			Count = 0; 
		} 
		// check to see how many times we have looped
		if(Count>2){
			// this means the swithc has been in one place for a given amount of time 
			LimitSwitchStatus = SwtichStatus; 
		}
		// set the last switch status 
		LastSwitchStatus = SwtichStatus; 

		usleep(WaitBetweenLoop/5);
		Count++; // step the count 
	}
	// clean up the limit switch 
	// should already be cleaned up 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void controlLoop(void){
	// the actual control loop that calls the updates and control system 
	while(GlobalRun==1){
		int Gain = 30; 
		// call wheel control system 
		/*
			- 0 Drive Everything as a speed 
			- 1 Up/Down the Sensor
			- 2 home the sensor 
			- 3 estop 
			- 4 standby
		*/
		cout<<"Control Loop, in mode: "<<ControlType<<endl;
		if(ControlType == 3){
			// estop 
			for(int n = 0;n<6;n++){
				MotorSpeed[n] = 64; 
				doSerial(n);
			}
		}else if(ControlType ==0){
			// Drive Stuff 
			for(int n = 0;n<3;n++){
				if(n==0){
					// allow you switch the direction 
					MotorSpeed[n] = 64 - 63*SetPoints[n];
				}else{
					MotorSpeed[n] = SetPoints[n]*63 + 64;
				}
				if(MotorSpeed[n]>127){
					MotorSpeed[n] = 127;
				}else if(MotorSpeed[n]<0){
					MotorSpeed[n] = 0;
				}
				doSerial(n); 
				cout<<"For "<<MotorNames[n]<<" wrote speed "<<MotorSpeed[n]<<endl;
			}

		}else if(ControlType == 1){
			// UP / Down the sensors
			// sensorPos:  1 is up 0 is down 

			// these are the directions for the mmotor depending which way we want to go 
			int DirMod; 
			

			if(sensorPos==1){
				DirMod = 1;
			}else{
				DirMod = -1; 
			}

			if(LimitSwitchStatus ==0){
				// this means the swithch is not pressed so we want to go up untill the swith is pressed 
				MotorSpeed[2] = 64 + DirMod*Gain; 
			}else{
				//  a swithc is pressed. We need to figure out which is pressed 
				cout<<"Sensor at bottom or top"<<endl; 

			}
			if(MotorSpeed[2]>127){
				MotorSpeed[2] = 127;
			}else if(MotorSpeed[2]<0){
				MotorSpeed[2] = 0;
			}
			doSerial(2);
		}else if(ControlType ==2){
			// home the sensor dick 
			if(LimitSwitchStatus==1){
				// one of the switches is pressed 
				// if we go up a bit and the switch is still pressed we are probably at the top 
				if(HomeCount<3){
					// we have tried this less than 3 times 

					// ask the sensors to go up 
					MotorSpeed[2] = 64 + Gain;
					// step the coutner 
					HomeCount++; 
				}else{
					// if the swithc is depressed and we ahve tried for the specififed number of home coutns to move up, we are at the top 
					MotorSpeed[2] = 64; // stop the motor 
					cout<<"Sensor Homed, going to StandBy"<<endl; 
					ControlType = 4; 
				}
			}else{
				// the switch is off 
				// this means we can just move the thign up 
				MotorSpeed[2] = 64 + Gain; 
				
			}
			if(MotorSpeed[2]>127){
				MotorSpeed[2] = 127;
			}else if(MotorSpeed[2]<0){
				MotorSpeed[2] = 0;
			}
			doSerial(2); 
		}else{
			// Stand by 
			// double wait time 
			usleep(WaitBetweenLoop); 
		}		
		// sleep between loops 
		usleep(WaitBetweenLoop); 
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void cleanKill(int signum){
	cout<<"Clean Kill Func Value: "<<signum<<endl; 
	if(signum ==SIGINT){
		cout<<"Handle Clean Exit"<<endl;		
		for(int n = 0;n<6;n++){
			MotorSpeed[n] = 64; 
			doSerial(n);
		}
		// try to joing the abs encoders so they dont get killed half calked 
		GlobalRun = 0; 
		cout<<"Set GlobalRun to 0 all while loops should stop"<<endl; 
	//	t[2].join(); // the thread should stop and join before we leave. 
//		t[4].join(); // join the control looops
	//	t[3].join(); // join the fucker check
		// the MQTT thread, and the speed encoder threads are not joined in this way since they are not obviously loops 


		// should go back to main thread now as the controlLoop should fuck off 
	}
}

int main(void){
	signal(SIGINT,cleanKill);
	// main function that does setup 
	cout<<"Spawned C++ SCI FOOT  Control System"<<endl;
	//setup serial 
	setupSerial(); 

	
	// zero all motors 
	for(int n=0; n<6;n++){
		// call do serial function to write serial values 
		MotorSpeed[n] = StopCommand; 
		doSerial(n); 
	}

	GlobalRun = 1; 
	thread t1(&updateInputs); 
	thread t2(&limitSwtich); 
	controlLoop();
	delete broker; // get rid fo the broker 
	t2.join(); 
	cout<<"Joined the Limit Switch Thread"<<endl; 
	t1.join(); 
	cout<<"Joined the MQTT Thread"<<endl; 
	cout<<"Joined all joinable threads"<<endl;
	usleep(15*1000);
	//tidyUpSPI();
	cout<<"Clean Exit of fuck ya baud"<<endl;
 
	cout<<"Clean Exit"<<endl; 
}
