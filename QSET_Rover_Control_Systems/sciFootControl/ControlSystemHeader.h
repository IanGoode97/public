// Control System header

//////////////////////////////////////
// GLOBAL FUNCTIONS 

int doSerial( int); // the first int is the device, the second int is the speed value 
void setupSerial(void); // setup serial command 
void runEncoders(void); // the encoder function (only call once)


///////////////////////////////////////////////////////////////////////////////////////////////////////

// External Vars to be Shared over all files 

extern int MotorAddress[6][2]; // list of the motor address 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/
extern float MotorSpeed[6]; // list of current motor speeds set through serial 

extern int GlobalRun; // global run var 