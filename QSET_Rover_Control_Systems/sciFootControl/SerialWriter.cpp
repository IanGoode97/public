#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <time.h>   // time calls

#include "ControlSystemHeader.h"

using namespace  std;

int fdGlobal; // glaobal fd;  

int open_port(void)
{
	int fd; // file description for the serial port
	
	fd = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);
//	unsigned char sendBytes[] = {0x34};
//	write(fd,sendBytes,1); 
	if(fd == -1) // if open is unsucessful
	{
		//perror("open_port: Unable to open /dev/ttyS0 - ");
		printf("open_port: Unable to open /dev/ttyS0. \n");
	}
	else
	{
		fcntl(fd, F_SETFL, 0);
		printf("port is open.\n");
	}
	
	return(fd);
} //open_port

int configure_port(int fd)      // configure the port
{
	struct termios port_settings;      // structure to store the port settings in

	cfsetispeed(&port_settings, B9600);    // set baud rates
	cfsetospeed(&port_settings, B9600);

	port_settings.c_cflag &= ~PARENB;    // set no parity, stop bits, data bits
	port_settings.c_cflag &= ~CSTOPB;
	port_settings.c_cflag &= ~CSIZE;
	port_settings.c_cflag |= CS8;
	
	tcsetattr(fd, TCSANOW, &port_settings);    // apply the settings to the port
	return(fd);

} //configure_port

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setupSerial(void){
	int fd; 
	fd = open_port();
	configure_port(fd);

	unsigned char SetupByte = 170; // change this to corrispond to the right bit rate adn crap 
	unsigned char setupBytes[] = {SetupByte}; 
	write(fd,setupBytes,1); // write the one setup byte
	// set the global fd after setup 
	fdGlobal = fd; 
	cout<<"Setup Serial"<<endl; 
	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int doSerial(int Index){ 
		
	int fd = fdGlobal; 
	unsigned char MoAd = MotorAddress[Index][0];
	unsigned char MoSel = MotorAddress[Index][1];
	unsigned char MoSp = (int)MotorSpeed[Index];
	unsigned char check = (MoAd + MoSel + MoSp) & 127;
	unsigned char sendBytes[]={MoAd, MoSel, MoSp, check};
	//cout<<"Sending: [" << (unsigned int)sendBytes[0] << ", " << (unsigned int)sendBytes[1] << ", "<< (unsigned int)sendBytes[2] << ", " << (unsigned int)sendBytes[3] << "]" <<endl;
	write(fd,sendBytes,4); 

} 
