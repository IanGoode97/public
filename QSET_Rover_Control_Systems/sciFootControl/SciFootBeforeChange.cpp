////Wheel control system // This is run in C++ // Use Mosquito for IO with JS // values that we will bring in 
//are left and right set points from the joy stick // This will do the two sabertooths for the wheel motors and 
//the one for the linear actuator and x stage

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// IMPORT 

#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <fstream>
#include <time.h>   // time calls
#include <thread> // for multi thread 
#include <math.h>
#include "MQTTwrap/MQTT.h"
#include "json.hpp"

using namespace  std;
using json = nlohmann::json;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Linke Local Headers to Other Files 
#include "ControlSystemHeader.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Global Variables (Extern, make sure Header file relates to this)
# define StopCommand 64
# define MaxWheelSpeed 250 // max val the encoder can read 
	//int MotorAddress = {{130, 2},{130, 1},{128, 2},{128, 1}};
int MotorAddress[][2] = {{130, 7},{128, 6},{130, 6},{128, 7},{132,7},{132,6}}; // list of the motor address 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/
float MotorSpeed[]={64,64,64,64,64,64}; // list of current motor speeds set through serial 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/

string MotorNames[]={"Chain","Tilt","Sensors"};
int GlobalRun =1;

// global MQTT thing so we can emit from anywhere ;)
MQTT* broker;


void messageHandler(string topic, string message){
	cout<<"on "<<topic<<" got "<<message<<endl;	
	if(topic.compare("estop")==0){
		for(int n=0; n<6;n++){
			MotorSpeed[n] = 64; 
			doSerial(n); 
		}
	}
	if(topic.compare("sciFoot")==0){

		auto data = json::parse(message);

		int tilt = data["tilt"].get<int>();
		int chain = data["chain"].get<int>();
		int sensor = data["sensor"].get<int>();
//		float sensors = data["sensors"].get<float()>;
		// left wheels 
		MotorSpeed[0] = tilt;
		MotorSpeed[1] = chain; 
		MotorSpeed[3] = sensor; 

		for(int n=0;n<3;n++){
			doSerial(n); // write the stuff 
		}
	}

}


void updateInputs(){
	

	broker = new MQTT("QSET-SCIFOOT", "localhost", 1883);

	broker->emit("system", "Controls Online");

	broker->on("sciFoot", &messageHandler);
	
	

}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void controlLoop(void){
	// the actual control loop that calls the updates and control system 

	int WaitBetweenLoop = 50*1000; // microseconds 
	while(GlobalRun==1){
		// call wheel control system 
		
	
		usleep(WaitBetweenLoop); 
//		cout<<"Control Loioping"<<endl; 
	}
	// if we get a clean exit zero everything 
	for(int n=0;n<4;n++){
		MotorSpeed[n] =  64.0; 
		doSerial(n); 
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void){
	// main function that does setup 
	cout<<"Spawned C++ SCI FOOT  Control System"<<endl;
	//setup serial 
	setupSerial(); 

	
	// zero all motors 
	for(int n=0; n<6;n++){
		// call do serial function to write serial values 
		MotorSpeed[n] = StopCommand; 
		doSerial(n); 
	}


	updateInputs(); 
	
 
	cout<<"Clean Exit"<<endl; 
}
