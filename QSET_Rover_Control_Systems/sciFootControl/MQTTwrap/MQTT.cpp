#include "MQTT.h"
#include <iostream>

using namespace std;

//int (*callback_on_message(std::string, std::string));

/**
* Constructor
*/
MQTT::MQTT(const char *id, const char* host_, int port_) : mosquittopp(id), host(host_), port(port_), callback_on_message(NULL) {
    mosqpp::lib_init();
    //cout<<"this->mqpp:"<<this->mosquittopp<<endl;
    int keepAlive = 10;


    connect_async(host.c_str(), port, keepAlive);
    if(loop_start() != MOSQ_ERR_SUCCESS){
        cout<<"FAILED TO START MQTT LOOP"<<endl;
    }
}

/**
* Destructor
*/
MQTT::~MQTT(){
    cout<<"ENDING MQTT SESSION"<<endl;
    if(disconnect() != MOSQ_ERR_SUCCESS){
        cout<<"ERROR DISCONNECTING"<<endl;
    }

    if (loop_stop() != MOSQ_ERR_SUCCESS) {
        std::cout << "loop_stop failed" << std::endl;
    }

    mosqpp::lib_cleanup();

}


void MQTT::emit(string topic, string msg){
    int status = publish(NULL, topic.c_str(), msg.size(), msg.c_str(), 1, false);
    if(status != MOSQ_ERR_SUCCESS){
        cout<<"Message publish failure"<<endl;
    }

}

void MQTT::on(string topic, CallbackFunc callback){
    int status = subscribe(NULL, topic.c_str());
    if(status!=MOSQ_ERR_SUCCESS){
        cout<<"Subscription Error!"<<endl;
    } else {
        cout<<"Sunbscribed to: "<<topic<<endl;
    }
    this->callback_on_message = callback;
}

//PRIVATE:
//--------

void MQTT::on_connect(int resp_code){
    cout<<"MQTT connected with code "<<resp_code<<endl;
}

void MQTT::on_publish(int msg_id){
    cout<<"Message with id "<<msg_id<<" published"<<endl;
}

void MQTT::on_message(const mosquitto_message* msg){
    string topic = msg->topic;
    string message = reinterpret_cast<char*>(msg->payload);
    cout<<"Message received: "<< message <<" in topic "<< topic <<endl;
    if(this->callback_on_message){
        (this->callback_on_message)(topic, message);
    }
}