// - Encoder simple -
// This example simply creates an Encoder handle, hooks the event handlers, and then waits for an encoder is attached.
// Once it is attached, the program will wait for user input so that we can see the event data on the screen when using the
// encoder.
//
// Copyright 2008 Phidgets Inc.  All rights reserved.
// This work is licensed under the Creative Commons Attribution 2.5 Canada License. 
// view a copy of this license, visit http://creativecommons.org/licenses/by/2.5/ca/

/*
	IAN COMMENT 

	This file was taken from the phidget 21 example files and was modified for our use 
	this file by default loops until the Cleanup phidget function is called 
	for this function call see the end of the main ControlSystem.cpp file 
	The function that was edited for our needs was Postion Change Handler 
	this function runs whenever an encoder changes position 

	In here we modifiy those ticks to be a wheel speed and also use the front two wheels to get step control 


*/

#include <stdio.h>
#include <phidget21.h>
#include <string.h>
#include "ControlSystemHeader.h"
#include <stdlib.h>

float RateOld[] = {0,0,0,0}; 

int AttachHandler(CPhidgetHandle ENC, void *userptr)
{
        int serialNo;
        CPhidget_DeviceID deviceID;
        int i, inputcount;

        CPhidget_getSerialNumber(ENC, &serialNo);

        //Retrieve the device ID and number of encoders so that we can set the enables if needed
        CPhidget_getDeviceID(ENC, &deviceID);
        CPhidgetEncoder_getEncoderCount((CPhidgetEncoderHandle)ENC, &inputcount);
        printf("Encoder %10d attached! \n", serialNo);

        //the 1047 requires enabling of the encoder inputs, so enable them if this is a 1047    
        if (deviceID == PHIDID_ENCODER_HS_4ENCODER_4INPUT)
        {
                printf("Encoder requires Enable. Enabling inputs....\n");
                for (i = 0 ; i < inputcount ; i++)
                        CPhidgetEncoder_setEnabled((CPhidgetEncoderHandle)ENC, i, 1);
        }
        return 0;
}


int DetachHandler(CPhidgetHandle ENC, void *userptr)
{
	int serialNo;
	CPhidget_getSerialNumber(ENC, &serialNo);
	printf("Encoder %10d detached! \n", serialNo);

	return 0;
}

int ErrorHandler(CPhidgetHandle ENC, void *userptr, int ErrorCode, const char *Description)
{
	printf("Error handled. %d - %s \n", ErrorCode, Description);

	return 0;
}

int InputChangeHandler(CPhidgetEncoderHandle ENC, void *usrptr, int Index, int State)
{
	printf("\"Input\": #%i - \"State\": %i \n", Index, State);

	return 0;
}

int PositionChangeHandler(CPhidgetEncoderHandle ENC, void *usrptr, int Index, int Time, int RelativePosition)
{
	int Position;
	CPhidgetEncoder_getPosition(ENC, Index, &Position);
///	char Indexstr[10];
//	if(Index==0){
//		strcpy(Indexstr, "Enc0");
//	}
//	else if(Index==1){
//		strcpy(Indexstr, "Enc1");
//	}
//	else if(Index==2){
//		strcpy(Indexstr, "Enc2");
//	}
//	else if(Index == 3){
//		strcpy(Indexstr, "Enc3");
//	}
	//sprintf(Indexstr, "%d", Index);
//	FILE *wfd = fopen(Indexstr,"w");

//	printf("\"Encoder\": #%i -\"Position\": %5d --\"Relative Change\": %2d -- \"Elapsed Time\": %5d \n", Index, Position, RelativePosition, Time);
	int CountsPerRev = 512;
//	float GearReduction = 1;
	float Change = (float)RelativePosition/(float)Time;
	float Rate = (float)Change*(float)1000000/(float)CountsPerRev; ///(CountsPerRev*GearReduction);
	// take the rverse of the right wheels 
	if (Index ==1  || Index ==2){
		//take negative
		Rate = -Rate;
	}
	// count the number of steps 

	// since the step control uses the front two encoders if either of these die we will not have a working step control system 
	

	float StepCal = 140.0;
	if(Index==0){
		// update the step counter for the front left wheel 
		WheelStepENCL += (float)RelativePosition/(float)CountsPerRev/(StepCal); // should give you number of wheel rotations for this chooch of the encoder reader
//		printf("Wheel pos change was: %0.2f \n",RelativePosition); 
		//printf("\"Encoder\": #%i -\"Position\": %5d --\"Relative Change\": %2d -- \"Elapsed Time\": %5d \n", Index, Position, RelativePosition, Time);

	}

	if(Index==1){
		// update the step counter for the front right wheel 
		WheelStepENCR -= (float)RelativePosition/(float)CountsPerRev/(StepCal); // should give you number of wheel rotations for this chooch of the encoder reader
//		printf("Wheel pos change was: %0.2f \n",RelativePosition); 
		//printf("\"Encoder\": #%i -\"Position\": %5d --\"Relative Change\": %2d -- \"Elapsed Time\": %5d \n", Index, Position, RelativePosition, Time);

	}
//	printf("{\"index\":%i, \"rate\":%0.5f}\n",Index,Rate);
	//fprintf(wfd, "{\"Index\":%i, \"Speed\":%0.5f}\n",Index,Rate);
//	fprintf(wfd, "%0.5f",Rate);
//	fclose(wfd);

	EncoderValues[Index] = (Rate+RateOld[Index])/2.0; // this was done to try and do some averagin to try and smoth out the values 
	RateOld[Index] = Rate;  // set rate to the old rate . 
	return 0;
}

//Display the properties of the attached phidget to the screen.  We will be displaying the name, serial number and version of the attached device.
//Will also display the number of inputs and encoders on this device
int display_properties(CPhidgetEncoderHandle phid)
{
	int serialNo, version, num_inputs, num_encoders;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetEncoder_getInputCount(phid, &num_inputs);
	CPhidgetEncoder_getEncoderCount(phid, &num_encoders);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n", serialNo, version);
	printf("Num Encoders: %d\nNum Inputs: %d\n", num_encoders, num_inputs);

	return 0;
}
//Declare an encoder handle
CPhidgetEncoderHandle encoder = 0;
void runEncoders(void)
{
	int result;
	const char *err;

	

	//create the encoder object
	CPhidgetEncoder_create(&encoder);

	//Set the handlers to be run when the device is plugged in or opened from software, unplugged or closed from software, or generates an error.
	CPhidget_set_OnAttach_Handler((CPhidgetHandle)encoder, AttachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)encoder, DetachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)encoder, ErrorHandler, NULL);

	//Registers a callback that will run if an input changes.
	//Requires the handle for the Phidget, the function that will be called, and an arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetEncoder_set_OnInputChange_Handler(encoder, InputChangeHandler, NULL);

	//Registers a callback that will run if the encoder changes.
	//Requires the handle for the Encoder, the function that will be called, and an arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetEncoder_set_OnPositionChange_Handler (encoder, PositionChangeHandler, NULL);

	CPhidget_open((CPhidgetHandle)encoder, -1);

	//get the program to wait for an encoder device to be attached
	printf("Waiting for encoder to be attached....");
	if((result = CPhidget_waitForAttachment((CPhidgetHandle)encoder, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return ;
	}

	//Display the properties of the attached encoder device
	display_properties(encoder);

	//read encoder event data
	printf("Reading.....\n");
/*
	//keep displaying encoder data until user input is read
	printf("Press any key to end\n");
	getchar();

	//since user input has been read, this is a signal to terminate the program so we will close the phidget and delete the object we created
	printf("Closing...\n");
	CPhidget_close((CPhidgetHandle)encoder);
	CPhidget_delete((CPhidgetHandle)encoder);
	printf("Closed\n");

	//all done, exit
	exit(0);
	*/
}
/*
int main(int argc, char* argv[])
{
	encoder_simple();
	return 0;
}
*/
void cleanPhidget(void){
	printf("Closing Phidget...\n");
	CPhidget_close((CPhidgetHandle)encoder);
	CPhidget_delete((CPhidgetHandle)encoder);
	printf("Closed Phidget\n");
	exit(0);
}
