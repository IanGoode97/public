////Wheel control system // This is run in C++ // Use Mosquito for IO with JS // values that we will bring in 
//are left and right set points from the joy stick // This will do the two sabertooths for the wheel motors and 
//the one for the linear actuator and x stage

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// IMPORT 

#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <fstream>
#include <time.h>   // time calls
#include <thread> // for multi thread 
#include <math.h>
#include "MQTTwrap/MQTT.h"
#include "json.hpp"
#include <signal.h>
using namespace  std;
using json = nlohmann::json;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Linke Local Headers to Other Files 
#include "ControlSystemHeader.h"

//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Global Variables (Extern, make sure Header file relates to this)
# define StopCommand 64
// the following line is used to control what wheel speed a value of 1 corrisponds too 
// the units on this are random ish this is just a random units reeally its approximatly the revs per second of the motor rotor 
// the motors can go faster, the max is actually about 360 however the rover was a bit less easy to control. 
// sometimes when people will want the rover to go fast they will turn this number up to a value larger than the motors can possibly go
// these people are stupid, that just makes the controller saturate way much fast if you want  it to go faster tunr in up to 360 ish 
# define MaxWheelSpeed 250 // max val the encoder can read 
	//int MotorAddress = {{130, 2},{130, 1},{128, 2},{128, 1}};
// the setup i left this in had 3 sabertooths, each was labled in pencil on the Al for the address, 6 and 7 corrispond to the Motor A or B, read the sabertooth documentation for more deets

int MotorAddress[][2] = {{128, 7},{128, 6},{130, 6},{130, 7},{132,6},{132,7}}; // list of the motor address 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/
// this array should be an int, why is it a float? I dunno, It should be an int. 
// this array holds the values that were last set / are to be sent to the sabertoth and are delath with in the do Serial functions 
float MotorSpeed[]={64,64,64,64,64,64}; // list of current motor speeds set through serial 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/
// the following holds the setpoints that are brought in from the Outside wrold 
float SetPoints[]={0,0,0,0}; // value between -1 and 1, 0 is stop
/*
	0 --> LEFT Wheels 
	1 --> RIGHT Wheels 
	2 --> XStage
	3 --> Linear Actuator  
*/
float SPSpeed[4]; // this is a repeat of the setpoints array it was useful for when traction control was being tried 
float EncoderValues[4]; // values of the encoders 
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
*/

int GlobalRun; // global run var 


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Local Global Variables
// the following previous arrays were set up to give the controller the ability to look at historical things 
float PreviousSetPoints[4]; // previous vals of setpoints 
/*
	0 --> LEFT Wheels 
	1 --> RIGHT Wheels 
	2 --> XStage 
	3 --> Linear Actuator  
*/
int PreviousSpeeds[6]; // previous speeds array Serial values
/*
	0 --> Front Left 
	1 --> Front Right 
	2 --> Back Right 
	3 --> Back Left 
	4 --> X Stage 
	5 --> Linear Actuator 
*/
int WheelMode = 0; // hold the value of the wheel mode
/*
	- 0 Is normal 
	- 1 is step 
	- 2 is traction control 
	- 3 is straight voltage control ie. shit is fucked 
*/
// default controller gains 
// these were tuned using magic, so ya 
// there is a function here to adjust these on the fly through an mqtt call 
// realistically the way this is written the proportional term almost acts as a hybrid term so the ki just increases this 
// since there was no filter the derivative was very harsh hence the small value of kd 
// this should be done to help prevent the little bit of overshoot you see under hard acceleration 
// to do this there should be some kind of filter to get rid of the high freq terms going into the derivative and smear the square 
// wave into a smooth curve 
float kp = 0.3;
float ki = 0.05; 
float kd = 0.001;
// the following was used to deal with the step control 
float CumStep2GoL = 0.0; // <--Cum like Kum and Go but more like the other kind  
float CumStep2GoR = 0.0; // same but for the right side 

// global MQTT thing so we can emit from anywhere ;)
MQTT* broker;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float Step2GoL; // set to zero to stop the thing 
float Step2GoR; // set to zero to stop the thing 
float WheelStepENCL = 0; // wheel steps from the encoders
float WheelStepENCR = 0; // wheel steps from the encoders
int doStepControl = 0; 
float WheelStepL; // made global 
float WheelStepR; // right wheel step 
int EncFailure = 0;
int EncFailureList[4];// = {2, 3}; 
int PreWheelMode; 

void messageHandler(string topic, string message){
	cout<<"on "<<topic<<" got "<<message<<endl;	
	if(topic.compare("estop")==0){
		// if the topic estop was sent this would run, it did not matter what the message was 
		// this just writes each motor to a voltage of zero 
		for(int n=0; n<6;n++){
			MotorSpeed[n] = 64; 
			SetPoints[n] = 0.0;
			WheelMode = 4; // standby
			doSerial(n); 
		}
	}
	if(topic.compare("wheels")==0){
		// this is the main wheel topic. It sets a set speed for the left and right wheels. Here the setpoints array is the desired speed of each wheel from -1 to 1
		// 1 being full forward 

		auto data = json::parse(message);

		float right = data["right"].get<float>();
		float left = data["left"].get<float>();
		// left wheels 
		SetPoints[0] = left;
		SetPoints[3] = left;
		// right wheels 
		SetPoints[1] = right;
		SetPoints[2] = right;
		cout<<"Set SetPoints Left:"<<left<<" right:"<<right<<endl;
	}
	if(topic.compare("wheelsStep")==0){
		// do wheel step things 
		// add in stuff to do a number of wheel rotations 
		// this means we need to do a wheel step 
		auto data1 = json::parse(message);
		float rotationsL = data1["rotationsL"].get<float>();
		float rotationsR = data1["rotationsR"].get<float>();
		
		WheelStepL = rotationsL; 
		WheelStepR = rotationsR; 
		cout<<"Rotations L read in "<<rotationsL<<" rotation R "<<rotationsR<<endl;
		
		doStepControl = 1; // this means do Step Control 
		// zero the wheel step counter
		WheelStepENCL = 0;  
		WheelStepENCR = 0; 
		// zero the cum errors
		CumStep2GoL = 0.0;
		CumStep2GoR = 0.0; 
		// put wheel mode to step mode 
		PreWheelMode = WheelMode; // store the previous wheel mode 
		WheelMode = 1; // drop us in to step cotnrol 
	}
	if(topic.compare("wheelModeChange")==0){
		// do a wheel mode change 
		//auto data = json::parse(message);
		int newMode = stoi(message); 
		WheelMode = newMode;
//		WheelMode = data["WheelMode"].get<int>(); 
		

	}
	// the following is used to control the linear actuatuarer

	if(topic.compare("auxControl")==0){
		// do the aux control 
		// for 2018 all this ran was the lin act to adjust the suspension 
		auto data = json::parse(message);
		MotorSpeed[4] = data["linAct"].get<int>(); 
		cout<<"writing  a value to lin act of" <<MotorSpeed[4]<<endl; 
		doSerial(4); 
	
	}
	//the following is used to modify the controller gains 
	if(topic.compare("changeGain")==0){
		// change the contrller gains 
		// super useful, this was never used, possibly does not even work, but ya fuck this one 
		auto data = json::parse(message);
		kp = data["kp"].get<float>();
		ki = data["ki"].get<float>();
		kd = data["kd"].get<float>();

		cout<<"Wheel Control System has Updated Contoller Gains"<<endl; 
	}

	// encoder failure 
	if(topic.compare("wheelEncFailure")==0){
		// this works real good, you just send it a value of which encoder is fucked and it ignores it, this has been tested and seems to work 
		EncFailure++; // increase the number of failed encoders 
		auto data = json::parse(message); 
		EncFailureList[EncFailure-1] = data["newFailedEnc"].get<int>(); // appends the broekn encoder number to the list 
		// this list of encoders is 0 through three which is front left to back left in a clockwise circle 
		cout<<"Added Encoder: "<<EncFailureList[EncFailure-1]<<" to failed encoder list"<<endl; 

	}

	// turn on done lights for autonomous 
	// uses an extra port on the sabertooth (2x12) to run some ruddy lights 
	if(topic.compare("auto/done")==0){
		int lightStatus = stoi(message); 
		if(lightStatus==1){
			// turn on light
			MotorSpeed[5] = 0;
		}else{
			// turn off the light 
			MotorSpeed[5] = 64;
		}
		doSerial(5);
	}

}

// MQTT magic, ask Valen 
// basically just listens for thos topics then directs to the message handler function which based on the topic will do things 
// this is called once from a thread and then runs until it is killed. 
void updateInputs(){
	

	broker = new MQTT("QSET-WHEELS", "localhost", 1883);

	broker->emit("system", "Controls Online");

	broker->on("wheels", &messageHandler);
	broker->on("wheelsStep",&messageHandler);
	broker->on("wheelModeChange",&messageHandler);
	broker->on("auxControl",&messageHandler); 
	broker->on("changeGain",&messageHandler); 
	broker->on("wheelEncFailure",&messageHandler); 
	broker->on("estop",&messageHandler); 
	broker->on("auto/done",&messageHandler);	

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// a bunch of avlues that are used in the wheel controller, these are reasigned each time, but declared otu here so they were not re floated each loop
float SP; // setpoint for specific motor 
float Error; // differnce between SP and MotorSpeeds
float EncSpeed; // motor speed from Encoder 
int MotorVal;
float ThrottleVal;
// tings to put the ID in PID, if you put a you in here it might even make a contraceptive? <--- I dont get this joke, I wrote it, but I was drunk 
// these are arrays so one value is stored for each wheel 
// there is no fancy control on the aux motors as in the config i worked with there were no encoders on those 
float CumError[4]; 
float LastError[4]; 
float DerError[4]; 
// this function tries its darndest to match the setpoint speed on each wheel, its basically a P controller that adjusts a throttle postion after each loop 
// basicalyl shitty cruise cotnrol 
// it kinda comes out to be a PI controller, we dont need no D, we got dirt for that 
void wheelControl(void){
	// the actual wheel control system that is called from the control loop 
	// loop over wheel motors to get speeds 
	// these max speeds are teh max that the controlelr can handel 
	int MaxSpeed = 127;
	int MinSpeed = 0;
	

    
	for(int n=0; n<4;n++){
		// get setpoint in terms of encoder speed

		SP = 0.0; // setpoint for specific motor 
		Error = 0; // differnce between SP and MotorSpeeds
		EncSpeed = 0; // motor speed from Encoder 
		MotorVal = 0;
		ThrottleVal = 0;
		
	//	cout<<"Working on Wheel: "<<n<<endl;
		// SP will then be a value from -MaxWheelSpeed to Plus Max Wheel Speed
		SP = SPSpeed[n]*(float)MaxWheelSpeed; 
		// get encoder speed 
		EncSpeed = EncoderValues[n]; // this value should have the same range OR MORE than the SP range 
		// if the range of EncSpeed is less than the range of SP, as when you have a large Max wheel Speed this means the system saturates quickly 
		//cout << "Encoder: "<<n<< " Value is: "<< EncSpeed<< " From Control Loop"<<endl; 
		//Find the difference between the setpoint and where the encoders are 
		Error = SP - EncSpeed; 
		CumError[n] += Error; // update the cum error 
		DerError[n] = (Error - LastError[n]); // update the der error 
		if(CumError[n]*Error <0){
			/// this means we had a zero crossing and we should zero the CumError and the Last Error 
			// a zero crossing is the sign of the error changed 
			CumError[n] = 0.0;
			DerError[n] = 0.0;
			// in proper this does not need to be done, but it was added to try to improve settling time, normally you would have to have a large 
			/// overshoot to kill the cum error 
		}
		// set a throttle value based on the gains and the errors 
		// everything should be kept as a flot going in, and then when we modify the motor vals it becomes and int 
		ThrottleVal = kp * Error + ki * CumError[n] + kd * DerError[n]; 
		//apply the throttle change to the Motor val 
		// the throttle value is used to move the motor speed (voltage applied) from the pervious loop into the present 
		// MOTOR VAL HAS TO BE AN INT. The motor controllers can only take in ints as a motor speed value 
		if(n<2){
			// this motor is the back right motor, it is backwards becasues reasons 
			MotorVal = MotorSpeed[n] - ThrottleVal; 
		}else{
			MotorVal = MotorSpeed[n] + ThrottleVal; 
		}
		// bound the motor speed 
		// the purpose of this was to not give the motor controller a value outside of the expected range. This acts as a forced saturation on the motor controller in code 
		// the motor controller should also do this itself, but this was done by the Departmant of Reduandacy Department to be redundant. 
		if(MotorVal>MaxSpeed){
			MotorVal = MaxSpeed; 
		}else if(MotorVal<MinSpeed){
			MotorVal = MinSpeed; 
		}
		// re assign the motor speed 
		MotorSpeed[n] = MotorVal; // after its been bounded we write this to the valeu taht we are going to write 
//		if(n==2)
//		cout<<"Writing Motor Send: "<<MotorVal<<" EncoderValue: "<<EncSpeed<<" Index: "<< n<<" Error: "<< Error << " ThrottleVal:"<<ThrottleVal << " SetPoint:"<< SP <<endl;

		LastError[n] = Error; 
	}
	// loop over wheels to write
	// these were done seperatly to redunce the delay in writing to all the motors incase of large change so we dont have weird jerks 

	//cout<<"Done Writing Serial For this Loop"<<endl; 
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void stepControl(void){
	// step control function 
	// NOTE: THIS will only run if the front two encoders are not in the broken encoder list 
	float SPHereL; // a setpoint value
	SPHereL = 0.0; // start by setting the point randomly 
	// set the wheel steps recoder to zero 
	float SPHereR;
	SPHereR = 0.0; 
	

	float StepGainP ; 
	float StepGainI ; 

	float StepError = 0; 

	// emit the number of wheel steps done 
	broker->emit("stepsDone","{\"left\" :"+to_string(WheelStepENCL)+",\"right\" :" + to_string(WheelStepENCR)+"}");	// set the wrist angle to where it currently is. 

	
	Step2GoL = WheelStepL-WheelStepENCL;
	Step2GoR = WheelStepR - WheelStepENCR;
	if(fabs(Step2GoL)<fabs(WheelStepL/2)){
		// we are well past half way there
		CumStep2GoL -=WheelStepENCL;

	}else{ 
		CumStep2GoL += Step2GoL; 
	}

	if(fabs(Step2GoR)<fabs(WheelStepR/2)){
		// past halfway point
		CumStep2GoR -= WheelStepENCR;
	}else{
		CumStep2GoR += Step2GoR;
	}
	 

	if(CumStep2GoL*Step2GoL<0){
		// sign change
		CumStep2GoL = 0.0; 
	}
	if(CumStep2GoR*Step2GoR<0){
		// sign change
		CumStep2GoR = 0.0; 
	}

//	cout<<"Wheel Rotations left: "<<Step2GoR<<"Wheel Step Rotations Done: "<<WheelStepENCR<<endl; 

	StepGainP = 8.0/1000.0; // gain proportional 
	StepGainI = 16.0/1000.0; // gain integral  
	SPHereL = StepGainP*Step2GoL/fabs(WheelStepL)+CumStep2GoL*StepGainI;// + StepGainI*StepError; // gives you a setpoint 
	SPHereR = StepGainP*Step2GoR/fabs(WheelStepR)+CumStep2GoR*StepGainI;
// 	change max speed based on how far we are going 
//	cout<<"Left Speed: "<<SPHereL<<" Right Speed: "<<SPHereR<<endl; 

	// the followign chain of ifsetatments sets the speed that the rover goes depending on how many total steps it had to go, it goes faster if you ask it to go further

	if(fabs(WheelStepL)>2){
			if(SPHereL>0.7){
			SPHereL=0.7;
		}else if(SPHereL<-0.7){
			SPHereL=-0.7; 
		}
	}else{
		if(SPHereL>0.2){
			SPHereL=0.2;
		}else if(SPHereL<-0.2){
			SPHereL=-0.2; 
		}
	}
	if(fabs(WheelStepR)>2){
		if(SPHereR>0.7){
			SPHereR=0.7;
		}else if(SPHereR<-0.7){
			SPHereR=-0.7;
		}	
	}else{
		if(SPHereR>0.2){
			SPHereR=0.2;
		}else if(SPHereR<-0.2){
			SPHereR=-0.2;
		}
	}
	// the following if statpent breaks the step controller once each side of the rover has gotten statisfactoryy close 
	// it also breaks if you asked it to do some stpupid small number of steps, 
	// that number should actually be larger so that it corrisponds and accounts for the back lash in the wheels 
	// also none of this accounts for the backlash in the wheels, so the first step is always the shortest, kind of like life 
	float StepDeadZone = 0.025; 
	if((fabs(Step2GoL)<StepDeadZone && fabs(Step2GoR)<StepDeadZone) || (fabs(SPHereR)<0.001 && fabs(SPHereL)<0.001)){
		
		SPHereL = 0.0;
		SPHereR = 0.0; 
		doStepControl = 0; // shoudl break the step contoller 
		WheelMode = 0; // set it to normal  
		// dump that done 
		broker->emit("drivingStatus", "doneSteps");

	}

	SPSpeed[0] = SPHereL; 
	SPSpeed[3] = SPHereL; 

	SPSpeed[1] = SPHereR; 
	SPSpeed[2] = SPHereR; 
	

}

//////////////////////////////////////////////////////////////////////////////////////////
// the following is a steamy pile of shit, you can read it if you wwant but it is shit and was never used 
// i tested it once, it went poorly so we said "Fuck it". Honeslty you can read to see if its useful, but after watching the rover get stuck im not sure this would help
int MotorVolts[4]; 
void newTracControl(){
	// check we are not turning, if we are turning dont do traction control, traction control and skid steering is dumb 
	if(fabs(SetPoints[0]-SetPoints[1])<0.1){
		// we are going straight ish so we can run the traction control 
		// the basic way this works is the slowest wheel is right, the slowest wheel is going ground speed
		// faster wheels are slipping.  
		// The only case this is not true is if we were slidding down a hill, but there is not much that can be done then 
		// so this assumes we are not slidding down a hill, if you do find your self sliding down a hill, grab your towel and "DONT PANIC!"
		// this new traction control looks at the voltages written to the motors and if one is less than the others it is probably slipping and not loaded
		// this works on the idea that unless the controller is saturated teh wheels will all be going the same speed 
		// so the one slippin is the one that is using the least voltage to acheive the same speed 
		// find max voltage 
		// cout<<"Running Traction Control"<<endl;
		int MaxVoltage = 0; 
		int MaxLoadedWheel=0; // which wheel is working the hardest
		int SlowestSpeed = MaxWheelSpeed; 
		int SlowWheel; 
		for(int n=0;n<4;n++){
			MotorVolts[n] = MotorSpeed[n] - StopCommand; 
			if(fabs(MotorVolts[n])>MaxVoltage){
				MaxVoltage = fabs(MotorVolts[n]); 
				MaxLoadedWheel = n;
			}
			if(fabs(EncoderValues[n])<SlowestSpeed){
				SlowestSpeed = fabs(EncoderValues[n]); 
				SlowWheel = n; 
			}
		}
//		cout<<"Max Wheel Voltage: "<<MaxVoltage<<endl; 
//		cout<<"Max Loaded Wheel is: "<<MaxLoadedWheel<<" Slowest Wheel is "<<SlowWheel<<endl; 
		// knowing which wheel is the the most loaded we can modify the other wheels to derate their voltages 

		// the following accounts for if we are going backwards or forwards
		int DirModify =0;
		if(SetPoints[0]>0){
			// we are going forward
			DirModify = 1;
		}else if(SetPoints[0]<0){
			// we are going backwards 
			DirModify = -1; 
		}
		float SPLocal; 
		if(MaxVoltage>62){
			// this means that the wheel is near stall 
			// we want to slow down all the other wheels 
			SPLocal = EncoderValues[SlowWheel]/MaxWheelSpeed; // should give a value between -1 to 1 
			cout<<"Wheel "<<MaxLoadedWheel<<" is near stall, the setpoit of all wheels is set to "<<SPLocal<<endl; 
			for(int n=0;n<4;n++){
				if(n!=MaxLoadedWheel && fabs(MotorVolts[n])<62){
					// only derate the non stalled motors 
					SPSpeed[n] = SPLocal; 
					cout<<"Derated Motor"<<n<<" to Local Setpoint"<<endl; 
				}
			}
			// we want to re run the speed controller to get the new voltages for the slower setpoints on 3 of 4 wheels 
			wheelControl(); //re calculate values 
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void EncFailureHandler(){
	// encoder failure handler 
	// this is tested and it works 
	// if an encoder is broken, the motor on the borken encoder is given the equivelent voltage of the its pair on the same side
	// sometimes this voltage is opposite in sign since reasonss 
	int BrokeEnc; 
	
	// mirror front and back encoders on a side, if more are broken just drive the old way 
	int LeftCount  = 0; 
	int RightCount = 0;
	for(int n=0; n<EncFailure;n++){
		BrokeEnc = EncFailureList[n]; 
	//	cout<<"There are "<<EncFailure<<" broken encoders, one is: "<<BrokeEnc<<endl;
		if(BrokeEnc == 0 || BrokeEnc ==3){
			LeftCount++;
			cout<<LeftCount<<" broken Left Encoders"<<endl; 
			if(BrokeEnc==0){
				MotorSpeed[0] = 127 -MotorSpeed[3]; 
			}else{
				MotorSpeed[3] = 127 -MotorSpeed[0]; 
			}
		}else if(BrokeEnc ==1 || BrokeEnc ==2){
			RightCount ++;
			cout<<RightCount<<" broken Right Encoders"<<endl; 
			if(BrokeEnc ==1){
				MotorSpeed[1] = 127 -MotorSpeed[2];
			}else{
				MotorSpeed[2] = 127 - MotorSpeed[1]; 
			}
		}
	}
	if(LeftCount >1 || RightCount >1){
		// more than one broken encoder per side means we should revert tot eh old method. This relies on both sides having atleast one good encoder
		cout<<"Auto Switching to Voltage Control"<<endl; 
		WheelMode =3; 
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void controlLoop(void){
	// the actual control loop that calls the updates and control system 

	int WaitBetweenLoop = 50*1000; // microseconds 
	while(GlobalRun==1){
		// call wheel control system 
		if(WheelMode==3){
			// fall back get called if more than 2 encoders break and that is manually entered or if you set the mode 
			for(int n=0;n<4;n++){
				if(n<2){
					// fix backwards wheels 
					MotorSpeed[n] = -SetPoints[n]*63.0 + StopCommand; 
				}else{
					MotorSpeed[n] = SetPoints[n]*63.0 + StopCommand; 	
				}
				 
			}
		}else if(WheelMode ==1){
			// step control this figures out a wheel speed based on how far we are in terms ot steps 
			// this should not run if encoders 1 or 0 are broken as these are used to get the steps 
			// we should check the broken encoder list 
			int FrontBrokeEncCount = 0;  
			for(int ENC=0;ENC<4;ENC++){
				if(EncFailureList[ENC]==0 ||EncFailureList[ENC]==1){
					// we have a broken front incoder 
					FrontBrokeEncCount++; 
				}
			}
			if(FrontBrokeEncCount==0){
				stepControl(); 
				// wheel control is called after the step control to actually do some honest work and move the wheels 
				wheelControl(); 
				// after the wheel controller ahs set speeds we need to check that all enocders are good 
				EncFailureHandler(); // this is only called in fuctions where the encoders are used and is done after to write over bad readings 
				// this enc failure call is only used if a back encoder is broken 
			}else{
				cout<<"A FRONT ENCODER IS BROKEN SO STEP CONTROL WILL NOT RUN"<<endl; 
			}
		}else if(WheelMode==0){
			// standard case
			
			for(int n=0;n<4;n++){
				SPSpeed[n] = SetPoints[n]; 
//				cout<<"SPSpd: "<<SPSpeed[n]<<" for "<<n<<endl;
//				cout<<"SP :" <<SetPoints[n]<<endl;
			}
			wheelControl(); // get throttle values to send based on setpoints of speed 
			// after the wheel controller ahs set speeds we need to check that all enocders are good 
			EncFailureHandler(); 
		}else{
			// donothing 
			for(int n = 0 ; n<4; n++){
				MotorSpeed[n] = StopCommand; 
			}
			// make the wait bigger here 
			usleep(WaitBetweenLoop);
		}
		// actually write what we decided to do 
		// the serial writes are done together 
		// this was done incase onf of the control loops hanged to try and write all new wheel speeds at the same time to not have weird wheel speed
		// the other way to do this would be to have the serial writter run in its own thread and have it send commands continously 
		// however this had the chance to overhwelm the sabertooths so it would need a delay 
		// also this would speed up the speed at which the loop would run, but since we had a delay anyways to increase teh stability of the control system 
		// it would just be using another thread with no real gain 

		for(int n=0;n<4;n++){
			doSerial(n); 
		}
		/// this delay was the built indealy in the loop to give the system stability 
		usleep(WaitBetweenLoop); 
//		cout<<"Control Loioping"<<endl; 
	}
	// if we get a clean exit zero everything 
	// this is called after the control loop breaks 
	for(int n=0;n<4;n++){
		MotorSpeed[n] =  64.0; 
		doSerial(n); 
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// this function closes everything nicely and stops the wheels in the event of a program exit 


void cleanKill(int signum){
	cout<<"Clean Kill Func Value: "<<signum<<endl; 
	if(signum ==SIGINT){
		delete broker; // this stops the MQTT broker from running 
		GlobalRun = 0; 
		// forces again all motors to stop command 
		for(int n = 0;n<6;n++){
			MotorSpeed[n] = 64; 
			doSerial(n);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void){
	// main function that does setup 
	signal(SIGINT,cleanKill);
	cout<<"Spawned C++ Wheels Control System"<<endl;
	//setup serial 
	setupSerial(); 

	
	// zero all motors 
	for(int n=0; n<6;n++){
		// call do serial function to write serial values 
		MotorSpeed[n] = StopCommand; 
		// zero Previous Speeds
		PreviousSpeeds[n] = 0;
		doSerial(n); 
		if(n<4){
			// update previous setpoints 
			PreviousSetPoints[n] = 0;  
		}
	}
	// call encoder thread
	// how threads work here 
	/*
	This uses C++1y's standard for doing threads, the basic thread call is: 

		thread tXX(&functionNAME,inputvar1,....,inputvarN); 

	The threads should call a function pointer, not like the function calls I did below, this was fixed for the arm but not here 

	bascially the thread is given a name like t1,t2 up to tN 
	thread is the var type, like an int or a float but also helps the complier to see that we are doing thread calls 
	this uses the C++1y thread library as imported at the head of this file 

	Threads when they are done can be jointed back to the main thread 
	this is what is seen below. 

	After the control loop dies, which happens on the call of clean kill, we joint threads, we can join the thread t2 since this is the MQTT thread 
	and when we delete the broker in the KleanKill function we stop that thread so it can be joined 

	Next the function call of cleanPhidget is used this stops the phidget loop from running and makes the first thread ready to be joined.  

	The control loop itself is broken when CleanKill is called and we set the global run var to 0 and the loop finishes when it recheacks the run condition 
	It is possible that the control loop will run for a cycle after the cleankill call, this will leave a possible 50ms delay

	After all thefunctions have been stoped and threads joined the system prints that it is exiting cleanly 

	*/


	thread t1(runEncoders); 
	cout<<"Launched Encoders"<<endl; 

	//drive?
	thread t2(updateInputs);

	
	// launch the control sytem 
	cout<<"Launcing Control system"<<endl; 
	// set global run var
	GlobalRun=1; 
	controlLoop(); 
	// join all threads

	t2.join();
	cout<<"Joined the MQTT Thread"<<endl;
	cleanPhidget(); // clean up the phidget 
	t1.join(); 
	cout<<"Joined the Phidget Thread"<<endl; 
	cout<<"Clean Exit"<<endl; 

	return 0;
}
