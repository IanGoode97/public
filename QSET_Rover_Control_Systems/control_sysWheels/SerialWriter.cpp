#include <stdio.h> // standard input / output functions
#include <string.h> // string function definitions
#include <unistd.h> // UNIX standard function definitions
#include <fcntl.h> // File control definitions
#include <errno.h> // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <iostream>
#include <time.h>   // time calls

#include "ControlSystemHeader.h"

using namespace  std;

int fdGlobal; // glaobal fd;  

int open_port(void)
{
	int fd; // file description for the serial port
	
	fd = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);
//	unsigned char sendBytes[] = {0x34};
//	write(fd,sendBytes,1); 
	if(fd == -1) // if open is unsucessful
	{
		//perror("open_port: Unable to open /dev/ttyS0 - ");
		printf("open_port: Unable to open /dev/ttyS0. \n");
	}
	else
	{
		fcntl(fd, F_SETFL, 0);
		printf("port is open.\n");
	}
	
	return(fd);
} //open_port

int configure_port(int fd)      // configure the port
{
	struct termios port_settings;      // structure to store the port settings in

	cfsetispeed(&port_settings, B9600);    // set baud rates
	cfsetospeed(&port_settings, B9600);

	port_settings.c_cflag &= ~PARENB;    // set no parity, stop bits, data bits
	port_settings.c_cflag &= ~CSTOPB;
	port_settings.c_cflag &= ~CSIZE;
	port_settings.c_cflag |= CS8;
	
	tcsetattr(fd, TCSANOW, &port_settings);    // apply the settings to the port
	return(fd);

} //configure_port

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setupSerial(void){
	int fd; 
	fd = open_port();
	configure_port(fd);

	unsigned char SetupByte = 170; // change this to corrispond to the right bit rate adn crap this is 9600 baud 
	unsigned char setupBytes[] = {SetupByte}; 
	write(fd,setupBytes,1); // write the one setup byte
	// set the global fd after setup 
	fdGlobal = fd; 
	cout<<"Setup Serial"<<endl; 
	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// this is the function that acutally writes each command to each motor 

// -- NEW JULY 2018 
// Declare teh vars up here to save creating new vars each time 

unsigned char sendBytes[4];

int doSerial(int Index){ 
		
	int fd = fdGlobal; 
	sendBytes[0] = MotorAddress[Index][0]; // this is the address that is set for each motor controller based on the dip switches
	sendBytes[1] = MotorAddress[Index][1]; // this is the motor A or B plus the mode see the data sheet 
	sendBytes[2] MoSp = (int)MotorSpeed[Index]; // this is the actual speed range is 0 to 127 result is based on the MoSel 
	sendBytes[3] check = (MoAd + MoSel + MoSp) & 127; // the checksum is a value that the sabertooth does the same math on
	// it will only preforme teh same action if it reads the same data.  
	//unsigned char sendBytes[]={MoAd, MoSel, MoSp, check}; // this puts all the data into an array 
	//cout<<"Sending: [" << (unsigned int)sendBytes[0] << ", " << (unsigned int)sendBytes[1] << ", "<< (unsigned int)sendBytes[2] << ", " << (unsigned int)sendBytes[3] << "]" <<endl;
	write(fd,sendBytes,4); // this writes the array of bytes to the device fd which is the serial device is this bust 

} 
