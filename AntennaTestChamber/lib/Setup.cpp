// this file handels the setup of the program and all the required user interfacing to setup the software. 

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <thread> 
#include <math.h>
#include <string.h>
#include <time.h>
using namespace std;
#include "Functions.h"
#include <signal.h>
#include "GPIBFunctions.h"
#include "gpib/ib.h"
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void findZero(){
	// finds the zeros for the encoders 
	cout<<"Set system homes"<<endl; 
	int MoNum; // motor number to zero 
	cout<<"Which joint do you want to zero?"<<endl;
	cout<<"(0) Tx Polarization"<<endl;
	cout<<"(1) Rx Pan "<<endl; 
	cout<<"(2) Rx Polarziation"<<endl; 
	
	while(!(cin >> MoNum)){
	    cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	int ZeroLockOut = 0;
	if(MoNum>-1 && MoNum<3){
		// good
		if(MoNum==0){
			// this is the Tx Pol 
			cout<<"Are you sure you want to modify the zero of the Tx Polarization Zero Point?"<<endl; 
			string inString3; 
			cout<<"y/n"<<endl;
			cin>>inString3;
			if(inString3=="y"){
				ZeroLockOut =1;
			}else{
				ZeroLockOut = 0; 
				cout<<"Okay will not run zeroing on Tx Polarization"<<endl; 
			}
		}else if(MoNum == 1){
			// this is the Rx Pan 
			cout<<"Are you sure you want to modify the zero of the Rx Pan Zero Point?"<<endl; 
			string inString3; 
			cout<<"y/n"<<endl;
			cin>>inString3;
			if(inString3=="y"){
				ZeroLockOut =1;
			}else{
				ZeroLockOut = 0; 
				cout<<"Okay will not run zeroing on Rx Pan"<<endl; 
			}
		}else{
			// the Rx Pol 
			cout<<"Starting zeroing proceedure for Rx Polarization joint"<<endl;
			ZeroLockOut = 1; 
		}
		while(true && ZeroLockOut){
			// loop through until we get to a good value 
			cout<<"Enter number of degrees to move stage"<<endl;
			cout<<"Enter 0 when you are satisfied"<<endl; 
			float MoveBy; // angle to move by
			
			while(!(cin >> MoveBy)){
			    cout << "Bad value!: Try again: \n";
			    cin.clear();
			    cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
			if(MoveBy==0){
				// we are at the zero point
				setEncZero(MoNum);
				tidyUpSPI(); 
				gpioCleanUp();// clean up gpio pins 
				break;  
			}else{
				// Set the SP for that motor to the move amount 
				MotorSP[MoNum]+= MoveBy; 
				// run the control system 
				ControlLoop();
			}
		}
	}
	cout<<"Done Zeroring Joint "<<MoNum<<endl; 
	// i cant get this to work so exit the program 
	cout<<"Program needs to exit to properly reset encoder"<<endl; 
	//cleanKill(SIGINT);
	if(ZeroLockOut){
		//only run the clean kill of we actualyl zeroed something 
		//cleanKill(SIGINT);	
		usleep(100*1000); // wait for a bit
		gpioStartup(); // inialize the gpio pins
		forceSPIStart(); // restart the SPI 
	}
	
}
//////////////////////////////////////////////////////////////////////////////////////////

void moreSetup(){
	// more settings
	string inString; // string used to process comands
	while(true){
		// VNA Setup Loop
		cout<<endl<<"--- More Settings Menu: ---"<<endl<<endl;
		cout<<"NOTE: These settings have been verified for this setup and should not need changed"<<endl<<endl;
		cout<<"Enter:"<<endl;
		cout<<"(a) to change the number of power meter averages per frequency point"<<endl; 
		cout<<"(d) to change the dwell time"<<endl; 
		cout<<"(p) to change signal generator power level"<<endl; 
		cout<<"(r) to turn on/off RF"<<endl;
		cout<<"(ps) run a power sweep measurment"<<endl;
		cout<<"(RxPol) to enable or disable the Rx Pol motor "<<endl;
		cout<<"(Door) to set the state of the door pause program"<<endl;
		cout<<"(w) to change the wait time afrer moving the joint"<<endl<<endl;; 
		cout<<"(q) to return to main setup menu"<<endl<<endl;
		cout<<"Choice: ";
		while(!(cin >> inString)){
		    cout << "Bad value!: Try again: \n";
		    cin.clear();
		    cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}

		if(inString =="q"){
				break;
		}else if(inString=="p"){
			// setup VNA
			cout<<"Changing the power level output from the signal generator"<<endl; 
			cout<<"Current value is "<<PWRLevel<<" dBm"<<endl<<endl;
			 

			cout<<"Enter the new signal generator power level [dBm]: "; 
			
			while(!(cin >> PWRLevel)){
			    cout << "Bad value!: Try again: \n";
			    cin.clear();
			    cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}

			// force power level to an int to make the sig gen hapyp 
			PWRLevel = (int)PWRLevel; 
			if(PWRLevel>4){
				cout<<"Entered Power level was too large"<<endl;
				cout<<"Limiting power to 4 dBm"<<endl; 
				PWRLevel = 4; //dBm
			}else if(PWRLevel<-20){
				// power level is too small 
				cout<<"Entered Power Level is too small"<<endl; 
				cout<<"Increasing output power to -20 dBm"<<endl;
				PWRLevel = -20; //dBm
			}
			if(PWRLevel>0){
				// note 
				cout<<endl<<"NOTE: See user documentation, there is not much point in increasing the signal generator power above 0dBm"<<endl<<endl;
			}
			setPWR(PWRLevel); 
			cout<<"Set new power level to "<<PWRLevel<<" dBm"<<endl; 
			
		}else if(inString=="a"){
			cout<<"Changing number of power meter measurment averages per frequency point."<<endl; 
			cout<<"Current value is "<<PWRAve<<" averages"<<endl<<endl;
			cout<<"Enter new value: "; 
			
			while(!(cin >> PWRAve)){
			    cout << "Bad value!: Try again: \n";
			    cin.clear();
			    cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
			cout<<"Updated power meter averages to "<<PWRAve<<" averages"<<endl; 
		}else if(inString=="ps"){
			// do power sweep 
			pwrSweep();
		}else if(inString=="d"){
			// change the dwell time 
			cout<<"Changing the dwell time between changing the frequency on the signal generator and reading the power meter"<<endl;
			cout<<"Current dwell time is "<<DwellTime<<" ms"<<endl<<endl;
			cout<<"Enter new value for dwell time [ms]: "; 
			while(!(cin >> DwellTime)){
			    cout << "Bad value!: Try again: \n";
			    cin.clear();
			    cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
			cout<<"Updated dwell time to "<<DwellTime<<" ms"<<endl; 
		}else if(inString=="r"){
			cout<<"(1) to turn on RF"<<endl; 
			cout<<"(0) to turn off RF"<<endl; 
			int RFState=0; 
			cout<<endl<<"Choice: ";
			while(!(cin >> RFState)){
			    cout << "Bad value!: Try again: \n";
			    cin.clear();
			    cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
			if(RFState==0){
				switchRF(0);
				cout<<"Turned off RF"<<endl; 
			}else if(RFState==1){
				switchRF(1); 
				cout<<"Turned on RF"<<endl; 
			}else{
				cout<<"Bad command"<<endl; 
			}
		}else if(inString=="w"){
			// change the wait time 
			
				cout<<"Change wait between the stage moving and measurment starting, default is "<<WaitB4Measure<<" ms"<<endl<<endl;
				cout<<"Enter new value [ms]: "; 
				while(!(cin >> WaitB4Measure)){
				    cout << "Bad value!: Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				cout<<"Updated the wait to "<<WaitB4Measure<<" ms"<<endl;
		}else if(inString=="RxPol"){
			cout<<"You can change the connection status of the Rx Pol Motor"<<endl; 
			cout<<"Currently the Rx Pol Motor is ";
			if(NumJoints ==3){
				cout<<"connected"<<endl;
			}else{
				cout<<"not connected"<<endl; 
			}

			cout<<"Would you like to enable/leave enabled the Rx Pol motor? [y/n]";
			string RxPolConnection; 
			while(!(cin >> RxPolConnection)){
			    cout << "Bad value!: Try again: \n";
			    cin.clear();
			    cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
			cout<<endl;
			if(RxPolConnection=="y"){
				// the rx pol motor is connected 
				cout<<"The Rx Pol Motor is connected"<<endl;
				cerr<<"The Rx Pol Motor is connected"<<endl;
				NumJoints = 3; 
			}else{
				cout<<"The Rx Pol Motor is NOT connected"<<endl;
				cerr<<"The Rx Pol Motor is NOT connected"<<endl;
				NumJoints = 2; 
			}
			cout<<endl; // add a space 

		}else if(inString=="Door"){
			// setup the door status in Door.cpp 
			setupDoorStatus(); 
		}else{
			//bad command
			cout<<"Bad Command"<<endl; 
		}
	}
}
//////////////////////////////////////////////////////////////////////////////////////////
// this function is used to make the array of values that the system will pan over 
void setupPan(){
	// after while loop set values 
	NumPoints = (PanStop-PanStart)/StepSize+1; 
	// make list of frequency points 
	PanAngles[0] = PanStart;
	for(int n=1;n<NumPoints;n++){
		PanAngles[n] = PanAngles[n-1] + StepSize; 
	}
	cout<<"Antenna Pan is setup for "<<NumPoints<<" angle steps"<<endl;

	// log setup 
	cerr<<endl<<"---ANGLE SWEEP SETUP---"<<endl<<endl;
	cerr<<"PanStart Angle: "<<PanStart<<endl;
	cerr<<"Pan Step Angle: "<<StepSize<<endl; 
	cerr<<"Pan Stop Angle: "<<PanStop<<endl<<endl; 
}

///////////////////////////////////////////////////////////////////////////////////////////
// Make new or check for output directory 

void newOutputDir(string NewDir){
	string NewDirFull = OutDir + NewDir+"/"; 
	cout<<"Changing ouput folder to: "<<NewDirFull<<endl; 
	OutDir = NewDirFull; 
	// check to see if this exists and make it if need be 
	// need to convert the new dir to a const char *
	const char * NewOutDir = NewDirFull.c_str(); 
	DIR* dir = opendir(NewOutDir);
	if(dir){
		cout<<"Switched to new directory"<<endl; 
		closedir(dir);
	}else if (ENOENT == errno){
		const int dir_err = mkdir(NewOutDir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		if (-1 == dir_err){
		    cerr<<endl<<"Error creating directory!n"<<endl<<endl;
		    exit(1);
		}	
	}else{
		cerr<<"Failed to make new dir: "<<NewDirFull<<endl; 
	}
}

///////////////////////////////////////////////////////////////////////////////////////////

void setup(){
	// setup function 
	string inString; // string used to process comands
	while(true){
		cout<<endl<<"--- SETUP MENU ---"<<endl<<endl; 
		cout<<"Enter:"<<endl;
		cout<<"(f) to change frequency sweep range"<<endl;
		cout<<"(m) to set panning characteristics"<<endl;
		cout<<"(p) to change axis polarization"<<endl;		
		cout<<"(d) to change directory for saving patterns"<<endl;
		cout<<"(z) for setting the zero point for each axis \n"<<endl;
		cout<<"(s) for more settings\n"<<endl; 
		cout<<"(q) TO EXIT to MAIN MENU"<<endl<<endl;
		cout<<"Choice: "; 
		while(!(cin >> inString)){
		    cout << "Bad value!: Try again: \n";
		    cin.clear();
		    cin.ignore(numeric_limits<streamsize>::max(), '\n');
		} 


		if(inString =="q"){
			break;
		}else if(inString=="z"){
			// set zeros 
			findZero(); 
		}else if(inString=="d"){
			// change working directory 
			cout<<"The current output driectory is: "<<OutDir<<endl; 
			cout<<"Changing the directory will save the rad patterns in a folder inside output files"<<endl; 
			cout<<"For example if you change the directory to TestAntenna, then the new output directory will be: "<<OutDir<<"TestAntenna/"<<endl; 
			cout<<"Do not have a / at the begining or end of the new directory"<<endl; 
			cout<<"If the directory does not exist the program will make the directory"<<endl<<endl; 
			string NewDir; 
			cout<<"Enter new output directory: "; 
			while(!(cin>>NewDir)){
				cout << "Bad value!: Try again: \n";
			    cin.clear();
			    cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
			newOutputDir(NewDir);  // setup the new directory 
						
		}else if(inString=="s"){
			// more settings 
			moreSetup();
		}else if(inString=="p"){
			// change polarization
			float newPol; 
			while(true){
				cout<<endl<<"Current Tx Polarization is: "<<MotorPos[0]<< "deg"<<endl<<endl; 
				cout<<"Enter New Tx Polarization Angle: ";
				
				while(!(cin >> newPol)){
				    cout << "Bad value!: Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				MotorSP[0] = newPol;
				cout<<endl<<"Current Rx Polarization is: "<<MotorPos[2]<< "deg"<<endl<<endl; 
				cout<<"Enter New Rx Polarization Angle: ";
				
				while(!(cin >> newPol)){
				    cout << "Bad value!: Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}

				MotorSP[2] = newPol;
				cout<<endl<<"Setting Rx Pol to: "<<MotorSP[2]<<" deg, Setting Tx Pol to: "<<MotorSP[0]<<" deg"<<endl<<endl; 

				cout<<endl<<"Is this correct? If yes the stages will move to the new angles y/n: "; 
				string inString1; 
				
				while(!(cin >> inString1)){
				    cout << "Bad value!: Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}

				if(inString1=="y"){
					ControlLoop();
					break; // exit the loop 
				}else{
					cout<<"You said no, try again"<<endl; 
				}			
			}
		}else if(inString=="m"){
			// change measurment setup
			while(true){
				cout<<endl<<"Enter Start Rx Pan Angle [deg]: ";
				
				while(!(cin >> PanStart)){
				    cout << "Bad value!: Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				cout<<"Pan Start set to "<<PanStart<<" deg"<<endl<<endl; 

				cout<<"Enter Stop Rx Pan Angle [deg]: "; 
				
				while(!(cin >> PanStop)){
				    cout << "Bad value!: Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				cout<<"Pan Stop Set to "<<PanStop<<" deg"<<endl<<endl; 

				cout<<"Enter Pan Step Size [deg]: "; 
				
				while(!(cin >> StepSize)){
				    cout << "Bad value!: Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				cout<<"Pan Step Size Set to "<<StepSize<<" deg"<<endl; 
			
				string inString2 = "y"; 
	/*			cout<<endl<<"Are these values correct y/n: ";
				
				while(!(cin >> inString2)){
				    cout << "Bad value!: Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
		*/
				if(inString2=="y"){
					// check to see if the values are good then break 
					if(PanStart<RxPanMin){
						cout<<"Start Pan angle is too small"<<endl;
						cout<<"Min start value is "<<RxPanMin<<endl;
					}else if(PanStop>RxPanMax){
						cout<<"Stop Pan angle is too large"<<endl; 
						cout<<"Max Stop Value is "<<RxPanMax<<endl; 
					}else if(StepSize<MinStepSize || StepSize>MaxStepSize){
						cout<<"Step size is out of range"<<endl; 
						cout<<"Range of good step values is "<<MinStepSize<<" to "<<MaxStepSize<<" deg and needs to be positive"<<endl;
					}else if(PanStop<PanStart){
						cout<<"Pan Stop angle has to be larger than Pan Star Value, try again"<<endl;
						cout<<"Inputed Values, Pan Start: "<<PanStart<<" Pan Stop: "<<PanStop<<endl; 
					}else{
						// good value
						// break the loop
						break;
					}
					cout<<endl<<"Please try again"<<endl<<endl; 
				}else{
					cout<<"Try again"<<endl; 
				}
			}
			setupPan(); 
		}else if(inString=="f"){
			//frequency sweep setup 
			while(true){
				cout<<"Frequency Sweep Setup"<<endl; 
				cout<<"You can setup a freqeuncy sweep here, max number of frequency points is 801"<<endl; 
				cout<<"If a single frequency point measurment is desired, enter the same value for the start and stop frequencies and the number of points as 1"<<endl<<endl; 
				cout<<"Enter your Start Frequency [GHz]: ";

				while(!(cin >> FreqStart)){
				    cout << "Bad value! should be a real number. Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				cout<<endl<<"Enter your Stop Frequency [GHz]: "; 
				
				while(!(cin >> FreqStop)){
				    cout << "Bad value! should be a real number. Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				cout<<endl<<"Enter Number of Desired Points, ie. 201,401,801: ";
				

				while(!(cin >> NumberofPointsVNA)){
				    cout << "Bad value! should be a real number. Try again: \n";
				    cin.clear();
				    cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
				// fix how we are getting an extra point 
				NumberofPointsVNA--; 
				if(NumberofPointsVNA>801){
					cout<<"Reducing number of freq points to 801"<<endl;
					NumberofPointsVNA = 801;
				}else if(NumberofPointsVNA<1){
					cout<<"Increasing number of freq points to 1"<<endl; 
					NumberofPointsVNA = 1; 
				}
				// check the data input 
				if(FreqStart>FreqStop){
					cout<<"The freqeuncy stop value needs to be larger than the frequency start value"<<endl; 
					cout<<"Inputed Values: Frequency Start: "<<FreqStart<<" Frequency Stop "<<FreqStop<<" GHz"<<endl;
				}else if(FreqStart > 40 || FreqStop>40){
					cout<<"Upper Frequecy is too large, keep frequency range to 1 to 40 GHz."<<endl;
				}else if(FreqStart<1 || FreqStop<1){
					cout<<"Lower Frequecy is too small, keep frequency range to 1 to 40 GHz."<<endl;
				}else{
					cout<<"Frequency sweep setup"<<endl; 
					break; // break the loop since we are done the frequency setup 
				}
			}

			cerr<<endl<<"---Frequency Sweep Setup ---"<<endl<<endl;
			cerr<<"Start Freq: "<<FreqStart<<endl;
			cerr<<"Number of Freq Points: "<<NumberofPointsVNA<<endl; 
			cerr<<"Stop Freq: "<<FreqStop<<endl<<endl; 

		}else{
			cout<<"BAD COMMAND TRY AGAIN"<<endl; 
		}

	}

}
