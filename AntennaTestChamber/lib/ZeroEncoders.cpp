
#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <wiringPi.h> 
#include <math.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
using namespace std;
#include "spiIAN.h" 
#include "Functions.h"

#define numEncoders 3 // number of encoders 






unsigned char transferHZ(unsigned char msg,int cs){
	// transfer function 
	unsigned char msgRx =0; 
	digitalWrite(cs,0); //select the device 
//	msg >>8;
	msgRx = transferSPI(msg); 
	digitalWrite(cs,1); 
	return msgRx; 
}
////////////////////////////////////////////////////////////////////////////////////////////////

float ReadEncoderZ(int cs){
	// this function reads encoder 

//	cout<<"ReadEncoder #: "<<endl; 

	
	// the code that goes in here will be SPI based to read the AMT CUI 203 encoders 

	digitalWrite(cs,0); // select 
//	usleep(1000); 
	transferHZ(0x70,cs); //  issue zero command 

	unsigned char recieved = 0xA5;    //just a temp vairable
//	usleep(500); 
	recieved = transferHZ(0x00,cs);    //issue NOP to check if encoder is ready to send
//	printf("Returned: %04x \n",recieved);
	while (recieved != 0x80){
	    //loop while encoder is not ready to send
		recieved = transferHZ(0x00,cs);    //check again if encoder is still working 
//		printf("Returned in loop: %04x \n",recieved);
//		usleep(500);    //wait a bit
   }


	//write chip select high 
	digitalWrite(cs,1); 

	//cout<<"Encoder Zeroed"<<endl; 
	//cout<<"Rememeber to Power Cylce the Encocder for the New Value to stick"<<endl;  



}

//////////////////////////////////////////////////////////////////////////////////////////////

void setEncZero(int EncNum){
	// main function 
	
	cout<<"Zeroing Encoder: "<<EncNum<<endl; 
	int PinHere; 
	if(EncNum == 0){
		PinHere = 0;
	}else if(EncNum ==1){
		PinHere = 2; 
	}else if(EncNum ==2){
		PinHere = 3;
	}	
	ReadEncoderZ(PinHere); 
	
	cleanUpSPI(); // cleanup the SPI bus 
	cout<<"DONE Zeroing Encoder: "<<EncNum<<endl;  

}
