#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <thread> 
#include <math.h>
#include <string.h>
#include <time.h>
using namespace std;
#include "Functions.h"
#include <wiringPiSPI.h> 
#include <wiringPi.h> 
#include <signal.h>
#include "GPIBFunctions.h"
#include "gpib/ib.h"
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>

// this file is used to see if the door is open or not
/*  DOOR STATUS PLAN for DoorStatusType
	0 -> Ignore door status 
	1 -> Pause when door opens, resume when the door closes 
	2 -> Pause when door opens, resume when door is closed and KEYPRESS 
*/


//////////////////////////////////////////////////////////////////////
int readDoorStatus(){
	// reads the door status 
	int DoorStatus = 0; 
	// 1 is open 
	// 0 is closed 
	int PinStatus = digitalRead(DoorPin); // read the pin status for the hall effect sensor 
	// based on the pin status, hi or lo decide if the door is open 
	
	// actual code to chekc the hall effect sensor
	// the pin for the GPIO for the controller is setup in the Controller.cpp file  

	return DoorStatus; 
}

////////////////////////////////////////////////////////////////////
void checkDoorStatus(){
	// checks the door status 
	if(DoorStatusType>0){
		// if the type is 0 we want to not run this and just skip
		// get the door status 
		int DoorStatus = readDoorStatus(); 

		// see what type of pause we are doing 
		if(DoorStatusType==1){
			if(DoorStatus){
				// door is open 
				cout<<"Door is open, waiting for it to be closed"<<endl; 
				while(DoorStatus){
					// while the door is open loop 
					// re check the door status 
					DoorStatus = readDoorStatus(); 
					usleep(10000); // add a sleep to not overwhelm 
				}
				cout<<"Door closed, resuming measurment"<<endl; 
				// send message to error log that the door was opened and closed 
				cerr<<"Door was open and closed"<<endl; 
			}
		}else{
			// door status type is 2
			if(DoorStatus){
				// this means the door is open, wait for key press 
				cout<<"DOOR IS OPEN"<<endl; 
				cout<<"Close door and press any key to continue"<<endl; 
				// wait for key press 
				system("read");
			}
		}
	}

}


///////////////////////////////////////////////////////////////////
void setupDoorStatus(){
	// setup the door status checker 
	cout<<endl;
	cout<<"Door Status Type is currently on: "<<DoorStatusType<<endl; 
	cout<<"\t (0) To ignore the door status"<<endl; 
	cout<<"\t (1) To pause when the door opens and resume when the door closes"<<endl;
	cout<<"\t (2) To pause when the door opens and resume when the user presses a key and the door is closed"<<endl;
	cout<<"Enter new door status cheker: ";
	while(!(cin >> DoorStatusType) && DoorStatusType<0 && DoorStatusType>2){
	    cout << "Bad value!: Try again: \n";
	    if(DoorStatusType>2 || DoorStatusType<0){
	    	cout<<"Door status type out of range"<<endl; 
	    }
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	cout<<"Door status type set to: "<<DoorStatusType<<endl; 
}