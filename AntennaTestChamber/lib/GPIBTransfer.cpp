#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <string.h>
using namespace std;
#include "GPIBFunctions.h"
#include "gpib/ib.h"
#include <math.h>
#include <sstream>
#include <stdlib.h>
//global variable for the device setup 
int dev; // The device name 

// This setup function was taken from the ibtest.c and modified

void setupDevice(void){
	// call the setup shell script to make sure things are connected 
//	system("Startup.sh"); 
	cout<<"GPIB Connected"<<endl; 
}

int BufferSetup = 0; 

///////////////////////////////////////////////////////////////////////////////////

int setupGPIB(int pad){
	int ud;
	int minor = 0; 
	const int sad = 0;
	const int send_eoi = 1;
	const int eos_mode = 0;
	const int timeout = T1s;
	

	
	//printf("trying to open pad = %i on /dev/gpib%i ...\n", pad, minor);
	ud = ibdev(minor, pad, sad, timeout, send_eoi, eos_mode);
	if(ud < 0)	{
		cerr<<"---GPIB ERROR---"<<endl;
		cerr<<"Error connecting the GPIB Interface: "<<stderr<<endl<<endl; 
		cout<<"Error connecting GPIB devices, check they are powered on and connected?"<<endl; 
		//cerr<<"There was an error ... "<< stderr<<endl; 
//		fprint_status( stderr, "ibdev error\n");
		abort();
	}

	dev = ud; 
	return dev; 

//	cout<<"GPIB Connected on Channel "<<pad<<endl;
}


////////////////////////////////////////////////
// Function to prompt for write from the ibtest.c file 

void writeGPIB(string inBound, int ud){
	int CountHere = 0; 
	char String2Send[1024];
	while(CountHere<5){
	    strcpy(String2Send,inBound.c_str());
	//	cout<<"Sending: "<<String2Send<<endl;
		if( ibwrt(ud, String2Send, strlen(String2Send)) & ERR ){
			cerr<<"---GPIB ERROR---"<<endl;
			cerr<<"Error Sending GPIB String will retry "<<5-CountHere<<" more times"<<endl;
			cerr<<"The problem GPIB string is: "<<inBound<<endl<<endl;
			CountHere++; 
		}else{
			// break the loop look like it used too 
			break; 
		}
		// add a small loop delay 
		usleep(5000); 
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

void readGPIB(int max_num_bytes, int ud){
	
	
	
	buffer_size = max_num_bytes + 1;
	int is_string=1;
	//cout<<"Starting Read Loop"<<endl;
//	while(is_string){
	/*
	if(BufferSetup==0){
		// first run 
		buffer = (char*)malloc(buffer_size);
		BufferSetup++;
	}
	
	*/
	//memset(buffer,0,buffer_size);

	ibrd(ud, buffer, buffer_size - 1);
	int read_count;

	if((ThreadIbsta() & ERR == 0) || ThreadIberr() != EDVR)	{
		read_count = ThreadIbcntl();
	}

	is_string = 1;
	int i; 

	for(i = 0; i < read_count; ++i){
		if(isascii(buffer[i]) == 0){
			is_string = 0;
			break;
		}
	}
	
	//cout<<"Done Read Loop"<<endl; 
}

////////////////////////////////////////////////////////////////////////////////////////////
// Reader function from ibtest.c 

// same as the orrginal but does nto do buffer parse loop, trying to fix seg faults when reading the pwr meter
float BufferReturn = 0.0; 
float readGPIBPower(int max_num_bytes, int ud){
	
	
	
	buffer_size = max_num_bytes + 1;
	int is_string=1;
	//cout<<"Starting Read Loop"<<endl;
//	while(is_string){
	/*
	if(BufferSetup==0){
		// first run 
		buffer = (char*)malloc(buffer_size);
		BufferSetup++;
	}
	
	*/
	//memset(buffer,0,buffer_size);

	ibrd(ud, buffer, buffer_size - 1);
	int read_count;

	if((ThreadIbsta() & ERR == 0) || ThreadIberr() != EDVR)	{
		read_count = ThreadIbcntl();
	}

	is_string = 1;

	BufferReturn = stof(buffer);
	return BufferReturn; 
	//cout<<"Done Read Loop"<<endl; 
}

/////////////////////////////////////////////////////////////////////////////////////////////
// END OF FILE 
