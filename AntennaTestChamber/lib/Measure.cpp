// This file contains all the functions involved with taking and recording data

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <thread> 
#include <math.h>
#include <string.h>
#include <time.h>
using namespace std;
#include "Functions.h"
#include <signal.h>


/////////////////////////////////////////////////////////////////////////////////////////////////////////
float ActualAngleList[3601]; // record of the actual angle
void measure(int RF){
	// the value of RF handles if we want the RF on or not, we would turn the rf off to take a noise floor measurment 
	// handles taking the measurment 
	cout<<"Starting to take Measurment"<<endl; 
	cerr<<endl<<"Starting Normal Measurment"<<endl<<endl;
	//cout<<"Saving data in a matrix that is "<<NumberofPointsVNA+1<<" rows by "<<NumPoints<<" cols"<<endl;

	// make a frequency list 
	float freqList[NumberofPointsVNA];
	freqList[0] = FreqStart; 
	float freqStep = (FreqStop-FreqStart)/(float)NumberofPointsVNA; 
	// only run this if the number of points is greater than 1 
	if(NumberofPointsVNA>1){
		for(int n=1;n<NumberofPointsVNA+1;n++){
			freqList[n] = freqList[n-1]+freqStep; 
		}
	}
	// actually take measruement 
	// turn on RF 
	if(RF==1){
		switchRF(1);
	}else{
		switchRF(0); //turn RF off 
	}
	
	for(int PanCount=0; PanCount<NumPoints;PanCount++){
		// loop in which the measurment is taken 
		// first set the pan motor to the desired angle 
		MotorSP[1] = PanAngles[PanCount]; 
		// call the control system to put this motor at that angle 
		ControlLoop();
		// sleep to make sure the VNA has settled \ 
		usleep(WaitB4Measure*1000); 
		cout<<"Starting Measurment for Pan Point "<<PanCount+1<<" of "<< NumPoints<< " for angle "<<MotorPos[1]<<" deg"<<endl; 
//		
		
		// save the output of S21dB to the data set 
		// run in a loop if the number of freq poitns is greaater than 1
		if(NumberofPointsVNA>1){
			for(int n=0;n<NumberofPointsVNA+1;n++){
				CurrentFreq = freqList[n]; 
				readVNA(); // read in the new data 
				dataset[n][PanCount]=S21dB;// this puts it in terms of gain 
				cout<<"Saving point "<<CurrentFreq<<" GHz with measured Rx power of "<<S21dB<<" dBm for Pan Angle "<<MotorSP[1]<<" deg"<<endl;
			}
		}else{
			// single poitn measure 
			CurrentFreq = freqList[0]; 
			readVNA(); // read in the new data 
			dataset[0][PanCount]=S21dB;// this puts it in terms of gain 
			cout<<"Saving point "<<CurrentFreq<<" GHz with measured Rx power of "<<S21dB<<" dBm for Pan Angle "<<MotorSP[1]<<" deg"<<endl;
			
		}
		cout<<"Done Measurment at point "<<PanCount+1<<" of "<< NumPoints<<endl; 
		ActualAngleList[PanCount] = MotorPos[1];  // save the actual motor pos 
	}
	// turn off RF 
	switchRF(0);
	// at end zero the center stage 
	//cout<<"Zering Pan Motor"<<endl; 
	//MotorSP[1] = Motor2Home; // point the dut antenna towards the door 
	//ControlLoop();
	// measurment finished 
	// write the data to file 
	
}
///////////////////////////////////////////////////////////////////////////////////////////////////////

// function used to sweep the Tx pol 

void measurePol(){
	// handles taking the measurment 
	cout<<"Starting to take Tx Pol Measurment"<<endl; 
	//cout<<"Saving data in a matrix that is "<<NumberofPointsVNA+1<<" rows by "<<NumPoints<<" cols"<<endl;
	cerr<<endl<<"Starting Tx Polarization Sweep Measrument"<<endl<<endl;
	// make a frequency list 
	float freqList[NumberofPointsVNA];
	freqList[0] = FreqStart; 
	float freqStep = (FreqStop-FreqStart)/(float)NumberofPointsVNA; 
	// only run this if the number of points is greater than 1 
	if(NumberofPointsVNA>1){
		for(int n=1;n<NumberofPointsVNA+1;n++){
			freqList[n] = freqList[n-1]+freqStep; 
		}
	}
	// actually take measruement 
	// turn on RF 
	switchRF(1);
	for(int PanCount=0; PanCount<NumPoints;PanCount++){
		// loop in which the measurment is taken 
		// first set the rx pol motor 
		MotorSP[0] = PanAngles[PanCount]; 
		// call the control system to put this motor at that angle 
		ControlLoop();
		// sleep to make sure the VNA has settled \ 
		usleep(WaitB4Measure*1000); 
		cout<<"Starting Measurment for Tx Pol  Point "<<PanCount+1<<" of "<< NumPoints<< " for angle "<<MotorPos[0]<<" deg"<<endl; 
//		
		
		// save the output of S21dB to the data set 
		// run in a loop if the number of freq poitns is greaater than 1
		if(NumberofPointsVNA>1){
			for(int n=0;n<NumberofPointsVNA+1;n++){
				CurrentFreq = freqList[n]; 
				readVNA(); // read in the new data 
				dataset[n][PanCount]=S21dB;// this puts it in terms of gain 
				cout<<"Saving point "<<CurrentFreq<<" GHz with measured Rx power of "<<S21dB<<" dBm for Tx Pol Angle "<<MotorSP[0]<<" deg"<<endl;
			}
		}else{
			// single poitn measure 
			CurrentFreq = freqList[0]; 
			readVNA(); // read in the new data 
			dataset[0][PanCount]=S21dB;// this puts it in terms of gain 
			cout<<"Saving point "<<CurrentFreq<<" GHz with measured Rx power of "<<S21dB<<" dBm for Tx Pol Angle "<<MotorSP[0]<<" deg"<<endl;
			
		}
		cout<<"Done Measurment at point "<<PanCount+1<<" of "<< NumPoints<<endl; 
		ActualAngleList[PanCount] = MotorPos[0];  // save the actual motor pos for the Tx angle 
	}
	// turn off RF 
	switchRF(0);
	// at end zero the center stage 
	//cout<<"Zering Pan Motor"<<endl; 
	//MotorSP[1] = Motor2Home; // point the dut antenna towards the door 
	//ControlLoop();
	// measurment finished 
	// write the data to file 
	
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
void writeData(int type){
	// writes the data to file
	string fileName; 
	cout<<endl<<"Enter File Name: ";
	while(!(cin>>fileName)) {
	 	cin.clear();
	 	cin.ignore(999,'\n');
	 	cout<<"Invalid data type! Please enter a new file name again"<<endl;
	 }
	fileName = OutDir+fileName; 
	cerr<<endl<<"---DATA SET---"<<endl;
	cerr<<"Data saved with file name: "<<fileName<<endl<<endl;
	if(type ==1){
		// this means we want to do a Tx pol sweep measurment 
		measurePol();
	}else if(type==2){
		// take a noise floor measurment 
		measure(0); // this means the RF power will nto be on 
	}else{
		// do normal measure function 
		measure(1);
	}
	
	cout<<"Writing file with "<<NumberofPointsVNA<<" rows and "<<NumPoints<<" columns"<<endl;

	// make a frequency list 
	float freqList[NumberofPointsVNA];
	freqList[0] = FreqStart; 
	float freqStep = (FreqStop-FreqStart)/(float)NumberofPointsVNA; 

	for(int n=1;n<NumberofPointsVNA+1;n++){
		freqList[n] = freqList[n-1]+freqStep; 
	}


	// open file 
	ofstream file; 
	file.open(fileName);

	// write the angle points to teh top of the file 
	file<<0<<",";
	for(int cols = 1;cols<NumPoints+1;cols++){
	//	file<<PanAngles[cols-1]<<",";
		file<<ActualAngleList[cols-1]<<","; 
	}
	file<<"\n"; 


	if(NumberofPointsVNA>1){
		for(int rows = 1; rows<NumberofPointsVNA+2;rows++){
			// write the frequency in the first column 
			file<<freqList[rows-1]<<","; 
			for(int cols = 1; cols<NumPoints+1;cols++){
				file<<dataset[rows-1][cols-1];
				file<<","; // comma for csv  
			}
			file<<"\n"; // new line at end 
		}
	}else{
		// single point measure 
		file<<freqList[0]<<",";
		for(int cols = 1; cols<NumPoints+1;cols++){
			file<<dataset[0][cols-1];
			file<<","; // comma for csv  
		}
		file<<"\n"; // new line at end 

	}

	file.close();
	cerr<<endl<<"---File Close---"<<endl;
	cerr<<"File was allowed to close"<<endl<<endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
void writeDataMulti(){
	// This function takes a measurmetn with multiple polarization angles
	string fileNameRoot; 
	string fileName; 
	cout<<"Multi-Polarization Measurment"<<endl; 
	cout<<"This program will ask for a root file name and then save the files for each polarization setup"<<endl;
	cout<<"as RootFileName__TxPolAngle__RxPolAngle.csv"<<endl;
	cout<<endl<<"Enter File Name Root: ";
	
	while(!(cin >> fileNameRoot)){
	    cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	// the below strings are used to hold the number of angles for each polariztion on each stage
	int NumRxPol;
	int NumTxPol;
	/*
	if(NumJoints==3){
		// onyl do this if the rx motor is conencted
		cout<<endl<<"Enter desired number of Rx Polarization setups: "; 
		while(!(cin >> NumRxPol)){
		    cout << "Bad value!: Try again: \n";
		    cin.clear();
		    cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}
	}
*/
	
	int RxPolStates = (NumRxPol);
	int TxPolStates = (NumTxPol);

	float RxPolAngles[RxPolStates];
	float TxPolAngles[TxPolStates];
	// user declare angles of each polariztion 
	float inAngles;
	float StartAng; 
	float StopAng; 
	float NumPointsPol; 
	float StepSize;
	if(NumJoints==3){
		// only do this if the Rx pol motor is conenctecd 
		
		cout<<"RxPol Sweep Setup"<<endl; 
		cout<<"Enter Desired Rx Pol Start Angle: "; 
		while(!(cin >> StartAng)){
		    cout << "Bad value!: Try again: \n";
		    cin.clear();
		    cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}
		cout<<endl<<"Enter Desired Rx Pol Stop Angle: "; 
		while(!(cin >> StopAng)){
		    cout << "Bad value!: Try again: \n";
		    cin.clear();
		    cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}
		cout<<endl<<"Enter Desired Number of Rx Pol Steps: ";
		while(!(cin >> NumPointsPol)){
		    cout << "Bad value!: Try again: \n";
		    cin.clear();
		    cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}
		StepSize = (StopAng-StartAng)/(NumPointsPol-1);
		RxPolStates = NumPointsPol; 
		RxPolAngles[0] = StartAng;
		for(int n=1;n<NumPointsPol;n++){
			
			RxPolAngles[n] = RxPolAngles[n-1]+StepSize; 

		}
	}

	// Tx angle sweep setup 
	cout<<"TxPol Sweep Setup"<<endl; 
	cout<<"Enter Desired Tx Pol Start Angle: "; 
	while(!(cin >> StartAng)){
	    cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	cout<<endl<<"Enter Desired Tx Pol Stop Angle: "; 
	while(!(cin >> StopAng)){
	    cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	cout<<endl<<"Enter Desired Number of Tx Pol Steps: ";
	while(!(cin >> NumPointsPol)){
	    cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	StepSize = (StopAng-StartAng)/(NumPointsPol-1);
	TxPolStates = NumPointsPol; 
	TxPolAngles[0] = StartAng;

	for(int n=1;n<TxPolStates;n++){
		// enter each angle 
		 
		TxPolAngles[n] = TxPolAngles[n-1] + StepSize;
	}
	int Counter = 1; 
	for(int Rx=0;Rx<RxPolStates;Rx++){
		for(int Tx =0;Tx<TxPolStates;Tx++){
			cout<<"Working on Polarization Setup "<<Counter<<" of "<<RxPolStates+TxPolStates<<endl;
			Counter++; 

			fileName = OutDir+fileNameRoot+"_"+to_string((int)TxPolAngles[Tx])+"_"+to_string((int)RxPolAngles[Rx])+".csv"; 
			// set the polarization 
			MotorSP[0] = TxPolAngles[Tx]; // set the Tx Pol Angle
			MotorSP[2] = RxPolAngles[Rx]; // set the rx pol angle 
			// apply this by calling the control system
			ControlLoop(); 
			// take the measurmetns 
			measure(1);
			cout<<"Writing file with "<<NumberofPointsVNA<<" rows and "<<NumPoints<<" columns"<<endl;

			// make a frequency list 
			float freqList[NumberofPointsVNA];
			freqList[0] = FreqStart; 
			float freqStep = (FreqStop-FreqStart)/(float)NumberofPointsVNA; 

			for(int n=1;n<NumberofPointsVNA+1;n++){
				freqList[n] = freqList[n-1]+freqStep; 
			}


			// open file 
			ofstream file; 
			file.open(fileName);

			// write the angle points to teh top of the file 
			file<<0<<",";
			for(int cols = 1;cols<NumPoints+1;cols++){
				//file<<PanAngles[cols-1]<<",";
				file<<ActualAngleList[cols-1]<<","; 
			}
			file<<"\n"; 



			for(int rows = 1; rows<NumberofPointsVNA+2;rows++){
				// write the frequency in the first column 
				file<<freqList[rows-1]<<","; 
				for(int cols = 1; cols<NumPoints+1;cols++){
					file<<dataset[rows-1][cols-1];
					file<<","; // comma for csv  
				}
				file<<"\n"; // new line at end 
			}

			file.close();
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////

void pwrSweep(){
	// this function sweeps power while leaving the horns lined up 
	cout<<endl<<"Enter Start Power in dBm: "; 
	float StartPWR=0.0; 
	float StopPWR = 0.0; 
	float PWRStep = 1.0; 
	// range of the power meter 
	int MaxPWR = 8; 
	int MinPWR = -20;
	// read start power 
	while(!(cin >> StartPWR)){
	    cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	cout<<endl<<"Enter Stop Power in dBm: "; 
	while(!(cin >> StopPWR)){
	    cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	// limit the stop power and start power 
	if(StopPWR>MaxPWR){
		cout<<"Stop power over max, setting to max safe value"<<endl;
		StopPWR = MaxPWR; 
	}
	if(StartPWR>MaxPWR){
		cout<<"Start power over max, setting to max safe value"<<endl;
		StartPWR = MaxPWR; 
	}
	cout<<"Enter Power Step Size in dBm"<<endl; 
	while(!(cin >> PWRStep)){
	    cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	// limit the PWRStep 
	if(PWRStep>15||PWRStep<0.1){
		// limit the power step 
		cout<<"Odd value for power step, forcing step size of 1 dBm"<<endl; 
		PWRStep = 1.0; 
	}
	cout<<endl<<"Enter File Name to Save Data As: "; 
	string fileNamePWR;
	while(!(cin >> fileNamePWR)){
	    cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}

	// make a frequency list 
	float freqList[NumberofPointsVNA];
	freqList[0] = FreqStart; 
	float freqStep = (FreqStop-FreqStart)/(float)NumberofPointsVNA; 

	for(int n=1;n<NumberofPointsVNA+1;n++){
		freqList[n] = freqList[n-1]+freqStep; 
	}

	// open the file 
	ofstream file; 
	fileNamePWR = OutDir+fileNamePWR; 
	file.open(fileNamePWR);
	// measrue example 
	// save the output of S21dB to the data set 
	int NumPWRPoints = (StopPWR-StartPWR)/PWRStep+1; 
	float LocPower = StartPWR; 
	// set the power 
	// turn off RF 
	switchRF(0);
	cout<<"Saving Freq sweep for RF turned off"<<endl; 
	for(int n=0;n<NumberofPointsVNA+1;n++){
		CurrentFreq = freqList[n]; 
		readVNA(); // read in the new data 
		dataset[n][0]=S21dB;
		cout<<"Saving point "<<CurrentFreq<<" GHz with measured gain of "<<S21dB<<" dB for local power "<<LocPower<<" dBm"<<endl;
	}
	// turn on RF 
	switchRF(1);
	for(int P = 1; P<NumPWRPoints+1;P++){
		// set the power 
		setPWR(LocPower); 
		cout<<"Saving Freq sweep for power level of: "<<LocPower<<" dBm"<<endl; 
		for(int n=0;n<NumberofPointsVNA+1;n++){
			CurrentFreq = freqList[n]; 
			readVNA(); // read in the new data 
			dataset[n][P]=S21dB;
			cout<<"Saving point "<<CurrentFreq<<" GHz with measured gain of "<<S21dB<<" dB for local power "<<LocPower<<" dBm"<<endl;
		}
		// step the local power 
		LocPower += PWRStep; 
	}
	// turn off RF 
	switchRF(0);
	// save the data 
	// the first line is a 0 followed by the power levels 
	file<<"0,-100,"; //the -100 dBm is used to give the RFoff measure 
	LocPower = StartPWR; 
	for(int P = 2; P<NumPWRPoints+1;P++){
		file<<LocPower<<","; 
		// step the Power 
		LocPower += PWRStep; 
	}
	file<<"\n"; 
	// now save the data 
	for(int rows = 0; rows<NumberofPointsVNA;rows++){
		// write the frequency in the first column 
		file<<freqList[rows]<<","; 
		for(int cols = 0; cols<NumPWRPoints;cols++){
			file<<dataset[rows][cols];
			file<<","; // comma for csv  
		}
		file<<"\n"; // new line at end 
	}

	file.close();

	
}
