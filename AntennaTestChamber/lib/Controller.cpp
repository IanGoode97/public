#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <thread> 
#include <wiringPiSPI.h> 
#include <wiringPi.h> 
#include <math.h>
#include <string.h>
#include <time.h>
using namespace std;
#include "Functions.h"

#include <python2.7/Python.h>


// This is the Controller File 

// storage space for MotorDriverFunction 
int MotorStates[3]; 

int StateList[4][8]; // there are 8 states per cycle.  

/*
A --> 0
B --> 1
A Bar --> 2 (C)
B Bar --> 3 (D)
*/
//Below List Pins that are used for Each Motor 
int MotorPins[3][4]; // rows are motors 1 through 3, the 4 cols follow the A through B bar rule
//GPIOClass* gpios[3][4]; // storage for GPIO stuff 
int FirstRun =1; // this is a value to decide if it is the first run or no 
//setup GPIOS for chip select on encoders
//POClass* gpioCS[3]; 
int CSpins[]={0,2,3}; // chip eect for encoders 
int DoorPin = 11;
string JointNames[] = {"TxPol","RxPan","RxPol"};
// with the L6208 Stpper driver 
// There are 3 pins needed per motor, a Clock called stepCLK
//  a reset called stepReset 
// a direction called stepDir 
//int stepCLKPins[3]; 
//int stepResetPins[3]; 
//int stepDirPins[3]; 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gpioStartup(void){
	// This is the Control Loop 

	// stepper motor pin assignmetns 
	MotorPins[0][0] = 21; MotorPins[0][1] = 25; MotorPins[0][2] = 23; MotorPins[0][3] = 24; 
	MotorPins[1][0] = 1; MotorPins[1][1] = 4; MotorPins[1][2] = 5; MotorPins[1][3] = 6; 
	MotorPins[2][0] = 26; MotorPins[2][1] = 27; MotorPins[2][2] = 28; MotorPins[2][3] = 29; 
	// list of possible states 
	StateList[0][0] = 1; StateList[0][1] = 1; StateList[0][2] = 0; StateList[0][3] = 0;StateList[0][4] = 0;StateList[0][5] =0;StateList[0][6] = 0; StateList[0][7] = 1;// A
	StateList[1][0] = 0; StateList[1][1] = 1; StateList[1][2] = 1; StateList[1][3] = 1;StateList[1][4] = 0;StateList[1][5] =0;StateList[1][6] = 0; StateList[1][7] = 0;// B
	StateList[2][0] = 0; StateList[2][1] = 0; StateList[2][2] = 0; StateList[2][3] = 1;StateList[2][4] = 1;StateList[2][5] =1;StateList[2][6] = 0; StateList[2][7] = 0;// A "B"ar 
	StateList[3][0] = 0; StateList[3][1] = 0; StateList[3][2] = 0; StateList[3][3] = 0;StateList[3][4] = 0;StateList[3][5] =1;StateList[3][6] = 1; StateList[3][7] = 1;// B "B"ar 
	// list of Chip speclect pins 
	
// this inilizes allh GPIo pins 
	cout<<"Running GPIO startup"<<endl; 
	wiringPiSetup (); // this uses the wiriingPi pin standad which is a little weird 
	for(int motor = 0; motor<3; motor++){
		for(int pins=0;pins<4;pins++){
			pinMode(MotorPins[motor][pins],OUTPUT); 
		}
		pinMode(CSpins[motor],OUTPUT); 
	}
	pinMode(7,OUTPUT); // declare enable pin 
	pinMode(DoorPin,INPUT);// setup the door pin for an input pin; 
	
	// set all GPIO pins to zero to start 
	for(int motor = 0; motor<3;motor++){
		for(int pins=0;pins<4;pins++){
			digitalWrite(MotorPins[motor][pins],0);	
		}
		digitalWrite(CSpins[motor],0); 		
	}
	digitalWrite(7,0); // write enable low 
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gpioCleanUp(void){
	cout<<"Cleaning up all Pins by forcing them to be inputs"<<endl; 
	for(int motor = 0; motor<3; motor++){
		
		for(int pins=0;pins<4;pins++){
			pinMode(MotorPins[motor][pins],INPUT); 
		}
		pinMode(CSpins[motor],INPUT); 
	}
	digitalWrite(7,0); // write enable low 
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void writePins(int MotorNumber){
	//  this function does the GPIO writing to the stepper controller 
	for(int Pin=0;Pin<4;Pin++){
		// sets the GPIo value; 
		digitalWrite(MotorPins[MotorNumber][Pin], StateList[Pin][MotorStates[MotorNumber]]);



		// Actuall Call the GPIO pin to do the things, for now it will just be a pritn statement 
	//	cout<<"Writing Pin: "<<Pin2Write<<" to "<<Val2Write<<endl; 

	}
	//cout<<"Wrote all gpio!"<<endl; 

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Old motor driver function when I wrote the step machine 
void MotorDriver(int MotorNumber, int Direction, int NumHalfSteps){
	// this is the motor driver function, it drives 3 two pole stepper motors using Darlington Transistor H bridges 
	// this code is half steppign, passing the half step values is easier than than deailing with floats 
	int MotorState = MotorStates[MotorNumber]; 
	//cout<<endl<<"DEBUG: Moving Motor "<<JointNames[MotorNumber]<<" "<<NumHalfSteps<<" steps"<<endl<<endl;
	// Direction: 0 is subtracting states 1 is increasign States 
	int HStepCounter = 0;  // number of half steps 
	int RunHere = 1; 
	int DelayBetweenSteps; 
	float Gain = 2.1*3000.0/(float)NumHalfSteps;  // Gain used for controlsystem 
	int DistanceFromEnd = 0; // this is the distance from the end point. We start at 0 so we will step up until Half 
	int Halfway = NumHalfSteps/2; 

	int MinDelay; // uSeconds 
	int MaxDelay; // uSeconds 

	if(NumHalfSteps>1000){
		MinDelay = 1700; // uSeconds 
		MaxDelay = 4000; // uSeconds 	
	}else{
		MinDelay = 3000; // uSeconds 
		MaxDelay = 3500; // uSeconds 
	}
	 

	while(RunHere ==1){
		// this is the loop that runs through the states 
		if(Direction==1){
			// add to the value of motorStates 
			//Also Step the Counter 
			HStepCounter++;
			MotorStates[MotorNumber]++; 
			if(MotorStates[MotorNumber]>7){
				// this means we need to loop 
				MotorStates[MotorNumber] = (MotorStates[MotorNumber]) - 8;   // Will take a value of 8 and return it to 0 
			}

		}else if(Direction == 0){
			// substract from the value of MotorStates 
			//Also Step the Counter 
			HStepCounter++; 
			MotorStates[MotorNumber]--; 
			if(MotorStates[MotorNumber]<0){
				// this means we need to loop 
				MotorStates[MotorNumber] = (MotorStates[MotorNumber]) + 8;  //  This will return a value of -1 to 7 
			}

		}else{
			cout<<"Bad Value for Direction, use only 1 or 0"<<endl; 
		}
		// actually step the motor 
		writePins(MotorNumber);
		//See if we need to break 
		if(HStepCounter>NumHalfSteps){
			// this means we are at the desired number of steps 
			RunHere = 0; 
		// 	cout<<"At Desired Number of Steps"<<endl;  
			break; 

		}else{
			// loop again, but add a delay before writing the next pin postion this should be done based on the difference between the number of steps done and the desired number of steps 

			// update Delay Between Steps 
			// we want to ramp at both ends, so that we are at a max when the Steps to go is half of the number of steps we have to go
			// Therefore we will accelerate for the first half ot the 
			if(HStepCounter<Halfway+1){
				// increase the size of Distance From End 
				DistanceFromEnd++; 
			}else{
				// we are past the halfway mark, start decreasing the distnace 
				DistanceFromEnd = NumHalfSteps-HStepCounter; 
			}

			// the larger the disance from end is the faster we want to go, thus we will be at a max speed when we are halfway

			float Speed = (float)DistanceFromEnd*Gain; 

			DelayBetweenSteps = MaxDelay - Speed; 

			if(DelayBetweenSteps>MaxDelay){
				// bound it 
				DelayBetweenSteps = MaxDelay; 
			}else if(DelayBetweenSteps<MinDelay){ 
				// bound it 
				DelayBetweenSteps = MinDelay; 
			}

			// call the Delay 
			usleep(DelayBetweenSteps); 			

		}

	}

}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float MotorLasPos[3]; 
float MotorPosChange[3]; // value to track if we have moved between loops or if we are stalled
int ControlLoopFirstRun =1; // quick counter to see if this is the first run of the cotnrol loop 
int BackLashCount[] = {0,0,0}; // a counter array for the coutnging the back lash tries 
float SmalAngleStalls[] = {0.0,0.0,0.0}; // count the small angle stalls, this is the kind of stall we get when around half a degree we count a bunch of thme then we break for that
void ControlLoop(void ){

	// 
	

	// Chip Select Pins for Encoders 
//	CSpins[0] = 0; CSpins[1] = 2; CSpins[2]=3; 
	// stepCLKPins
	//stepCLKPins[0] = 7; stepCLKPins[1] = 0; stepCLKPins[2] = 2; 
	// stepResetPins 
	//stepResetPins[0] = 3; stepResetPins[1] = 4;  stepResetPins[2] = 5; 
	// stepDirPins 
	//stepDirPins[0] = 22; stepDirPins[1] = 23; stepDirPins[2] = 24; 



//	cout<<"In Control Loop Function"<<endl; 
	cout<<endl<<"Moving Motors"<<endl<<endl; 
	/*// setup GPIO pins at first run 
	if(FirstRun==1){
		// this is the first run, we should inilize the GPIO 
		FirstRun++; // make it not equal to 1 
		// call the gpioStartup 
		gpioStartup(); 
		cout<<"************************************************************Setup GPIO Pins********************************************************************"<<endl; 


	}
*/


	int HalfSteps2Deg = 42; // This is the number of Half Steps to move 1 degree 

	int RunHere = 1; // this is a run value for the while loop that the controller is 
	float MotorDiff; // Difference between the setpoints and the Motor Postions 
	float MotorDiffMax; // Maximum motor Diff 
 
	int MoveMotor; // An array of 3 motors move the ones that are 1 dont move the oens that are 0  
	int MoveMotorSum = 0; // a value to show how many motors to mvoe 

	int NumSteps; // Number of steps we are going to ask teh system to move 
	int Direction; // direction we are going ask the system to mvoe 
	
	if(ControlLoopFirstRun==1){
		ControlLoopFirstRun++; // step this to make sure this does nto run every time
		// update the last motor postion 
		for(int n=0;n<NumJoints;n++){
			encoderReader(n); // update the motor pos 
			MotorLasPos[n] = MotorPos[n] + 10.0;// randomly add an offset so its not the same off the bat
		}
	}


	digitalWrite(7,1); // write enable high 
	//while(0){ //debug 
	int RunCounter = 0;// counts how many times the loop has run 
	while(RunHere ==1){
		// loop to loop in until we have good values 
		// first get Motor Postions 
		MoveMotorSum = 0; // leave at 0  until we check this 
		//updateMotorPos();  
		

		// update the Motor Differences 
		for(int MotorNumber=0;MotorNumber<NumJoints; MotorNumber++){
			// step through the three motors 
			// update Encoders 
//			cout<<"Working on Motor "<<MotorNumber<<endl;
			encoderReader(MotorNumber); 

			// deal with case where the setpoint is around 180 degrees 
			if((MotorSP[MotorNumber]>177 || MotorSP[MotorNumber]<-177)&&MotorPos[MotorNumber]<0){
				// in this case we should have the encoder values go from 0 to 360 
				cout<<"Modified the Motor Posiion to avoide +-180 degree break point"<<endl;
				MotorPos[MotorNumber] = MotorPos[MotorNumber] + 360.0; 
				if(MotorSP[MotorNumber]<0){
					MotorSP[MotorNumber] = MotorSP[MotorNumber] +360.0;
				}
			}
			MotorDiff = MotorPos[MotorNumber] - MotorSP[MotorNumber]; 
		//	if(fabs(MotorDiff)<2){
				// deal with back lash 
		//		MotorPosChange[MotorNumber]=1.0;
			//if(RunCounter==0){
				// New Loop 
			//	MotorPosChange[MotorNumber]=1.0; // give it a chance to move 
		//	}else{
				MotorPosChange[MotorNumber] = fabs(MotorPos[MotorNumber]-MotorLasPos[MotorNumber]); // how much the motor moved over the last loop we want to make sure this is larger than the angle dead zone 
		//	}
			// if this is smaller than the angle dead zone the motor is stalled or the encoder is broken 
			//cout<<"Current Pos is "<<MotorPos[MotorNumber]<<"Current Difference is: "<<MotorDiff<<endl; 
			usleep(1000); 
			// calcualte the number of steps 
			NumSteps = (int)(fabs(MotorDiff)* (float)HalfSteps2Deg); // gives the number of half steps we need to move the motor to the desired postion 
			// find the direction  we want to move the motor 
			if(MotorNumber==0 || MotorNumber==1){
				// this motor was wired backwards for the TxPol
				// the rx pan encoder is upside down 
				if(MotorDiff<0){
					// go forward 
					Direction = 0; 
				}else{ 
					Direction = 1; 
				}
			}else{
				if(MotorDiff<0){
					// go forward 
					Direction = 1; 
				}else{ 
					Direction = 0; 
				}
			}

			if(fabs(MotorDiff)>AngleDeadZone[MotorNumber] && MotorPosChange[MotorNumber]>AngleDeadZone[MotorNumber]){
				// we want to move this motor since the differnt is greater than the angle dead zone
				// the second part of the if statement is to ensure that the motor is not stalled 
				MoveMotorSum++;
				MoveMotor = 1; 

				// call the function that mvoes the motors 
				// motor 0 is moving randomly 
				if(MotorNumber==0 && fabs(MotorDiff)<80){
					// only log this if the change is small
					cerr<<endl<<"--- Tx Pol Debug ---"<<endl<<endl; 
					cerr<<"MotorSP "<<MotorSP[MotorNumber]<<" Motor Pos "<<MotorPos[MotorNumber]<<endl<<endl;
				}
		//		cout<<"Calling MotorDriver Function for motor:"<<MotorNumber<<" Moving "<<NumSteps<<" steps"<<endl; 
				MotorDriver(MotorNumber,Direction,NumSteps); 
		//		cout<<"Done Moving Motor"<<endl;
				// update the Motorlast postion so we know whats up 
				MotorLasPos[MotorNumber] = MotorPos[MotorNumber]; // update what this was at the start of the last loop, since we have moved the motor, the next time we run it should be differnt 
				// since we moved the motor lets zero the back lash counter 
				//BackLashCount[MotorNumber] = 0; 
			}else{
				// the else case is we dont want to move the motor 
				// amount to move by is either less than the dead zone or the motor ist stalled or the encoder is broken 
				int DoBackLash =0; // backlash flag 
				// let the backlash control be done by the small angle stall contion thing rather than the backlash congtroller it seems to work better
				if(BackLashCount[MotorNumber]<3 && fabs(MotorDiff)>AngleDeadZone[MotorNumber] && DoBackLash==1){
					// this means we want to move the motor, but a stall has been detected, it is possible it is just a backlash issue 
					// step backlash counter 
					cout<<endl<<"In Backlash System"<<endl;
					BackLashCount[MotorNumber] ++;
					//cout<<"Backlash value for Motor: "<< JointNames[MotorNumber]<<" is "<<BackLashCount[MotorNumber]<<endl<<endl;
					cerr<<"---BACKLASH ERROR---"<<endl; 
					cerr<<"Backlash was called for "<<JointNames[MotorNumber]<<" for an angle difference of "<<MotorDiff<<" this was attempt: "<<BackLashCount[MotorNumber]<<" of allowed 3"<<endl; 
					cerr<<"The angle differnce at this backlash call was: "<<MotorDiff<<endl;
					cerr<<"The angle of the motor was: "<<MotorPos[MotorNumber]<<endl;
					cerr<<"This backlash occured for angle setpoint of "<<MotorSP[MotorNumber]<<endl<<endl; 
					// the following makes sure the loop runs again
					MoveMotorSum++;
					MoveMotor = 1; 
					// we want to try to move the motor by a small amount in the same direction again 
					int BacklashSteps = 200; // number of steps to correct in backlash program
					
					// we want to limit how far it is moving incase something is actually wrong so we will limit it to 200 
					MotorDriver(MotorNumber,Direction,BacklashSteps); 
				

				}else if(MotorPosChange[MotorNumber]<=(fabs(MotorDiff)/3.0) && fabs(MotorDiff)>AngleDeadZone[MotorNumber] && fabs(MotorDiff)>5){
					// LARGE ANGLE STALL 
					// the motor is stalled or the encoder seems to be broken 
					// this is called if the joint does less than a third of the angle it should have based on the number of half steps called 
					// this is also onyl called if we have large angle call 
					cout<<"\n \n ------ WARNING!!! ------ \n"<<endl;
					cout<<JointNames[MotorNumber]<<" appears to be stalled or have mechanical encoder failure"<<endl<<endl; 
					
					cerr<<"---STALL ERROR--- \n  "<<endl;
					cerr<<JointNames[MotorNumber]<<" got into stall program for large angle stall"<<endl; 
					cerr<<" Current MotorPos: "<<MotorPos[MotorNumber]<<endl;
					cerr<<"last recorded pos: "<<MotorLasPos[MotorNumber]<<endl; 
					cerr<<"recorded value of MotorPos Change "<<MotorPosChange[MotorNumber]<<endl; 
					cerr<<"Motor Setpoint At Stall Call was: "<<MotorSP[MotorNumber]<<endl;
					cerr<<"Motor Diff Value: "<<MotorDiff<<endl<<endl;


					cout<<endl<<"WILL KILL SYSTEM NOW"<<endl<<endl;

					cerr<<endl<<"---EXITING BECAUSE OF STALL ---"<<endl<<endl;

					// try a break?
					raise(SIGINT);

				}else if(MotorPosChange[MotorNumber]<=(fabs(MotorDiff)/3.0) && fabs(MotorDiff)>AngleDeadZone[MotorNumber]){
					// the motor is stalled or the encoder seems to be broken 
					// this is called if the joint does less than a third of the angle it should have based on the number of half steps called 
					// this is the one for small angles
					SmalAngleStalls[MotorNumber] += (MotorDiff); // add the value we should have moved; 
					cerr<<endl<<"---IN SMALL ANGLE STALL ERROR CHECK ---"<<endl;
					cerr<<JointNames[MotorNumber]<<" is being check for small angle stall"<<endl; 
					cerr<<"cumulative value of the stall counter is "<<SmalAngleStalls[MotorNumber]<<endl;
					cerr<<"This time the motor difference angle was "<<MotorDiff<<endl;
					cerr<<"At angle: "<<MotorPos[MotorNumber]<<endl; 
					cerr<<"For Motor SP: "<<MotorSP[MotorNumber]<<endl; 
					cerr<<"Tried to move motor "<<NumSteps<<" steps"<<endl; 

					if(fabs(SmalAngleStalls[MotorNumber])>5){
						// this gets called if the cumulative small angle stall is greater than 5 degrees then call the stall 
						cout<<"\n \n ------ WARNING!!! ------ \n"<<endl;
						cout<<JointNames[MotorNumber]<<" appears to be stalled or have mechanical encoder failure"<<endl<<endl; 
						
						cerr<<"---SMALL ANGLE STALL ERROR--- \n  "<<endl;
						cerr<<JointNames[MotorNumber]<<" got into stall program for small angle stall"<<endl; 
						cerr<<" Current MotorPos: "<<MotorPos[MotorNumber]<<endl;
						cerr<<"last recorded pos: "<<MotorLasPos[MotorNumber]<<endl; 
						cerr<<"recorded value of MotorPos Change "<<MotorPosChange[MotorNumber]<<endl; 
						cerr<<"The cumulative small angle stall for this motor was: "<<SmalAngleStalls[MotorNumber]<<endl; 
						cerr<<"Motor Diff Value: "<<MotorDiff<<endl<<endl;


						cout<<endl<<"WILL KILL SYSTEM NOW"<<endl<<endl;

						cerr<<endl<<"---EXITING BECAUSE OF STALL ---"<<endl<<endl;

						// try a break?
						raise(SIGINT);
					}else{
						// since we havnt tripped the stall thing yet make srue the loop keeps running 
						// the following makes sure the loop runs again
						MoveMotorSum++;
						MoveMotor = 1; 
						MotorDriver(MotorNumber,Direction,NumSteps); 
						cerr<<"Motor Passed the check this time, will move motor"<<endl<<endl;
					}
				
				}else if(fabs(MotorDiff)<AngleDeadZone[MotorNumber]){
					// this means the motor got to where we want it to be 
					// lets zero the backlash counter
					// if this was not zero and we are zeroing it error log it 
					if(BackLashCount[MotorNumber]>0){
						cerr<<"--- BACKLASH ERROR RECOVERY ---"<<endl; 
						cerr<<"BackLashCount for "<<JointNames[MotorNumber]<<" was zeroed after "<<BackLashCount[MotorNumber]<<" attempts to get to set point angle "<<MotorSP[MotorNumber]<<endl;
						cerr<<"The diff was reduced to: "<<MotorDiff<<endl<<endl;; 
					}
					BackLashCount[MotorNumber] = 0; // zero the backlash counter 
					SmalAngleStalls[MotorNumber] = 0.0; // zero the small angle stalls coutner  
				}

			}
		//	MotorLasPos[MotorNumber] = MotorPos[MotorNumber];

		}

		if(MoveMotorSum==0){
			// if this is still equal to zero that means none of the motors need to mvoe, thus we can break 
			RunHere == 0; 
			break;  
		}
		// The else case is we want to run the loop again since we didnt get all the motors on the first try.  
		RunCounter++; // step the run counter
	}

	// if we broke the while loop this means all motors are at the postion 
//	cout<<"All joints should be at their desired postion"<<endl; 
	digitalWrite(7,0); // write enable low 


	
}

// END OF FILE 
