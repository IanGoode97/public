
#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <wiringPi.h> 
#include <math.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
using namespace std;
#include "spiIAN.h" 
#include "Functions.h"



int CS; // CS PIN make it global for easy 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int runSetup =1; 
void spiSetup(void){
	// spi setup function only run it once 
	cout<<"In chip select setup"<<endl; 
	// setup the chip select 
	wiringPiSetup(); 
	pinMode(0,OUTPUT);
	pinMode(2,OUTPUT);
	pinMode(3,OUTPUT); 
	digitalWrite(0,1);
	digitalWrite(2,1);
	digitalWrite(3,1); 


}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
int SPI_T(unsigned char msg){
	// call the transfer function 
	// this function is here since i was lazy 
	unsigned char msgRx;  //vairable to hold recieved data
	cout<<"Sending"<<hex<<msg<<endl; 
	msgRx = transferSPI(msg); 
	cout<<"Returned"<<hex<<msgRx<<endl;
	return msgRx;      //return recieved byte

}
*/
void tidyUpSPI(){
	// clean up the SPI 
	cleanUpSPI(); 
}

void forceSPIStart(){
	spiSetup(); // funciton above to redeclare CS stuff 
	SetupSPI(); // re run the SPI setup function for the device in the spiIAN file 
}

unsigned char transferH(unsigned char msg,int CS){
	// transfer function 
	unsigned char msgRx =0; 
	digitalWrite(CS,0); //select the device 
//	msg >>8;
	msgRx = transferSPI(msg); 
	digitalWrite(CS,1); 
	return msgRx; 
}
////////////////////////////////////////////////////////////////////////////////////////////////

unsigned char temp[2];

float ReadEncoder(int CS){
	// this function reads encoder 
	
	if(runSetup==1){
		// run the setup and set the runsetup command to not be 1 
		runSetup++; 
		spiSetup(); 
		cout<<"Setup the SPI Bus"<<endl; 
	}

	//cout<<"ReadEncoder #: "<<endl; 

	int EncoderPos; 

	// the code that goes in here will be SPI based to read the AMT CUI 203 encoders 

	digitalWrite(CS,0); // select 
//	usleep(1000); 
	transferH(0x10,CS); //  issue read command 

	unsigned char recieved = 0xA5;    //just a temp vairable
//	usleep(500); 
	recieved = transferH(0x00,CS);    //issue NOP to check if encoder is ready to send
//	printf("Returned: %04x \n",recieved);
	while (recieved != 0x10){
	    //loop while encoder is not ready to send
		recieved = transferH(0x00,CS);    //check again if encoder is still working 
	//	printf("Returned in loop: %04x \n",recieved);
//		usleep(500);    //wait a bit
   }

	temp[0] = transferH(0x00,CS);    //Recieve MSB
	temp[1] = transferH(0x00,CS);    // recieve LSB

	//write chip select high 
	digitalWrite(CS,1); 


	temp[0] &=~ 0xF0;    //mask out the first 4 bits

	EncoderPos = temp[0] << 8;    //shift MSB to correct ABSposition in ABSposition message
	EncoderPos += temp[1];    // add LSB to ABSposition message to complete message


	//EncoderPos= 8; 

	return EncoderPos; 
}

//////////////////////////////////////////////////////////////////////////////////////////////

void encoderReader(int Index){
	// main function 
	int CS;
	//cout<<"In Main Function"<<endl; 
	int pos; 
	float angle; 
	// loop and call encoder read
	int RunHere =0; 
//	while(RunHere<10000){
		// call the encoder reader 
	int n = Index;
	if(n == 0 ){
		CS = 0; 
	}else if(n==1){
		CS = 2;
	}else{
		CS = 3; 
	}
	//cout<<"Working on: "<<CS<<endl; 
	pos = ReadEncoder(CS); 
	angle = 0.087890625*(float)pos; 
	//cout<<"Reading Encoder: "<<n<<" Encoder Angle: "<<angle<<endl; 

	// make the angle go from -180 to 180 

	if(angle>180){
		angle = angle -360; 
	}
	MotorPos[Index] = angle;
// the following was used when the Rx Pol Encoder was broke
/*
	if(n==2){
		MotorPos[Index] = MotorSP[Index];
	}else{
		MotorPos[Index] = angle;
	}
*/	//	}
//
	//	usleep(200*1000); 
//
	//}

	
	 

}
