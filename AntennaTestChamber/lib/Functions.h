// This is the header file that connects the files 

// FUNCTIONS 

// MAIN FUNCTIONS 

//Setup Functions 
void findZero(void); // function that allows the user to zero a given axis. Including the menu 
void moreSetup(void); // additional setup menu 
void setup(void); // main setup menu
void setupPan(void); // sets up the array of pan angles 
void newOutputDir(std::string); // sets up a new output directory 

// Measurment Functions 
void measure(void); // function that handles setting angles and frequenceis for measurmetns and storing the data in the matrix 
void writeData(int); // the function that calls measure and save the data
void writeDataMulti(void); // allows user to take multiple polarizations in one go 
void pwrSweep(void); // a sweep function to sweep power levels

// Control System Functions 
void ControlLoop(void); // the main control loop, this gets all motors to their set points

// VNA Functions 
void readVNA(void); // read data from the power meter 
void connectVNA(void); // setup the GPIB to talk to the sig gen and power meter 
void switchRF(int); // turns on or off the RF output of the sig gen, give it a 1 or 0 for on or off 
void setPWR(float); // sets the signal gen power level give it a value in dBm 


// BACK END FUNCTIONS 

// encoder functions 
void encoderReader(int); // read encoders
void spiSetup(void); // initializes the spi at program startup 
void setEncZero(int ); // sets encoder number int to zero
void forceSPIStart(void); // forces the SPI to restart 
void tidyUpSPI(void); // deletes the variable used to store the SPI 

// GPIO Functions 
void gpioStartup(void); // inits all gpio pins
void gpioCleanUp(void); // sets all GPIO to inputs

// Clean Kill Functions 
void cleanKill(int ); // Handles a clean exit 

// Functions for the door both in Door.cpp
void checkDoorStatus(void);
void setupDoorStatus(void); 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// VARS 

extern float MotorSP[3]; // motor set poitns (angles)


// motor postions 
extern float MotorPos[3]; // current postion of each motor 
extern float AngleDeadZone[3]; 



// motor Max and Min Values 
// all units in degrees 
extern int TxRoationMin; 
extern int TxRotationMax; 
extern int RxRotationMin; 
extern int RxRotationMax; 
extern int RxPanMin; 
extern int RxPanMax; 
extern float MinStepSize; 
extern int MaxStepSize;  

//Largest number of poitns that can be taken 
extern int MaxNumPoints; 

// array that holds the chip select pints for the absolute encoders 
extern int CSpins[3];

// number of freuqecny points
extern int NumberofPointsVNA;
extern float S21dB; // max size is the max read of the VNA  
extern int DwellTime; // time between changing frequency and taking measurmetn 

extern float FreqStart; // Frequency sweep start frequency 
extern float FreqStop; // freqeuncy sweep stop frequency
extern float CurrentFreq; // current frequency 
extern float PWRLevel;// output power from the signal genorator 

extern int NumPoints; 
extern float PanAngles[3601];
extern float dataset[802][3602];
// home postions for each motor
extern int Motor1Home;
extern int Motor2Home;
extern int Motor3Home; 

// panning characterisitics 
extern float StepSize;
extern float PanStart;
extern float PanStop;

// dead time between moving the stage and starting to take a measurment 
extern float WaitB4Measure;
// number of times we average the powr min 
extern int PWRAve;
extern float MaxPower; // value of power that should not be exceeded on Rx 
extern int NumJoints; // number of active joints in the chamber as we can activate or deactivate the Rx Pol motor 
extern std::string OutDir; 
extern int DoorStatusType; // stores the type of door status chekc were are doing 0: ignore, 1: pause until door closed, 2: pause until keypress 
extern int DoorPin; // gpio pin for the door hall effect sensor 