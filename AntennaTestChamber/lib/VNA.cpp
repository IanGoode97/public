// this file holds all the functions to connect to the 

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <pthread.h>
#include <sstream>
#include <unistd.h>
#include <fstream>
#include <string.h>
using namespace std;
#include "GPIBFunctions.h"
#include "gpib/ib.h"
#include <math.h>
#include <sstream>
#include <string>
#include <cstring> // for splitting the buffer 
#include "Functions.h"
#include <limits>
using namespace std;

// transfer buffer 
char *buffer;
int buffer_size;


// define uds for devices 
int PWRud; // gpib setup for power meter 
int SIGud; // gpib setup for the signal genarator 

// a tx of 0 from the signal generor was chosen becasue the amp was seen to be saturating at around 0dBm input for the High freq horns and around -1dBm for the lower horns 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Functions
float PowerRead; 
void readVNA(){
	// read the VNA 
//	cout<<"Reading Current Data Set on Screen to a buffer"<<endl; 
	// here we need to do multiple reads and concatenate the strings 
//	cout<<"Enter a number of reads you want to do, 1 is enough for 201 data points"<<endl; 
	int numReads; 

	buffer = (char*)malloc(1024);
	memset(buffer,0,1024); 

	
	// set the current frequency 
	// send the frequency point 
	writeGPIB("F1 "+to_string(CurrentFreq)+" GH",SIGud); 
	usleep(DwellTime); // 1ms dwell time 

	// run until the buffer is empty 
	int bufferCount =0;
	int bufferlen = 10;

	float dataRead=0;
	
	for(int n = 0; n<PWRAve;n++){
		writeGPIB("O 1",PWRud); // send the power meter the I want to read data command 
		// this sleep helps the GPIB USB thing to not break
		usleep(10000);
		//readGPIB(1024,PWRud);
		//dataRead += stof(buffer);		
		// check the status of the door 
		checkDoorStatus(); // in door.cpp 
		PowerRead = readGPIBPower(1024,PWRud); // read the power meter 
		if(PowerRead>MaxPower){
			// Rx Power is getting close to hurting the power head 
			cout<<"WARNING HIGH RX POWER"<<endl;  
			// check to see if the power is above 18 dBm
			cerr<<"---HIGH RX POWER ---"<<endl;
			cerr<<"HIGH RX POWER: "<<PowerRead<<" was read by the power meter"<<endl<<endl; 
			if(PowerRead>18){
				// turn off Tx 
				cout<<"Rx POWER WAS "<<PowerRead<<" dBm. This was deamed unsafe and RF power was turned off and the program will exit"<<endl; 
		
				cerr<<"Program exited, becasue power was "<<PowerRead<<" and was deamed unsafe"<<endl<<endl; 
				switchRF(0); 
				// exit the program
				exit(0); 
			}
		}
		dataRead += PowerRead; // this gives a buffer
	}
	dataRead = dataRead/PWRAve; // do averageing 
	S21dB = dataRead; // set it to the global val
}

void connectVNA(){
	// connect the VNA 

	setupDevice(); // runs the shell script that starts the GPIB connection.
	PWRud = setupGPIB(13); // set channel to power meter  
	SIGud = setupGPIB(5); // set device to siggen 
}

// handle turning on and off the RF 
void switchRF(int state){
	if(state == 1){
		// turn on RF
		writeGPIB("RF1",SIGud);
		// also send power level command incase the user did not do this 
		setPWR(PWRLevel); 
	}else{
		// turn off RF 
		writeGPIB("RF0",SIGud);
	}
}
// set power level 
void setPWR(float PWRLevelLocal){
	// double check limit 
	if(PWRLevelLocal<-20 || PWRLevelLocal>8){
		cout<<"Tx power level was set outside of range, defaulting to Tx power of 0 dBm"<<endl; 
		cerr<<"---POWER SETTING ERROR---"<<endl;
		cerr<<"User tried to set illegal power level"<<endl<<endl;
		PWRLevelLocal = 0; 
	}
	writeGPIB("L1 "+to_string(PWRLevelLocal)+" DM",SIGud); 
	cout<<"Set SigGen Power to "<<PWRLevelLocal<<" dBm"<<endl;
}

