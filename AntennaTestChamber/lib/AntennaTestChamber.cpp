#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <thread> 
#include <math.h>
#include <string.h>
#include <time.h>

#include "Functions.h"
#include <signal.h>
#include <limits>

// these are used for error log savvings 
#include <ctime>
#include <iomanip>
#include <sstream>

using namespace std;
// this is the main program for the Spring 2018 Antenna Test Chamber test program 
// This program allows for zeroing the stages and taking a measurment and setting up the VNA 

int Motor1Home = 0; // Tx Pol, cable should be pointing up 
int Motor2Home = 90; // this is to get the pan to look at the door to easily work on the DUT 
int Motor3Home = 0; // Rx Pol, cable should be pointing up 

// THIS IS THE MAIN FILE

// GLOBAL VARIABLES START

/*
MOTOR DEFINTION 
	1--> Tx Polariztion Rotation 
	3--> Rx Polarization Rotation 
	2--> Rx Rad Cut Roation PAN
*/



// SETPOINTS FOR MOTORS, THESE Are the Desired motor angles 
float MotorSP[3];  


// motor postions 
float MotorPos[3]; 

// ANGLE DEAD ZONE 
float AngleDeadZone[] = {0.1,0.1,0.1};  // this is the maximum acceptable difference between the Setpoint and the Actual postions. 
// this was made a differnt dead zone for each joitn axis this is to help prevent Rx Pan issues


// motor Max and Min Values 
// all units in degrees 
int TxRoationMin = 0 ; 
int TxRotationMax = 100; 
int RxRotationMin = 0; 
int RxRotationMax = 100; 
int RxPanMin = -120; 
int RxPanMax = 120; 
float MinStepSize = 0.1; 
int MaxStepSize = 90;  

int NumJoints = 3; // number of joints 

//Sample Points Array 
int MaxNumPoints = 3601;//(RxPanMax-RxPanMin)/MinStepSize+1;  

// Motion lockout 
int MotorLockOut = 0; // 0 is locked out, 1 is run 

// Global Run, this is used to kill all functions
int GlobalRun = 1;  //1 is run, 0 is kill 

//file path and name 
//string SavingLocation; 

int NumberofPointsVNA=21;
float S21dB; // var to pass current readign data 


// setup dataset 
float dataset[802][3602]; // max size of the dta set based on the VNA being able to do 801 poitns and with a 0.1 deg resolution from -120 to 120 
int NumPoints=3601; // Actual number of read points

// defaults are below
// Rx Antenna Panning Characteristics 
float StepSize = 5.0; 
float PanStart = -90.0;
float PanStop = 90.0; 

// array of pan angles 
float PanAngles[3601]; // list of all points 

// dwell time between changing the frequency and reading the power meter : 
int DwellTime = 2; // 2 ms 

// VNA STUFF 
// defaults below 
float FreqStart = 4.0; 
float FreqStop = 12.0; 
float WaitB4Measure = 1500.0; // number of ms to wait between the stage moving and the measurment starting

float CurrentFreq; // value of the frequency 

int PWRAve = 8; // number of averages in the read the power meter 
float PWRLevel = 0.0; // sig gen power level  dBm 
float MaxPower = 12.0; // max Rx power, cut off iw we are not good // actualyl 20 dBm, was set lower to be safe 

// defualt output folder 
string OutDir = "/home/lab/OutputFiles/"; 

//Door status type 
int DoorStatusType = 0; 
/* 
	0 -> Ignore door status 
	1 -> Pause when door opens, resume when the door closes 
	2 -> Pause when door opens, resume when door is closed and KEYPRESS 
*/
/////////////////////////////////////////////////////////////////////////////////////////////////////

void exitFunction(){
	// things to do before we exit 
	switchRF(0);// turn off the RF power 
	tidyUpSPI(); // clean up the encoders 
	gpioCleanUp();// clean up gpio pins 
	// get time 
	auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    ostringstream dateStringRaw;
    // format the file name based on the date time 
    dateStringRaw << put_time(&tm, "%Y-%m-%d %H:%M:%S");
    auto exitTime = dateStringRaw.str();
    cerr<<endl<<"Exit Time: "<<exitTime<<endl;
}

// Clean Kill function, if the user kills the program this will run 
void cleanKill(int signum){
	cerr<<"Clean Kill Func Value: "<<signum<<endl; 
	if(signum ==SIGINT){
		cerr<<"User Killed the Program"<<endl; 
		// get time 
		auto t = std::time(nullptr);
	    auto tm = *std::localtime(&t);

	    ostringstream dateStringRaw;
	    // format the file name based on the date time 
	    dateStringRaw << put_time(&tm, "%Y-%m-%d %H:%M:%S");
	    auto exitTime = dateStringRaw.str();
	    cerr<<endl<<"Exit Time: "<<exitTime<<endl;
		// turn off RF 
		switchRF(0);// turn off the RF power 
		exit(0);
	}
}


//////////////////////////////////////////////////////////////

int main(){
	// main function 
	signal(SIGINT,cleanKill); // handle the pgroam being killed 

	// get time 
	auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    ostringstream dateStringRaw;
    ostringstream dateStringRaw2;
    // format the file name based on the date time 
    dateStringRaw <<"/home/lab/OutputFiles/ErrorLogs/"<< put_time(&tm, "%Y-%m-%d_%H-%M-%S")<<".txt";
    dateStringRaw2 <<put_time(&tm, "%Y-%m-%d %H:%M:%S"); 
    auto fileDate = dateStringRaw.str();
    auto StartTime = dateStringRaw2.str(); 

   // cout<<"Created teh file name: \n"<<fileDate<<endl;
    // convert the date file nmae to a const char pointer array that freopen can use to make the error log 
    const char* fileDateName = fileDate.c_str(); 
    // this will save anythign that is sent to cerr to the file name created above.  
	freopen(fileDateName,"w",stderr);
	cerr<<"Antenna Test Chamber Error Log"<<endl;
	cerr<<"Error Log for program start time: "<<StartTime<<endl; 
	cerr<<"Date Formate is: Year-Month-Day Hour:Minute:Second"<<endl<<endl;

	cout<<"Antenna Test Chamber Program"<<endl; 
	gpioStartup(); // inialize the gpio pins
	// first connect the VNA 
	connectVNA(); 

	// ask the user if we have the Rx Pol Motor on teh chamber 
	cout<<endl<<"Is the Rx Polarization Motor and Encoder Connected to the System? [y/n]: ";
	string RxPolConnection; 
	while(!(cin >> RxPolConnection)){
	    cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	cout<<endl;
	if(RxPolConnection=="y"){
		// the rx pol motor is connected 
		cout<<"The Rx Pol Motor is connected"<<endl;
		cerr<<"The Rx Pol Motor is connected"<<endl;
		NumJoints = 3; 
	}else{
		cout<<"The Rx Pol Motor is NOT connected"<<endl;
		cerr<<"The Rx Pol Motor is NOT connected"<<endl;
		NumJoints = 2; 
	}
	cout<<endl; // add a space 

	// ask for user name
	string UserName; 
	cout<<endl<<"Enter name of of User: "; 
	while(!(cin>>UserName)){
		cout << "Bad value!: Try again: \n";
	    cin.clear();
	    cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	newOutputDir(UserName);  // setup the new directory 
	cout<<endl<<endl<<"User Name Set to: "<<UserName<<endl; 
	cout<<"Files will be saved in: "<<OutDir<<endl; 
	cout<<"Files can be viewed by browsing to: http://130.15.16.142/"<<UserName<<"/"<<endl<<endl; 


	// connect the control system 
	// set the home postion for each motor
	// update the encoders 
	// first encoder value appears to be crap 
	// read it once 
	encoderReader(0); 
	for(int n=0;n<NumJoints;n++){
		encoderReader(n); // read encoders 
		// set motors to where they are right now 
		MotorSP[n] = MotorPos[n]; 
	}

	
	// turn off RF 
	switchRF(0);// turn off the RF power 
	// set the RF power to a reasonable value 
	setPWR(PWRLevel);
	ControlLoop(); 
	setupPan();// sets up the panning characteristics based on the defaults 
	cout<<"Done first control system run"<<endl<<endl<<endl; // add some line breaks 

	string inString; // string used to process comands
	string confirmString; 
	while(true){
		// long time loop to run this multiple times 
		cout<<endl<<"--- MAIN MENU ---"<<endl<<endl;
		cout<<"Enter:"<<endl; 
		cout<<"(s) to change the setup"<<endl; 
		cout<<"(m) for take measurment"<<endl; 
		cout<<"(n) for take noise floor measurment"<<endl; 
		cout<<"(p) for polarization measurment"<<endl;
		cout<<"(mm) for a measurment with multiple automatic polarization postions"<<endl;
		cout<<"(d) to manually drive an axis"<<endl; 
		cout<<"(h) to home all joints"<<endl; 
		cout<<"(q) for exit"<<endl<<endl; 
		cout<<"Choice: "; 
		while(!(cin >> inString)){
		    cout << "Bad value!: Try again: \n";
		    cin.clear();
		    cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}

		if(inString=="q"){
			cout<<endl<<"Are you sure you want to exit?? [y/n]: "; 
			while(!(cin >> confirmString)){
			    cout << "Bad value!: Try again: \n";
			    cin.clear();
			    cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
			if(confirmString =="y"){
				cout<<"Exiting"<<endl;
				break; 
			}
		}else if(inString=="s"){
			//run setup 
			setup();
		}else if(inString=="m"){
			//take measurment 
			writeData(0);
		}else if(inString=="n"){
			//take noise floor measurment 
			cout<<"Will preform normal measurment with the RF source off"<<endl; 
			writeData(2);
		}else if(inString=="p"){
			//take measurment for polarization 
			writeData(1);
		}else if(inString=="mm"){
			//take a measurement and automatically change the polarization angle 
			writeDataMulti(); 
		}else if(inString=="h"){
			// home motors 
			MotorSP[0] = Motor1Home; 
			MotorSP[1] = Motor2Home; 
			MotorSP[2] = Motor3Home; 
			ControlLoop(); // call this to home all joints 
		}else if(inString=="d"){
			// manual drive 
			cout<<"Manully drive a joint "<<endl;
			int LocalMoNum =1; // pan by defautl 
			float LocAng = 10; // defualt angle to move by 
			cout<<"Enter Motor Number to Move \n (0) Tx Pol \n (1) Rx Pan "<<endl; 
			if(NumJoints==3){
				// list the Rx Pol 
				cout<<"(2) Rx Pol"<<endl; 
			}

			cout<<endl<<"Choice: "; 
			while(!(cin >> LocalMoNum)){
			    cout << "Bad value!: Try again: \n";
			    cin.clear();
			    cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}

			if(LocalMoNum <0 || LocalMoNum>(NumJoints-1)){
				// reasign 
				cout<<"Entered bad motor num, defaulting to Rx Pan"<<endl;
				LocalMoNum = 1; 
			}
			cout<<"Current Joint angle is "<<MotorPos[LocalMoNum]<<endl;
			cout<<endl<<"Enter angle to move motor to: ";
	
			while(!(cin >> LocAng)){
			    cout << "Bad value!: Try again: \n";
			    cin.clear();
			    cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
			cout<<"Calling Cotnrol System"<<endl; 
			MotorSP[LocalMoNum] = LocAng;
			ControlLoop();
			// give the value of the motor 
			cout<<endl<<"Motor was moved to "<<MotorPos[LocalMoNum]<<" deg"<<endl<<endl;
		}else{
			cout<<"Bad command"<<endl; 
		}

	}
	cout<<"Data is saved in output files folder"<<endl; 
	exitFunction(); 
	cerr<<"Exited Cleanly"<<endl; 
	return 0; 
}
