int setupGPIB(int pad);
void setupDevice(void);
void writeGPIB(std::string, int ud ); 
void readGPIB(int max_num_bytes, int ud);
float readGPIBPower(int max_num_bytes, int ud); // same as the orrginal but does nto do buffer parse loop, trying to fix seg faults when reading the pwr meter
void gpioStartup(void);
// extern vars to transfer the buffer around 

// transfer buffer 
extern char *buffer;
extern int buffer_size;
//extern float BufferReturn; 