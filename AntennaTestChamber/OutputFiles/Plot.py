import numpy as np 
import matplotlib.pyplot as p 
import os

#read in the data 
file = open('t1.csv','rb')

Freq = []
Angles = []
Data = []
DataSub = []
Count = 0
Data.append([])
for line in file:
	cols = line.split(",")
	if(Count ==0):
		# this is the angles array 
		ElCount = 0
		for el in cols:
			if el != "\n" and ElCount>0:
				Angles.append(np.float(el))
			ElCount = ElCount +1
	else:
		# not the first array 	
		Freq.append(cols[0]) # the first element is the frequency 	
		Count1 = 0
		for item in cols:
			if Count1 >0:
				if item != "\n":
					DataSub.append(np.float(item))
			Count1 = Count1 +1 
		#print(DataSub)
		Data.append([])
		for thing in DataSub:
			Data[Count].append(np.float(thing))
		DataSub = []
	Count = Count + 1
file.close()
Data2 = filter(None,Data) # remove the empty row 
Data = Data2		
#print(Angles)
#print(Freq)
#print(Data[0][0])

# do the plotting

for PlotNum in range(0,len(Freq)):
	point = PlotNum
	FrequencyPlotted = Freq[point]
	#print FrequencyPlotted
	# put the angles in rads 
	AnglePlotted = []
	for ang in range(0,len(Angles)):
		AnglePlotted.append(Angles[ang]*3.14159265/180.0)
	ValPlotted = [] 
	minVal = 1000
	maxVal = -10000 
	for ang in range(0,len(Angles)):   
		ValPlotted.append(Data[point][ang])
		val = Data[point][ang]
		if(val>maxVal):
			maxVal = val

		if(val<minVal):
			minVal = val 

	# now we can actually plot the rad patterns 
	#ax = p.subplot(111, projection='polar')
	#ax.plot(AnglePlotted, ValPlotted,label = "gain [dB]")
	#p.polar(AnglePlotted,ValPlotted)
	#print("Angle Plotted: ", AnglePlotted," ValPlotted: ", ValPlotted)
	Spacing = 5
	UpperBound = np.int(maxVal/Spacing)*Spacing + Spacing
	LowerBound = np.int(minVal/Spacing)*Spacing - Spacing
	print("UpperBound: ",UpperBound," LowerBound: ",LowerBound)
	#ax.set_yticks(range(-40, 10, -10)) 
	p.grid(True)
	p.ylim(LowerBound,UpperBound)
	axisSizes = []
	here = LowerBound
	while True:
		if here<UpperBound:
			axisSizes.append(np.int(here))
		else:
			axisSizes.append(UpperBound)
			break
		here = here + Spacing
	p.yticks(axisSizes)

	fileLoco = 'Plots/'
	nameString =  str(FrequencyPlotted)+'GHz_Rad_Pattern'
	#print ValPlotted
	p.title(nameString)
	# before we save the figure lets make sure the ffolder exists 
	if not os.path.exists(fileLoco):
		os.makedirs(fileLoco)
	p.savefig(fileLoco + nameString +'.png')
	p.close()


