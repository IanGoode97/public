# shell script to run at start up to make everythign work, this needs to run as root 

# This makes the GPIB thing work 
echo Sucessfully Called Test Chamber Startup
modprobe ni_usb_gpib
lsmod |grep gpib 
gpib_config

# run the test chamber 
cd /home/pi/antennatestchamber/Spring2018Stable/
./testChamber
RESULT=$?


if [ $RESULT -ne 0 ]; then
        echo
        echo
        echo Program Start Failed 
        echo See on Screen Errors
fi

