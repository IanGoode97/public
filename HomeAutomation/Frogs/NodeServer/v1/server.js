var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var spawn = require('child_process').spawn;
var fs = require('fs'); 

var sleep = require('sleep');
var env = Object.create(process.env); 

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json()); 

app.use(express.static('static'));

// global vars 
var LivingRoomTemp;
var LivingRoomHumid; 

///////////////////////////////////////////////////////////////////////////////////////////
// ROUTERS

app.get('/', function(req, res){
    // read server status 
    var ServerStatus; 
   
    fs.readFile('/home/pi/files/ServerStatus.tmp','utf8',function(err,data){
    	if(err){
    		return console.log(err);
    	}
    	console.log(data); 
    	ServerStatus = data; 
    }); 

	console.log(ServerStatus);    

//       if(ServerStatus == 'OFF\n\n'){
    	res.sendFile(__dirname + '/index.html');
//    }
//    else{
//    	res.sendFile(__dirname+'/alreadyon.html');
//    }
});


app.get('/ServerTurnedOn', function(req, res){
    res.sendFile(__dirname + '/success.html');
});

app.get('/newIndex',function(req,res){
    res.sendFile(__dirname+'newIndex.html');
});

process.on('uncaughtException', function (err) {
    console.error(err.stack);
    console.log("Node NOT Exiting...");
});

// make server
app.listen(3000,function(){
   console.log("listening on port 3000");
}); 
// The next line allows for a folder that can be viewable from the web 
//app.use(express.static(__dirname+'/tempFiles')); 

//////////////////////////////////////////////////////////////////////////////////////////

// Deal with shit 

app.post('/turnOn',function(req,res){

   
 console.log("Turning on Server");
  var Measurment = spawn('/home/pi/ON.py',[],{env: env}); 

  // send back to home 
  res.redirect('/ServerTurnedOn'); 
  console.log("redirected");
 


});

app.get('/turnOff',function(req,res){

   
 console.log("Turning off Server");
  var Measurment = spawn('/home/pi/OFF.py',[],{env: env}); 

  // send back to home 
  res.redirect('/'); 
  console.log("redirected");
 


});

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Update temp info
function updateTemp(){
  //timeout 
  var tempTimeout = setTimeout(updateTemp,60*1000); // run this every minute 
  var month;
  var dateData = new Date();
  month =  dateData.getMonth(); 
  var year = dateData.getYear(); 
  month = month+1; // since the getMonth goes {0,11}

}

