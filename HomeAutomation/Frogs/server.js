var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var spawn = require('child_process').spawn;
var fs = require('fs'); 
var http = require('http');
var querystring = require('querystring');
var env = Object.create(process.env); 
var request = require('request');

//hi
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json()); 

app.use(express.static('static'));

app.engine('html',require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', __dirname)
// import a schedule handler 
var LIGHTS = require('./lights.js');
var SCHEDULE = require('./schedule.js');

// global vars 
var ServerPort = 80; 
var FishLightStatus = "OFF"; 
var FishFeederPos=1; // what postion the fish feeder is in 
var MaxNumFishPos = 8; // maximum number of postions for the fish feeder 
var FrogFeederPos=1; // what pos the feeder is in 
var MaxNumFrogsPos = 8; // maximum numberof pos for the frog feeder 
var FrogStep = 1; // step size in the frog feeder
var FishStep =1; // step size in the fish feeder
var FishFeederChangeTime = 'Not Fed Yet' // time the fish feeder last moved 
var FishFeederChangeEvent ='Server At Boot'; //what made the fish feeter move 
var FishLightChangeTime = Date(); 
var FishLightChangeEvent = 'Server At Boot';
var FrogFeederChangeTime = 'Not Yet Fed'
var FrogFeederChangeEvent ='Server At Boot';
var fishTemp = 20; 
var frogTemp = 20; 
var NotLightStatus = "OFF"; // statment that is the opposite of teh light status 
var SchedulerStatus = 1; // 1 is runnign 
var CamPassword = "CuddleNest";

var FrogLightStatus = "OFF"; 
var NotFrogLightStatus  = "OFF"; 
var FrogLightChangeEvent = 'Server At Boot';
var FrogLightChangeTime = Date(); 
var SchedulerStatusFrog = 1; 


// arrays to hold on and off times 
var OnTimes=[]; 
var OffTimes=[]; 
var OnTimesFrog=[]; 
var OffTimesFrog=[]; 
// call the function to populate the arrays 
ReadSchedule(0); 
// call the fish and frogs temp updateer 
updateFishTemps();
updateFrogTemps();

// call the functions to check the fish and frog light status 
checkFrogStatus();
checkFishStatus(); 
///////////////////////////////////////////////////////////////////////////////////////////
// ROUTERS

app.get('/', function(req, res){
  // set the not light status 
  if(FishLightStatus=="ON"){
    NotLightStatus="OFF";
  }else{
    NotLightStatus="ON"
  }
  if(FrogLightStatus=="ON"){
    NotFrogLightStatus="OFF";
  }else{
    NotFrogLightStatus="ON"; 
  }
  sendIndex(res); 
//	console.log("Fish Light Change Event " , FishLightChangeEvent);
}); 


function sendIndex(res){
    // vals to send to index 
    var indexPasVals ={
      FishLightStatus:FishLightStatus,
      fishTemp:fishTemp,
      frogTemp:frogTemp,
      FishFeederPos:FishFeederPos,
      FrogFeederPos:FrogFeederPos,
      FishFeederChangeTime:FishFeederChangeTime,
      FrogFeederChangeTime:FrogFeederChangeTime,
      FishFeederChangeEvent:FishFeederChangeEvent,
      FrogFeederChangeEvent:FrogFeederChangeEvent,
      FishLightChangeTime:FishLightChangeTime,
      FishFeederChangeEvent:FishFeederChangeEvent,
      NotLightStatus:NotLightStatus,
      FishLightChangeEvent:FishLightChangeEvent,
      SchedulerStatus:SchedulerStatus,
      FrogLightChangeTime:FrogFeederChangeTime,
      FrogLightChangeEvent:FrogLightChangeEvent,
      NotFrogLightStatus:NotFrogLightStatus,
      FrogLightStatus:FrogLightStatus,
      SchedulerStatusFrog:SchedulerStatusFrog
    };
    
    res.render('index.html',indexPasVals);
    
}

// router for the cams 
app.get('/camsPass', function(req, res){
  console.log("Sending cam password page")
  var PasswordWRONG = "Enter Password to See Fish ";
  res.render('FISHCamPass.html',{PasswordWRONG:PasswordWRONG});
});

app.post('/cams',function(req,res){
  var ReadPassword = req.body.psk; 
  console.log("Read in password "+ ReadPassword);
  if(ReadPassword==CamPassword){
    console.log("Sending Fish Cams");
    res.render('FISHCam.html');
    // set the res password back to nothing 
    ReadPassword = '';
  }else{
    console.log("Resending Cam pass, wrong password");
    var PasswordWRONG = "Wrong Password Try Again";
    res.render('FISHCamPass.html',{PasswordWRONG:PasswordWRONG}) // send the index 
  }
});



// this makes us not crash 

process.on('uncaughtException', function (err) {
    console.error(err.stack);
    console.log("Node NOT Exiting...");
});

// make server
app.use(express.static('html')); // serve data 
app.listen(ServerPort,function(){
   console.log("listening on port "+ServerPort);
}); 
// The next line allows for a folder that can be viewable from the web 
//app.use(express.static(__dirname+'/tempFiles')); 

//////////////////////////////////////////////////////////////////////////////////////////

// TURNING LIGHTS ON and OFF 

//////////////////////////////////////////////////////////////////////////////////////////


////////////
// FISH 
////////////

app.post('/turnOnLight',function(req,res){
    console.log("Test Turn on Light Function");
    FishLightStatus = "ON";
    FishLightChangeEvent = "USER";
    SchedulerStatus = 0; 
    // get time and date 
    FishLightChangeTime = Date();
    // send back to home 
    res.redirect('/');
    console.log("redirected to home");
    // call function to turn on 
    LIGHTS.turnON();
});



app.post('/turnOffLight',function(req,res){
   console.log("test turnOffLight function");
   FishLightStatus = "OFF";
   FishLightChangeEvent = "USER";
   SchedulerStatus = 0; 
    // get time and date 
    FishLightChangeTime = Date();
   // send back to home 
  res.redirect('/');
  console.log("redirected to home");
  LIGHTS.turnOFF();
});

///////////
// FROGS 
///////////

app.post('/turnOnLightFrog',function(req,res){
    console.log("Test Turn on Light Function");
    FrogLightStatus = "ON";
    FrogLightChangeEvent = "USER";
    SchedulerStatusFrog = 0; 
    // get time and date 
    FrogLightChangeTime = Date();
    // send back to home 
    res.redirect('/');
    console.log("redirected to home");
    // call function to turn on 
    LIGHTS.turnONFrog();
});



app.post('/turnOffLightFrog',function(req,res){
   console.log("test turnOffLight function");
   FrogLightStatus = "OFF";
   FrogLightChangeEvent = "USER";
   SchedulerStatusFrog = 0; 
    // get time and date 
    FrogLightChangeTime = Date();
   // send back to home 
  res.redirect('/');
  console.log("redirected to home");
  LIGHTS.turnOFFFrog();
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SCHEDULER SHIT 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////
// Resuming Scheudlers 
////////////

// FISH 

app.post('/ResumeScheduler',function(req,res){
   SchedulerStatus = 1; 
  res.redirect('/');
});

// FROGS 
app.post('/ResumeSchedulerFrog',function(req,res){
   SchedulerStatusFrog = 1; 
  res.redirect('/');
});

////////////////////////////////////////////////////////
// SETTING SCHEDULES
////////////////////////////////////////////////////////

// General 

// get day name from day num 
var DayName= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
// turning on and off times 
var TurnOnTime;
var TurnOffTime; 
var DayNum; 
var ScheduleType = "FISH"; 

// FISH 


var ShowSchedule = 0; // var to show sceduel or not 
app.post('/setSchedule',function(req,res){
  console.log("Setting schedule"); 
  // parse form data 
  DayNum = req.body.weekDay; 
  ScheduleType = req.body.ScheduleType; 
  TurnOnTime =  req.body.TurnOnTime;
  TurnOffTime =  req.body.TurnOffTime;
  console.log("Day: "+DayNum+" On Time: "+TurnOnTime+" Off Time: "+TurnOffTime); 
  ShowSchedule = 1;
  sendSchedulePage(res);
  if(ScheduleType=="FISH"){
    // saave this data 
    fs.writeFile("ScheduleFiles/"+DayNum.toString(), TurnOnTime.toString()+","+TurnOffTime.toString(), function(err) {
      if(err) {
          return console.log(err);
      }

        console.log("The file was saved!");
    }); 
    // update the arrayys
    OnTimes[DayNum] = TurnOnTime;
    OffTimes[DayNum] = TurnOffTime;
  }else{
    // saave this data 
    fs.writeFile("FrogScheduleFiles/"+DayNum.toString(), TurnOnTime.toString()+","+TurnOffTime.toString(), function(err) {
      if(err) {
          return console.log(err);
      }

        console.log("The file was saved!");
    }); 
    // update the arrayys
    OnTimesFrog[DayNum] = TurnOnTime;
    OffTimesFrog[DayNum] = TurnOffTime;
  }
  ReadSchedule(DayNum);
});



////////////////////////////////////////////////////////////////////////////////////
// VIEW Schedules 
////////////////////////////////////////////////////////////////////////////////////

app.post('/viewSchedule',function(req,res){
  console.log("viewing schedule"); 
  DayNum = req.body.weekDay; 
  ScheduleType = req.body.ScheduleType;
  if(ScheduleType=="FISH"){
    TurnOnTime = OnTimes[DayNum];
    TurnOffTime = OffTimes[DayNum];
  }else{
    TurnOnTime = OnTimesFrog[DayNum];
    TurnOffTime = OffTimesFrog[DayNum];
  }
  ShowSchedule = 1;
  
  sendSchedulePage(res);
}); 



// COMMONN

function sendSchedulePage(res){
   
   var PassVals = {
    viewDay:DayName[DayNum],
    viewTurnOn:TurnOnTime,
    viewTurnOff:TurnOffTime,
    ShowSchedule:ShowSchedule,
    ScheduleType:ScheduleType
  }
  res.render('html/Schedules.html',PassVals);
  ShowSchedule = 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// READ in Saved Schedule Data 
////////////////////////////////////////////////////////////////////////////////////////////////////////////


// FISH 

function ReadSchedule(DAY){
    // loop over the days and read them in. 
    //console.log('Day: ' + DAY);
    fs.readFile("ScheduleFiles/"+DAY.toString(), 'utf8', function(err, data) {
      if (err) throw err;
      
      //console.log(data);
      var dataSplit = data.split(","); 
      OnTimes[DAY] = dataSplit[0];
      OffTimes[DAY] = dataSplit[1]; 
     // console.log("Spit Data"+dataSplit[0]);
      DAY++; // step the day
      if(DAY<7){
        ReadSchedule(DAY);
      }else{ 
        // call the scheduler 
        // call the Frog Read Scheduler 
        FrogReadSchedule(0); 
      }
    });
}

// FROGS

function FrogReadSchedule(DAY){
    // loop over the days and read them in. 
    //console.log('Day: ' + DAY);
    fs.readFile("FrogScheduleFiles/"+DAY.toString(), 'utf8', function(err, data) {
      if (err) throw err;
      
      //console.log(data);
      var dataSplit = data.split(","); 
      OnTimesFrog[DAY] = dataSplit[0];
      OffTimesFrog[DAY] = dataSplit[1]; 
     // console.log("Spit Data"+dataSplit[0]);
      DAY++; // step the day
      if(DAY<7){
        FrogReadSchedule(DAY);
      }else{ 
        // call the scheduler 
        // call the Frog Read Scheduler 
        Scheduler();
      }
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SCHEDULER LOOP
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// main scehduler that runs the fish 
 
function Scheduler(){
  FrogScheduler(); // call the frog scheduler 
  var date = new Date();
  CurrentHour = date.getHours();
  CurrentMinute = date.getMinutes(); 
  CurrentDay = date.getDay(); 
  //console.log("Scheduler Time Hour: "+CurrentHour+ " Minute: "+CurrentMinute+" Day: "+CurrentDay);
  var OnRAW = OnTimes[CurrentDay];
  var OffRAW = OffTimes[CurrentDay];
  OnRAW = OnRAW.split(":");
  OffRAW = OffRAW.split(":");
  // makew time into decimals since decimals are funner 
  var TurnOnHour = parseFloat(OnRAW[0]);
  var TurnOffHour =parseFloat(OffRAW[0]);
  var TurnOnMinute = parseFloat(OnRAW[1]);
  var TurnOffMinute = parseFloat(OffRAW[1]);
  var TurnOnDec = TurnOnHour+TurnOnMinute/60.0;
  var TurnOffDec = TurnOffHour+TurnOffMinute/60.0;
  var CurrentTime = CurrentHour+CurrentMinute/60.0; 
  console.log("FISH: Turn on Hour: "+TurnOnHour+" Turn On Minute: "+TurnOnMinute/60+" Turn on Dec: "+TurnOnDec);
  console.log("FISH: Turn off Hour: "+TurnOffHour+" Turn Off Minute: "+TurnOffMinute/60+" Turn off Dec: "+TurnOffDec);
  if(SchedulerStatus==1){
    // only run this if we are supposed to run the scheduler
  
    console.log("Light Status: "+FishLightStatus+" CurrentTime: "+CurrentTime+" Turn On Time: "+ TurnOnDec+" Turn Off Time: "+TurnOffDec);
     if(TurnOnDec<TurnOffDec){
      console.log("Turning on before turning off");
      // turning off light after it is turned on 
        if(CurrentTime>TurnOnDec && CurrentTime<TurnOffDec &&FishLightStatus=="OFF"){
          // we need to turn on the light
          LIGHTS.turnON();
          FishLightStatus = "ON"; 
          console.log("Will Turn on Light");
          FishLightChangeEvent = "Scheduler";
          FishLightChangeTime = Date();
        }else if(CurrentTime>TurnOffDec && FishLightStatus=="ON"){
          // we need to turn off the light 
          LIGHTS.turnOFF();
          FishLightStatus = "OFF"; 
          console.log("Will Turn off Light");
          FishLightChangeEvent = "Scheduler";
          FishLightChangeTime = Date();
        }
      }else{
        //turn off light before it is turned on for the day 
        if(CurrentTime>TurnOnDec &&FishLightStatus=="OFF"){
          // we need to turn on the light
          LIGHTS.turnON();
          FishLightStatus = "ON"; 
        }else if(CurrentTime>TurnOffDec && CurrentTime < TurnOnDec && FishLightStatus=="ON"){
          // we need to turn off the light 
          LIGHTS.turnOFF();
          FishLightStatus = "OFF"; 
        }
      }
  }

  setTimeout(Scheduler, 2*60*1000);
}


/// FROG SCEDHULER 
function FrogScheduler(){
  var date = new Date();
  CurrentHour = date.getHours();
  CurrentMinute = date.getMinutes(); 
  CurrentDay = date.getDay(); 
  //console.log("Scheduler Time Hour: "+CurrentHour+ " Minute: "+CurrentMinute+" Day: "+CurrentDay);
  var OnRAW = OnTimesFrog[CurrentDay];
  var OffRAW = OffTimesFrog[CurrentDay];
  OnRAW = OnRAW.split(":");
  OffRAW = OffRAW.split(":");
  // makew time into decimals since decimals are funner 
  var TurnOnHour = parseFloat(OnRAW[0]);
  var TurnOffHour =parseFloat(OffRAW[0]);
  var TurnOnMinute = parseFloat(OnRAW[1]);
  var TurnOffMinute = parseFloat(OffRAW[1]);
  var TurnOnDec = TurnOnHour+TurnOnMinute/60.0;
  var TurnOffDec = TurnOffHour+TurnOffMinute/60.0;
  var CurrentTime = CurrentHour+CurrentMinute/60.0; 
  console.log("Frog: Turn on Hour: "+TurnOnHour+" Turn On Minute: "+TurnOnMinute/60+" Turn on Dec: "+TurnOnDec);
  console.log("Frog: Turn off Hour: "+TurnOffHour+" Turn Off Minute: "+TurnOffMinute/60+" Turn off Dec: "+TurnOffDec);
  if(SchedulerStatusFrog==1){
    // only run this if we are supposed to run the scheduler
   
      console.log("Light Status: "+FrogLightStatus+" CurrentTime: "+CurrentTime+" Turn On Time: "+ TurnOnDec+" Turn Off Time: "+TurnOffDec);
       if(TurnOnDec<TurnOffDec){
        console.log("Turning on before turning off");
        // turning off light after it is turned on 
          if(CurrentTime>TurnOnDec && CurrentTime<TurnOffDec &&FrogLightStatus=="OFF"){
            // we need to turn on the light
            LIGHTS.turnONFrog();
            FrogLightStatus="ON";
            console.log("Will Turn on Light");
            FrogLightChangeEvent = "Scheduler";
            FrogLightChangeTime = Date();
          }else if(CurrentTime>TurnOffDec && FrogLightStatus=="ON"){
            // we need to turn off the light 
            LIGHTS.turnOFFFrog();
            FrogLightStatus="OFF";
            console.log("Will Turn off Light");
            FrogLightChangeEvent = "Scheduler";
            FrogLightChangeTime = Date();
          }
        }else{
          //turn off light before it is turned on for the day 
          if(CurrentTime>TurnOnDec &&FrogLightStatus=="OFF"){
            // we need to turn on the light
            LIGHTS.turnONFrog();
            FrogLightStatus="ON";
          }else if(CurrentTime>TurnOffDec && CurrentTime < TurnOnDec && FrogLightStatus=="ON"){
            // we need to turn off the light 
            LIGHTS.turnOFFFrog();
            FrogLightStatus="OFF";
          }
        }

  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TEMP UPDATER
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// functions to update the frog and fish temps 

function updateFrogTemps(){
  fs.readFile('/home/pi/FROGS/Plots/currentT.tmp' , 'utf-8', function(err, data) {
      if (err) throw err;

      console.log("The new frog Temp is: "+data+" deg C");
      frogTemp = data; 
  });
  setTimeout(updateFrogTemps, 2*60*1000);
}


function updateFishTemps(){
  request.get('http://192.168.1.62/currentT.tmp', function (error, response, body) {
    if (!error && response.statusCode == 200) {
        fishTemp = body; 
        // Continue with your processing here.
        console.log("New Fish Temp is: "+fishTemp);
    }
  });

  setTimeout(updateFishTemps, 2*60*1000);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CHECK Light Status 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function checkFrogStatus(){
  console.log("Frog Light Status Checker"); 

  LIGHTS.checkLightStatusFrog(function(locFrogLightStatus){
      
        if(locFrogLightStatus=="ON"){
          NotFrogLightStatus = "OFF";
        }else{
          NotFrogLightStatus = "ON";
        }
       // console.log("Fish Light Status: "+locFishLightStatus); 
        FrogLightStatus = locFrogLightStatus; // define local to glboal 
      
    }); 
  setTimeout(checkFrogStatus, 2*60*1000);
}


function checkFishStatus(){
  console.log("Fish Light Status Checker");
    // FishLightStatus = LIGHTS.checkLightStatus(); 
   
  LIGHTS.checkLightStatus(function(locFishLightStatus){
      
      if(locFishLightStatus=="ON"){
        NotLightStatus = "OFF";
      }else{
        NotLightStatus = "ON";
      }
     // console.log("Fish Light Status: "+locFishLightStatus); 
      FishLightStatus = locFishLightStatus; // define local to glboal 
    //sendIndex(res);
  }); 
    setTimeout(checkFishStatus, 2*60*1000);
  }
///////////////////////////////////////////////////////////////////////////////////////////////
// FEEDER FUNCTIONS 

var fishFeedOptions = {
  host: '192.168.1.62',
  port: '8080',
  path: '/feedFish',
  method: 'post'
};

app.post('/feedFish',function(req,res){
   // get the user name 
   FishFeederChangeEvent = req.body.FishFeedName; 
   FishFeederChangeTime = Date(); 
   // call remmote program to feed the fish 
   console.log("Will feed fish by:"+FishFeederChangeEvent); 
    var post_req = http.request(fishFeedOptions, function(res) {
      res.setEncoding('utf8');
      res.on('data',function(chunck) {
        console.log('Server Post Req Response: ' + chunck);
      });
    });
    // dummy message
    var post_data = querystring.stringify({
        'compilation_level' : 'ADVANCED_OPTIMIZATIONS',
        'output_format': 'json',
        'output_info': 'compiled_code',
          'warning_level' : 'QUIET',
          'js_code' : 'codestring'
    });
    // post the data 
    post_req.write(post_data); 
    post_req.end(); 
    // log the feeding 
    fs.appendFile('./html/logs/FishFeeder.txt',"Fish Fed by: "+FishFeederChangeEvent+" at "+FishFeederChangeTime+"\n"); 


  res.redirect('/');
});


app.post('/feedFrogs',function(req,res){
  FrogFeederChangeEvent = req.body.FrogFeedName;
  FrogFeederChangeTime = Date();
  // spawn local feeding routine 
  console.log("Will feed the frogs by: "+FrogFeederChangeEvent);
  

  // log that this was done 
  fs.appendFile('./html/logs/FrogFeeder.txt',"Frogs Fed by: "+FrogFeederChangeEvent+" at "+FrogFeederChangeTime+"\n"); 


  res.redirect('/');
});
