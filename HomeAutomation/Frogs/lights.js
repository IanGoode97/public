// This is the JS that handles the turning on and off of the light 


const { Client } = require('tplink-smarthome-api');
 
const client = new Client();


// turn light on function 
exports.turnON = function(){
	console.log("This would turn ON the light");
	const plug = client.getDevice({host: '192.168.1.171'}).then((device)=>{
	  device.getSysInfo().then(console.log);
	  device.setPowerState(true);
	});
	return 1;
}
// turn off light function
exports.turnOFF = function(){
	console.log("This would turn OFF the light");
		const plug = client.getDevice({host: '192.168.1.171'}).then((device)=>{
	  device.getSysInfo().then(console.log);
	  device.setPowerState(false);
	});

	return 0;
}

var LightState;
var FishLightStatus = "OFF";

exports.checkLightStatus = function(callback){
	const plug = client.getDevice({host: '192.168.1.171'}).then((device)=>{
	  device.getSysInfo().then((LS)=>{
	//	console.log(LS.relay_state);
				LightState = LS.relay_state;
		        if(LightState==1){
	                	FishLightStatus = "ON";
		        }else{
	               		 FishLightStatus = "OFF"
        		}
        		callback(FishLightStatus);
			
	        
//		console.log(FishLightStatus);
		//sendLS();
	   });
	});
//	console.log("New Status: "+FishLightStatus)
}

//////////////////////////////////////////////////
// FROGS 
//////////////////////////////////////////////////


// do the same but for the frog light 
// turn light on function 
exports.turnONFrog = function(){
	console.log("This would turn ON the light");
	const plug = client.getDevice({host: '192.168.1.172'}).then((device)=>{
	  device.getSysInfo().then(console.log);
	  device.setPowerState(true);
	});
	return 1;
}
// turn off light function
exports.turnOFFFrog = function(){
	
		const plug = client.getDevice({host: '192.168.1.172'}).then((device)=>{
	  device.getSysInfo().then(console.log);
	  device.setPowerState(false);
	});

	return 0;
}

var LightStateFrog;
var FrogLightStatus = "OFF";

exports.checkLightStatusFrog = function(callback){
	const plug = client.getDevice({host: '192.168.1.172'}).then((device)=>{
	  device.getSysInfo().then((LS)=>{
	//	console.log(LS.relay_state);
				LightStateFrog = LS.relay_state;
		        if(LightStateFrog==1){
	                	FrogLightStatus = "ON";
		        }else{
	               		 FrogLightStatus = "OFF"
        		}
        		callback(FrogLightStatus);
			
	        
//		console.log(FishLightStatus);
		//sendLS();
	   });
	});
//	console.log("New Status: "+FishLightStatus)
}