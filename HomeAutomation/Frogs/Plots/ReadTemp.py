import os
import glob
import time
import datetime
 
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
 
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'
 
def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines
 
def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c
	
# save this temp data 
now = datetime.datetime.now()

fileName = str(now.year)+"_"
month = now.month; 
if(month<10):
    # wee need to add a leadeing zero to the month 
    fileName = fileName+"0"+str(month)
else:
    fileName = fileName + str(month)
# add the suffix 
fileName = fileName + "TempData.csv";
fileName = "/home/pi/files/" + fileName

TimeStamp = now.strftime("%Y-%m-%d %H:%M") 

Temp = read_temp()

# save this data 
with open(fileName, "a") as myfile:
    myfile.write(str(TimeStamp)+","+str(Temp)+"\n")
