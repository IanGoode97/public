#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <fstream>

#include <math.h>
#include <string>
#include <time.h>
#include <limits>
using namespace std;

#include "Functions.h"
// motor home definitions 
#define Motor1Home 0
#define Motor2Home 0
#define Motor3Home 0 

// THIS IS THE MAIN FILE

// GLOBAL VARIABLES START

/*
MOTOR DEFINTION 
	1--> Tx Polariztion Rotation 
	2--> Rx Polarization Rotation 
	3--> Rx Rad Cut Roation 
*/

// SETPOINTS FOR MOTORS, THESE Are the Desired motor angles 
float MotorSP[3];  


// motor postions 
float MotorPos[3]; 

// ANGLE DEAD ZONE 
float AngleDeadZone = 0.2;  // this is the maximum acceptable difference between the Setpoint and the Actual postions. 



// motor Max and Min Values 
// all units in degrees 
int TxRoationMin = 0 ; 
int TxRotationMax = 100; 
int RxRotationMin = 0; 
int RxRotationMax = 100; 
int RxPanMin = -90; 
int RxPanMax = 90; 
float MinStepSize = 0.2; 
int MaxStepSize = 90;  

//Sample Points Array 
float MaxNumPoints = (RxPanMax-RxPanMin)/MinStepSize; 
float CutPoints[1000];  

// Motion lockout 
int MotorLockOut = 0; // 0 is locked out, 1 is run 

// Global Run, this is used to kill all functions
int GlobalRun = 1;  //1 is run, 0 is kill 

//file path and name 
string SavingLocation; 

// calibration factor from teh BoreSiteCal 
float CalFactor; 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void){
	// This is the main function 
	MotorSP[0] = Motor1Home; 
	MotorSP[1] = Motor2Home; 
	MotorSP[2] = Motor3Home; 

	cout<<"Antenna Motor Test no Encoders "<<endl;  


	int Run = 1; 

	int MotorNum;
	ControlLoop(); 
	int steps;

	
    // the motors should not be under control 



    cout<<"Starting Options"<<endl;

	while(Run ==1){
		// main loop in Main 
		cout<< "Enter Motor Num (-1 for exit)"<<endl; 
		cin>>MotorNum; 
		if(MotorNum ==-1){
			cout<<"Exiting"<<endl; 
			break; 
		}else if(MotorNum<0 || MotorNum>0){
			cout<<"Enter a 0 to move "<<endl; 
		}else{ 
			// move motors use the setpoint as the number of steps to move 
			cout<<"Moving Motor: "<<MotorNum; 

			cout<<"Enter number of steps to move motors"<<endl; 
			cin>>steps; 
			cout<<"Moving motor "<<steps<<" steps"<<endl; 

			MotorSP[MotorNum] = steps; 
			ControlLoop(); 



		}

		

	}
	//call GPIO Clean up 

	gpioCleanUp(); 

	cout<<"Exiting Antenna Measurment Program"<<endl; 
	// end of main function 


}

