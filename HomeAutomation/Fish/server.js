var express = require('express');
var app = express();
var bodyParser = require('body-parser');
const { spawn } = require('child_process');
var fs = require('fs'); 
var http = require('http');
var querystring = require('querystring');
var env = Object.create(process.env); 
//hi
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json()); 

app.use(express.static('static'));

app.engine('html',require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', __dirname)

app.use(express.static(__dirname +'/webServer')); // serve data 
app.listen(80); 


/////////////////////////////////////////////////////////////////////////////////////////
// Streamer Stuff


/**
* Run this on a raspberry pi 
* then browse (using google chrome/firefox) to http://[pi ip]:8080/
*/



const WebStreamerServer = require('./StreamerGood/lib/raspivid');

  //public website
app.use(express.static(__dirname + '/StreamerGood/public'));
app.use(express.static(__dirname + '/StreamerGood/vendor/dist'));

const server  = http.createServer(app);
const silence = new WebStreamerServer(server);

server.listen(8080);
console.log("Listening on 8080 for stream ");
// this makes us not crash 
process.on('uncaughtException', function (err) {
    console.error(err.stack);
    console.log("Node NOT Exiting...");
});


//////////////////////////////////////////////////////////////////////////////////////////
// FISH FEEDER 
app.post('/feedFish',function(req,res){
  console.log("Will feed the fish");
  // spawn the fish feeding program 
  spawn('/home/pi/FISH/Feeder/FeedFish', [], { stdio: 'inherit' });
  console.log("Fish feeder has been spawned"); 
});
