#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <fstream>

#include <math.h>
#include <string>
#include <time.h>
#include <limits>
using namespace std;

#include "Functions.h"
// motor home definitions 
#define Motor1Home 0
#define Motor2Home 0
#define Motor3Home 0 

// THIS IS THE MAIN FILE

// GLOBAL VARIABLES START

/*
MOTOR DEFINTION 
	1--> Tx Polariztion Rotation 
	2--> Rx Polarization Rotation 
	3--> Rx Rad Cut Roation 
*/

// SETPOINTS FOR MOTORS, THESE Are the Desired motor angles 
float MotorSP[3];  


// motor postions 
float MotorPos[3]; 

// ANGLE DEAD ZONE 
float AngleDeadZone = 0.2;  // this is the maximum acceptable difference between the Setpoint and the Actual postions. 



// motor Max and Min Values 
// all units in degrees 
int TxRoationMin = 0 ; 
int TxRotationMax = 100; 
int RxRotationMin = 0; 
int RxRotationMax = 100; 
int RxPanMin = -90; 
int RxPanMax = 90; 
float MinStepSize = 0.2; 
int MaxStepSize = 90;  

//Sample Points Array 
float MaxNumPoints = (RxPanMax-RxPanMin)/MinStepSize; 
float CutPoints[1000];  

// Motion lockout 
int MotorLockOut = 0; // 0 is locked out, 1 is run 

// Global Run, this is used to kill all functions
int GlobalRun = 1;  //1 is run, 0 is kill 

//file path and name 
string SavingLocation; 

// calibration factor from teh BoreSiteCal 
float CalFactor; 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void){
	// This is the main function 
	MotorSP[0] = Motor1Home; 
	

	cout<<"Feed FISH"<<endl;  


	int Run = 1; 

	int MotorNum = 0 ;
	ControlLoop(); 
	int NumStepsForFeed = 4000;

	cout<<"Will move the motor "<<NumStepsForFeed<<" steps to feed the fish"<<endl;
	
    // the motors should not be under control 



    

	MotorSP[MotorNum] = NumStepsForFeed; 
	ControlLoop(); 



	
	//call GPIO Clean up 

	gpioCleanUp(); 

	cout<<"Exiting Antenna Measurment Program"<<endl; 
	// end of main function 


}

