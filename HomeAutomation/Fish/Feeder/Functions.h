// This is the header file that connects the files 

void Measure(int  );
void ControlLoop(void  );
void BoreCal(void  );
float getVNA(void); 
void updateMotorPos(void ); 
void gpioCleanUp(void); 
void encoderReader(int); // read encoders
void cleanUpSPI(void); 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// do variable declaration 

extern float MotorSP[3]; 


// motor postions 
extern float MotorPos[3]; 
extern float AngleDeadZone; 



// motor Max and Min Values 
// all units in degrees 
extern int TxRoationMin  ; 
extern int TxRotationMax ; 
extern int RxRotationMin; 
extern int RxRotationMax; 
extern int RxPanMin; 
extern int RxPanMax; 
extern float MinStepSize; 
extern int MaxStepSize;  

//Sample Points Array 
extern float MaxNumPoints; 
extern float CutPoints[1000];  

// Motion lockout 
extern  int MotorLockOut; // 0 is locked out, 1 is run 

// Global Run, this is used to kill all functions
extern  int GlobalRun;  //1 is run, 0 is kill 

//file path and name 
extern  string SavingLocation; 

extern float CalFactor; 
extern int CSpins[3];