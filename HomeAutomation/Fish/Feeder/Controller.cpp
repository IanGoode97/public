#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <fstream>
#include <thread> 
#include <wiringPiSPI.h> 
#include <wiringPi.h> 
#include <math.h>
#include <string.h>
#include <time.h>
using namespace std;
#include "Functions.h"

//#include "GPIOClass.h"

// This is the Controller File 

// storage space for MotorDriverFunction 
int MotorStates[3]; 

int StateList[4][8]; // there are 8 states per cycle.  

/*
A --> 0
B --> 1
A Bar --> 2 (C)
B Bar --> 3 (D)
*/
//Below List Pins that are used for Each Motor 
int MotorPins[4]; // rows are motors 1 through 3, the 4 cols follow the A through B bar rule
//GPIOClass* gpios[3][4]; // storage for GPIO stuff 
int FirstRun =1; // this is a value to decide if it is the first run or no 
//setup GPIOS for chip select on encoders
//POClass* gpioCS[3]; 
int CSpins[3]; // chip eect for encoders 

// with the L6208 Stpper driver 
// There are 3 pins needed per motor, a Clock called stepCLK
//  a reset called stepReset 
// a direction called stepDir 
//int stepCLKPins[3]; 
//int stepResetPins[3]; 
//int stepDirPins[3]; 

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gpioStartup(void){
// this inilizes allh GPIo pins 
	wiringPiSetup (); // this uses the wiriingPi pin standad which is a little weird 
	
		for(int pins=0;pins<4;pins++){
			pinMode(MotorPins[pins],OUTPUT); 
		}
		
	
	pinMode(7,OUTPUT);	
	// set all GPIO pins to zero to start 

	for(int pins=0;pins<4;pins++){
		digitalWrite(MotorPins[pins],0);	
	}	
	
	digitalWrite(7,0); // set enable low 

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void gpioCleanUp(void){
	cout<<"Cleaning up all Pins by forcing them to be inputs"<<endl; 
	
		
		for(int pins=0;pins<4;pins++){
			pinMode(MotorPins[pins],INPUT); 
		}
	
	digitalWrite(7,0); //set enable low 
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void writePins(int MotorNumber){
	//  this function does the GPIO writing to the stepper controller 
	for(int Pin=0;Pin<4;Pin++){
		// sets the GPIo value; 
		
		digitalWrite(MotorPins[Pin], StateList[Pin][MotorStates[MotorNumber]]);



		// Actuall Call the GPIO pin to do the things, for now it will just be a pritn statement 
	//	cout<<"Writing Pin: "<<Pin2Write<<" to "<<Val2Write<<endl; 

	}
	//cout<<"Wrote all gpio!"<<endl; 
	

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Old motor driver function when I wrote the step machine 
void MotorDriver(int MotorNumber, int Direction, int NumHalfSteps){
	// this is the motor driver function, it drives 3 two pole stepper motors using Darlington Transistor H bridges 
	// this code is half steppign, passing the half step values is easier than than deailing with floats 
	int MotorState = MotorStates[MotorNumber]; 

	// Direction: 0 is subtracting states 1 is increasign States 
	int HStepCounter = 0;  // number of half steps 
	int RunHere = 1; 
	int DelayBetweenSteps; 
	float Gain = 2.1*3000.0/(float)NumHalfSteps;  // Gain used for controlsystem 
	int DistanceFromEnd = 0; // this is the distance from the end point. We start at 0 so we will step up until Half 
	int Halfway = NumHalfSteps/2; 

	while(RunHere ==1){
		// this is the loop that runs through the states 
		if(Direction==1){
			// add to the value of motorStates 
			//Also Step the Counter 
			HStepCounter++;
			MotorStates[MotorNumber]++; 
			if(MotorStates[MotorNumber]>3){
				// this means we need to loop 
				MotorStates[MotorNumber] = (MotorStates[MotorNumber]) - 4;   // Will take a value of 8 and return it to 0 
			}

		}else if(Direction == 0){
			// substract from the value of MotorStates 
			//Also Step the Counter 
			HStepCounter++; 
			MotorStates[MotorNumber]--; 
			if(MotorStates[MotorNumber]<-0){
				// this means we need to loop 
				MotorStates[MotorNumber] = (MotorStates[MotorNumber]) + 4;  //  This will return a value of -1 to 7 
			}

		}else{
			cout<<"Bad Value for Direction, use only 1 or 0"<<endl; 
		}
		// call the pine writer
		writePins(MotorNumber);

		//See if we need to break 
		if(HStepCounter>NumHalfSteps){
			// this means we are at the desired number of steps 
			RunHere = 0; 
			cout<<"At Desired Number of Steps"<<endl;  
			break; 

		}else{
			// loop again, but add a delay before writing the next pin postion this should be done based on the difference between the number of steps done and the desired number of steps 

			// update Delay Between Steps 
			// we want to ramp at both ends, so that we are at a max when the Steps to go is half of the number of steps we have to go
			// Therefore we will accelerate for the first half ot the 
			if(HStepCounter<Halfway+1){
				// increase the size of Distance From End 
				DistanceFromEnd++; 
			}else{
				// we are past the halfway mark, start decreasing the distnace 
				DistanceFromEnd = NumHalfSteps-HStepCounter; 
			}

			// the larger the disance from end is the faster we want to go, thus we will be at a max speed when we are halfway

			float Speed = (float)DistanceFromEnd*Gain; 

			int MinDelay = 5000; // uSeconds 
			int MaxDelay = 5400; // uSeconds  

			DelayBetweenSteps = MaxDelay - Speed; 

			if(DelayBetweenSteps>MaxDelay){
				// bound it 
				DelayBetweenSteps = MaxDelay; 
			}else if(DelayBetweenSteps<MinDelay){ 
				// bound it 
				DelayBetweenSteps = MinDelay; 
			}
		//	cout<<"Sleeping "<<DelayBetweenSteps<<" us between steps"<<endl;
			// call the Delay 
			usleep(DelayBetweenSteps); 			

		}

	}
	// after loop set setpoint to zero 
	MotorSP[MotorNumber] = 0 ; 

}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ControlLoop(void ){

	// This is the Control Loop 

	// stepper motor pin assignmetns 
	MotorPins[0] = 22; MotorPins[1] = 23; MotorPins[2] = 24; MotorPins[3] = 25; 

	// list of possible states 
	/*
	StateList[0][0] = 1; StateList[0][1] = 1; StateList[0][2] = 0; StateList[0][3] = 0;StateList[0][4] = 0;StateList[0][5] =0;StateList[0][6] = 0; StateList[0][7] = 1;// A
	StateList[1][0] = 0; StateList[1][1] = 1; StateList[1][2] = 1; StateList[1][3] = 1;StateList[1][4] = 0;StateList[1][5] =0;StateList[1][6] = 0; StateList[1][7] = 0;// B
	StateList[2][0] = 0; StateList[2][1] = 0; StateList[2][2] = 0; StateList[2][3] = 1;StateList[2][4] = 1;StateList[2][5] =1;StateList[2][6] = 0; StateList[2][7] = 0;// A "B"ar 
	StateList[3][0] = 0; StateList[3][1] = 0; StateList[3][2] = 0; StateList[3][3] = 0;StateList[3][4] = 0;StateList[3][5] =1;StateList[3][6] = 1; StateList[3][7] = 1;// B "B"ar 
	// list of Chip speclect pins 
	*/
	StateList[0][0] = 1; StateList[0][1] = 0;StateList[0][2] =0; StateList[0][3] = 1;// A
	 StateList[1][0] = 1; StateList[1][1] = 1;StateList[1][2] =0; StateList[1][3] = 0;// B
	 StateList[2][0] = 0; StateList[2][1] = 1;StateList[2][2] =1; StateList[2][3] = 0;// A "B"ar 
	 StateList[3][0] = 0; StateList[3][1] = 0;StateList[3][2] =1; StateList[3][3] = 1;// B "B"ar 


	cout<<"In Control Loop Function"<<endl; 

	// setup GPIO pins at first run 
	if(FirstRun==1){
		// this is the first run, we should inilize the GPIO 
		FirstRun++; // make it not equal to 1 
		// call the gpioStartup 
		gpioStartup(); 
		cout<<"************************************************************Setup GPIO Pins********************************************************************"<<endl; 
		

	}

	int HalfSteps2Deg = 4; // This is the number of Half Steps to move 1 degree 

	int RunHere = 1; // this is a run value for the while loop that the controller is 
	float MotorDiff; // Difference between the setpoints and the Motor Postions 
	float MotorDiffMax; // Maximum motor Diff 
	int MoveMotor; // An array of 3 motors move the ones that are 1 dont move the oens that are 0  
	int MoveMotorSum = 0; // a value to show how many motors to mvoe 

	int NumSteps; // Number of steps we are going to ask teh system to move 
	int Direction; // direction we are going ask the system to mvoe 
	digitalWrite(7,1); // write enable high 
	while(RunHere ==1){
		// loop to loop in until we have good values 
		// first get Motor Postions 
		MoveMotorSum = 0; // leave at 0  until we check this 
		//updateMotorPos();  
		

		// update the Motor Differences 
		int MotorNumber=0;
			// step through the three motors 
			// update Encoders 
			

			cout<<"Current Difference is: "<<MotorSP[MotorNumber]<<endl; 
			usleep(1000); 

			if(fabs(MotorSP[MotorNumber])>10){
				// we want to move this motor 
				MoveMotorSum++;
				MoveMotor = 1; 
				// calcualte the number of steps 
				NumSteps = fabs(MotorSP[MotorNumber]); // gives the number of half steps we need to move the motor to the desired postion 

				// find the direction 
				if(MotorSP[MotorNumber]>0){
					// go forward 
					Direction = 1; 
				}else{ 
					Direction = 0; 
				}
				// call the function that mvoes the motors 
				cout<<"Calling MotorDriver Function"<<endl; 
				MotorDriver(MotorNumber,Direction,NumSteps); 

			}
			// the else case is we dont want to move the motor 
		

		if(MoveMotorSum==0){
			// if this is still equal to zero that means none of the motors need to mvoe, thus we can break 
			RunHere == 0; 
			break;  
		}
		// The else case is we want to run the loop again since we didnt get all the motors on the first try.  

	}

	// if we broke the while loop this means all motors are at the postion 
	cout<<"All joints should be at their desired postion"<<endl; 
	digitalWrite(7,0); // write enable low  after all motors are in position 	


	
}

// END OF FILE 
