##
#This is a python file to plot the time data. This will run everyso often to update the graphs 
import datetime
import numpy as np
import matplotlib
matplotlib.use('Agg')
import pylab as p 
import matplotlib.ticker as ticker
import csv #to read the csv file 
import matplotlib.dates as mdates
Humid = []
Temp = []
Day = []
DayNumber=[]
Month = []
Year = []
dt = []
Time = []

#get file strings for the current month 
now = datetime.datetime.now(); 
if now.month >9:
	fileStringCurrent ='/home/pi/files/'+str(now.year)+'_'+str(now.month)+'TempData.csv'
else: 
	fileStringCurrent ='/home/pi/files/'+str(now.year)+'_'+'0'+str(now.month)+'TempData.csv'


print fileStringCurrent
file = open(fileStringCurrent,'rb')
for line in file.readlines():
    rows = line.split(",")
    
    Temp.append(rows[1])
    #split Time adn Date
    TimeLine2 = rows[0]

    dt.append(TimeLine2)
	
	

Time = [datetime.datetime.strptime(elem,"%Y-%m-%d %H:%M") for elem in dt]
# plot stuff 
fig, ax = p.subplots()

ax.plot(Time,Temp,'ro')
myFmt = mdates.DateFormatter("%b %d")
ax.xaxis.set_major_formatter(myFmt)
#p.title('FISH Temps')
p.xlabel('Date (Data taken every 10 minutes)')
p.ylabel('Temp [degsC]')
#p.savefig('/var/www/homewiki/images/temps.png')
p.savefig('/home/pi/FISH/webServer/PlotsCurrent.png')
p.close("all")



#####
# Added saving of the last line fo the temp and humidity to be able to call this data 
TempLast = Temp[-1]

fileString = '/home/pi/FISH/webServer/currentT.tmp'
tF = open(fileString,'w')
string2write = str(TempLast)
tF.write(string2write)
tF.close()

