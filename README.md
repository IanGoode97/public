# Public
Hello, my name is Ian Goode, I am a PhD Candidate in Electrical Engineering at Queen's University (Kingston Ontario) with an expected graduation date of Aug. 2019
This is a public REPO to exhibit some of the code I have written.  
Each folder will be a sample of, or the total of the project, this will try to give you an idea of some of the code I have worked on plus the style I use. 
This file can be used as a bit of an index to get an idea of what each folder is. 

Thanks for reading this readme, and if you have any questions please feel to contact me at ian.goode@queensu.ca 

# Index 
## Antenna Test Chamber 
This is the code that I wrote and is used to run the antenna test chamber I built at Queen's. This is written in C++. There is no GUI for this the program is run in a shell and the program is setup to use text based menus to give a UI.  

## QSET Control Systems 
This is the code that I wrote for the 2017-2018 QSET rover (http://QSET.ca) when I was CTO. The subdirectories in this project have the control system for the rover wheels, arm, and science collection system (scifoot). 
The rover was a four wheeled astronaut assistance vehicle prototype that competed in the annual University Rover Challenge, hosted by the Mars Society in Hanksville Utah. 
The wheel control system allowed for individual speed control on each wheel to maintain traction over varying terrain.  
The arm control system controlled a five degree of freedom arm that could lift 5 kg at full extension. This implemented full position control. 
The science control system was used to control the bucket wheel conveyor and sample storage of the science sample task where the rover had to collect a soil sample and measure sub-surface soil conditions.  

## Home Automation 
These two projects were used to remotely and automatically monitor, feed, view, and control my girlfriends fish tanks.  This is written in JS using a nodeJS server running on two separate PiZeroWs (one for each tank) that monitor and post the current temperature, interface with smart plugs to run the tank lights, have a stream to view the fish and the ability to feed the fish using a stepper motor driven auger system that I 3D printed at home. 

## Homebrewed Impedance Solvers for Planar Transmission Lines 
These were part of a project I did the summer before I started my masters while I was a research assistant at Queen's these are written in C++ and are multi-threaded impedance solvers that use a FEM to solve the Laplace equation around a transmission line for a variety of geometries. These were made multi-threaded for fun and to reduce computing time. 