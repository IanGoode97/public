float getZ2(void);
void Worker(int threadid);
void ThreadStartup(void);

// extern vars 

extern int numrows;
extern int numcols;
extern float VMaster[numrows][numcols];
extern float V[numrows][numcols];
extern float VSub[numrows][numcols];
extern float Epsilon[numrows][numcols]; 
extern int LineRow; 
extern int LineWidth;  
extern int numThreads;
extern int Run = 1; 
extern int Boundaries[4][25]; // this matrix stores the start and stop info 
//^ in each column the info is {RowStart;RowStop;ColStart;ColStop}
extern float Z; 
extern float ZLast; 