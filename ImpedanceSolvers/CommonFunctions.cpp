#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <thread>
#include <math.h>
#include <string.h>
#include <time.h>
#include <atomic>
#include <fstream>
#include "FunctionHead.h"
//#include "Workers.h"

using namespace std;

atomic<bool> ThreadRun (true); 



float getZ2(){
	float qsum = 0.0; // for the one with dieletric
	float qsumAir = 0.0; // the qusm for the completly air filled test 	

	float V0; 
	float V0Air;

	int BottomRow = LineRow/3;//LineRow/2; 
 
	int TopRow = 5*LineRow/3;//(numrows-LineRow)/2 + LineRow; 
	int LeftCol =numcols/2 -4*LineWidth/3;// LineStart/2; 
	int RightCol =numcols/2 +4*LineWidth/3; // numcols-LineStart/2; 
	
	// first loop across the columns 
	
	for(int cols = LeftCol;cols<RightCol;cols++){
		float Ertop = Epsilon[TopRow][cols];
		float Erbottom = Epsilon[BottomRow][cols]; 
		
		float diffTop = (V[TopRow+1][cols]-V[TopRow-1][cols])/2;
		float diffBottom = (V[BottomRow-1][cols]-V[BottomRow+1][cols])/2;
		
		qsum = qsum + diffTop*Ertop + diffBottom*Erbottom; 
		qsumAir = qsumAir + diffTop + diffBottom; 
	}
	
	
	float qsumcols = 0.0;
	float qsumcolsAir = 0.0;
	for(int rows = BottomRow; rows<TopRow; rows++){
		float Er1 = Epsilon[rows][LeftCol]; 
		float diffLeft = (V[rows][LeftCol-1]-V[rows][LeftCol+1])/2;
		float diffRight = (V[rows][RightCol+1]-V[rows][RightCol-1])/2; 
		//printf("%0.5f %0.5f \n", diffLeft, diffRight);
		qsumcols = qsumcols + diffLeft*Er1+diffRight*Er1;
		qsumcolsAir = qsumcolsAir + diffLeft + diffRight; 
		//printf("Col Sum Vals, Left: %0.5f, Right: %0.5f \n", diffLeft, diffRight); 
	}
	qsum = qsum + qsumcols;
	qsumAir = qsumAir + qsumcolsAir;
	
	
	
	float Z0 = 377.0/sqrt(qsum*qsumAir)*1000.0;


	float Zdiff; 

    Zdiff = fabs(Z0-ZLast); 

    if(Zdiff<0.1){
    	// stop the threads 
    	Run =0; 
    	ThreadRun = false; 
    	// the above doesnt seem to actually stop the threads
    	/*for(int n = 0;n<numThreads;n++){
    		terminate(t[n]); 
    	}*/
    }else{
    	ZLast = Z0; 
    	cout<<"The value of the Impedance is: "<< Z0<<" For Line Width: "<<LineWidth<< endl; 
    }

	return Z0; 
}



void Worker(int threadid){
	// worker 
	
	int id = threadid;// *((int*)(&threadid));	
	cout<<"Starting Worker:  "<<id<< endl; 
	ThreadRun = true; 
	
	int WorkerCounter = 0; 
	int rows;
	int cols;
	int rowStart =  Boundaries[0][id];
	int rowStop = Boundaries[1][id];
	int colStart = Boundaries[2][id];
	int colStop = Boundaries[3][id];

	float ErTop; 
	float ErBottom;  


	cout<< "Worker RowStart, RowStop, ColStart, ColStop "<<rowStart<<","<<rowStop<<","<<colStart<<","<<colStop<<endl; 

	while(ThreadRun){
		for(rows = rowStart;rows<rowStop;rows++){
			for(cols = colStart;cols<colStop;cols++){
				if(VMaster[rows][cols]<1){
					// if V master is less than 1 we can do some stuff 
					V[rows][cols] = (V[rows+1][cols]+V[rows-1][cols]+V[rows][cols+1]+V[rows][cols-1])/4;

					ErTop = Epsilon[rows+1][cols]; 
					ErBottom = Epsilon[rows-1][cols]; 

					VSub[rows][cols] = (VSub[rows+1][cols]*ErTop+ErBottom*VSub[rows-1][cols] +((ErBottom+ErTop)/2) * (VSub[rows][cols-1]+VSub[rows][cols+1])); 
				}
			}
		}
		if(threadid== 0){
			// only run this for one thread 
			if(WorkerCounter%200==0){
				// if the thread has looped 200 times check a function to stop it 

				getZ2(); // this function looks for the impedance; 
			}

			WorkerCounter++; 
		}
	
	}
	cout<<"Done Thread: "<<id<<endl;
//	t[id].join(); 
	//pthread_exit(NULL);

}




void threadStartup(){
	// ask for number of Threads 
	 
	// need to make sure that num threads is an even number or a square of an int
	int getThreads =1; 
	while(getThreads==1){
		// loop this until we get a good value 

		// check to see if the value is good 

		if(numThreads>25 && numThreads<0){
			cout<<"Please Enter a number of threads less than 25"<<endl; 
		}else{
			// small enough values of threads check that the value is not dumb 
			if(numThreads%2==0){
				// we are even therefore we can work with this 
				cout<< "Thread value is even"<<endl; 
				getThreads = 0; 
				break; 
			}else if(numThreads==9 || numThreads==25||numThreads==1){
				// two odd but nice squares
				cout<< "Thread value is {1,3,5}^2" <<endl;  
				getThreads =0 ; 
				break; 
			}else{
				//bad case
				cout<< "Please Enter an Even Number of Threads or number where its square root is an int"<<endl; 

			}
		}
	}

	cout<< "Running with: " << numThreads<<" Threads"<<endl; 


	int iSQRTThreads = sqrt(numThreads);
   float fSQRTThreads = sqrt(numThreads);
   if(fabs(iSQRTThreads-fSQRTThreads)<0.01){
	   	// this means that the number of Threads has an integer square root 
	   	// setup a square grid 
   		cout<<"Setting up Square ThreadLayout " <<endl; 
	   	int ThreadCount = 0; 
	   	int RowStart;
	   	int RowStop; 
	   	int ColStart; 
	   	int ColStop;  
	   	int sqrtThread = sqrt(numThreads);
	   	for(int rows = 0; rows< sqrtThread;rows++){
	   		for(int cols = 0; cols < sqrtThread; cols++){
	   			// start by defining the stop col and the stop row
	   			// the starts will be defined by the ajacent elements stop rows 

	   			 /* Index note for the Boundaries Mat 
	   			 0 -->  Row Start
	   			 1 --> Row Stop 
	   			 2 --> Col Start
	   			 3 --> Col Stop 

	   			 */ 
	   			RowStop = (rows+1) * numrows/ sqrtThread; 
	   			ColStop = (cols+1) * numcols/ sqrtThread; 
	   			RowStart = (rows) * numrows/ sqrtThread; 
	   			ColStart = (cols) * numcols/ sqrtThread; 



	   			Boundaries[1][ThreadCount] = RowStop; 
	   			Boundaries[3][ThreadCount] = ColStop;   

	   		
   				Boundaries[0][ThreadCount] = RowStart; 
   				Boundaries[2][ThreadCount] = ColStart;

   				for(int i=0; i<4;i++){
   					if(Boundaries[i][ThreadCount]<1){
   						Boundaries[i][ThreadCount]=1; 

   					}else if(Boundaries[i][ThreadCount]>numrows-1 && i<2){
   						// this means we are outside the bounds of rows 
   						// the i<2 term makes sure we are looking at only rows 
   						Boundaries[i][ThreadCount] = numrows-1; 

   					}else if(Boundaries[i][ThreadCount]>numcols-1 && i>1){
   						// this means we are outside the bounds of cols 
   						// the i<2 term makes sure we are looking at only cols 
   						Boundaries[i][ThreadCount] = numcols-1; 
   					}
   				}


	   			// at last step the thread count 
	   			ThreadCount++;
	   		}
	   	}


   }else{
	   	// number of threads is even just do a row and column thing 

	   	// define the number of coloumns based on the number of trheads 
   		cout<< "Setting Up Rectangular ThreadLayout" <<endl;  
	   	int ThreadCol; 

	   for(int i = 2; i<7;i++){
	   		if(numThreads%i == 0 ){
	   			// we are divisiable by that number therefore we will use that as the number of columns 
	   			ThreadCol = i;  
	   		}
	   }

	   int ThreadRow = numThreads/ ThreadCol; 
	   int RowStop; 
	   int ColStop; 
	   int RowStart; 
	   int ColStart;  
	   // setup the boundaries 
	   int ThreadCount = 0; 

	   for(int rows = 0; rows< ThreadRow;rows++){
	   		for(int cols = 0; cols < ThreadCol; cols++){
	   			// start by defining the stop col and the stop row
	   			// the starts will be defined by the ajacent elements stop rows 

	   			 /* Index note for the Boundaries Mat 
	   			 0 -->  Row Start
	   			 1 --> Row Stop 
	   			 2 --> Col Start
	   			 3 --> Col Stop 

	   			 */ 
	   			RowStart = rows * numrows/ ThreadRow; 
	   			ColStart = cols * numcols/ ThreadCol; 

	   			RowStop = (rows+1) * numrows/ ThreadRow; 
	   			ColStop = (cols+1) * numcols/ ThreadCol; 

	   			Boundaries[1][ThreadCount] = RowStop; 
	   			Boundaries[3][ThreadCount] = ColStop;   

	   		
   				Boundaries[0][ThreadCount] = RowStart; 
   				Boundaries[2][ThreadCount] = ColStart;

   				for(int i=0; i<4;i++){
   					if(Boundaries[i][ThreadCount]<1){
   						Boundaries[i][ThreadCount]=1; 

   					}else if(Boundaries[i][ThreadCount]>numrows-1 && i<2){
   						// this means we are outside the bounds of rows 
   						// the i<2 term makes sure we are looking at only rows 
   						Boundaries[i][ThreadCount] = numrows-1; 

   					}else if(Boundaries[i][ThreadCount]>numcols-1 && i>1){
   						// this means we are outside the bounds of cols 
   						// the i<2 term makes sure we are looking at only cols 
   						Boundaries[i][ThreadCount] = numcols-1; 
   					}
   				}


	   			// at last step the thread count 
	   			ThreadCount++;
	   		}
	   	}

   }

   // for the case where there is only one thread just re do it 
   if(numThreads ==1){
   	cout<<"Define Threads for Single Thread Operation" <<endl;  
   	Boundaries[1-1][0] = 1;
   	Boundaries[2-1][0] = numrows-1; 
   	Boundaries[3-1][0]  = 1;
   	Boundaries[3][0] = numcols-1; 
   }
}
