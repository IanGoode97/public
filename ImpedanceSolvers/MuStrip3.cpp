// Mustrip Line Width cal from impedance V2? 

// this was modified from the orrginal to model a water channel 
#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <thread>
#include <math.h>
#include <string.h>
#include <time.h>
#include <atomic>
#include <fstream>
#include <cmath>
#include "matplotlibcpp.h"


//#include "Workers.h"

using namespace std;
namespace plt = matplotlibcpp;

// declare threads
thread t[4];
// thse maxes are a hard max 
#define numrowsM 10000
#define numcolsM 12000

int numrows;
int numcols;

float VMaster[numrowsM][numcolsM];
float V[numrowsM][numcolsM];
float VSub[numrowsM][numcolsM];
float Epsilon[numrowsM][numcolsM]; 
int LineRow; 
int LineWidth;  
int numThreads = 4;
int Run = 1; 
atomic<bool> ThreadRun (true); 
int Boundaries[4][4]; // this matrix stores the start and stop info 
//^ in each column the info is {RowStart;RowStop;ColStart;ColStop}
float Z = 0.0; 
float ZLast = 0.0; 

	float VP ;

float getZ(){
	float qsum = 0.0; // for the one with dieletric
	float qsumAir = 0.0; // the qusm for the completly air filled test 	

	int BottomRow = LineRow/3;//LineRow/2; 
 
	int TopRow = 5*LineRow/3;//(numrows-LineRow)/2 + LineRow; 
	int LeftCol =numcols/2 -4*LineWidth/3;// LineStart/2; 
	int RightCol =numcols/2 +4*LineWidth/3; // numcols-LineStart/2; 
	
	
	// first loop across the columns 
	float diffTopSub;
	float diffBottomSub;
	float diffTop;
	float diffBottom;
	// these are the averaged Ers at each point long the side
	float ErTop;
	float ErBottom; 

	
	for(int cols = LeftCol;cols<RightCol;cols++){
		// Find difference along the top and bottom of the contour square for the area with the deletric removed
		diffTop = (V[TopRow+1][cols]-V[TopRow-1][cols])/2.0;
		diffBottom = (V[BottomRow-1][cols]-V[BottomRow+1][cols])/2.0;
		// sum for the Er =1 case with the air removed 
		qsumAir = qsumAir + diffTop + diffBottom;  
		// repeat for the case with the dietlectric
		// find the average Er at a given point, this is incase the contour passes through a region of ununiform dieletric 
		ErTop = (Epsilon[TopRow][cols+1]+Epsilon[TopRow][cols-1]+Epsilon[TopRow+1][cols]+Epsilon[TopRow-1][cols])/4.0;
		ErBottom = (Epsilon[BottomRow][cols+1]+Epsilon[BottomRow][cols-1]+Epsilon[BottomRow+1][cols]+Epsilon[BottomRow-1][cols])/4.0;

		diffTopSub = ErTop*(VSub[TopRow+1][cols]-VSub[TopRow-1][cols])/2.0;
		diffBottomSub = ErBottom*(VSub[BottomRow-1][cols]-VSub[BottomRow+1][cols])/2.0;

		// sum along the contour
		qsum = qsum + diffTopSub + diffBottomSub;	
	}

	float diffLeft;
	float diffRight;
	float diffLeftSub;
	float diffRightSub;
	// these are the averaged Ers at each point long the side
	float ErLeft; 
	float ErRight;
	
	// now sum along the vertical sides of the contour 
	for(int rows = BottomRow; rows<TopRow; rows++){
		// find the difference along the sides for case of no dieletric 
		diffLeft = (V[rows][LeftCol-1]-V[rows][LeftCol+1])/2.0;
		diffRight = (V[rows][RightCol+1]-V[rows][RightCol-1])/2.0; 
		// sum after each run 
		qsumAir = qsumAir + diffLeft + diffRight; 

		// solve for the case with the dieletric 
		// find the average Er for the left and right colloumn for a poitn where the side might be on a boundary of the dieletric
		ErLeft = (Epsilon[rows][LeftCol+1]+Epsilon[rows][LeftCol-1]+Epsilon[rows+1][LeftCol]+Epsilon[rows-1][LeftCol])/4.0;
		ErRight = (Epsilon[rows][RightCol+1]+Epsilon[rows][RightCol-1]+Epsilon[rows+1][RightCol]+Epsilon[rows-1][RightCol])/4.0;

		diffLeftSub = ErLeft*(VSub[rows][LeftCol-1]-VSub[rows][LeftCol+1])/2;
		diffRightSub = ErRight*(VSub[rows][RightCol+1]-VSub[rows][RightCol-1])/2; 
		//Sum over the controur 
		qsum = qsum + diffLeftSub + diffRightSub; 
	}
	
	
	
	
	float Z0 = 377.0/sqrt(qsum*qsumAir)*1000.0;

	VP = sqrt(qsum/qsumAir); 
	float Zdiff; 

    Zdiff = fabs(Z0-ZLast); 

    if(Zdiff/Z0<0.0001){
    	// stop the threads with 0.2% error 
    	Run =0; 
    	ThreadRun = false; 
    	// the above doesnt seem to actually stop the threads
    	/*for(int n = 0;n<numThreads;n++){
    		terminate(t[n]); 
    	}*/
    }else{
    	ZLast = Z0; 
    	cout<<"The value of the Impedance is: "<< Z0<<" For Line Width: "<<LineWidth<< endl; 
    }

	return Z0; 
}


void Worker(int threadid){
	// worker 
	
	int id = threadid;// *((int*)(&threadid));	
	cout<<"Starting Worker:  "<<id<< endl; 
	
	
	int WorkerCounter = 0; 
	int rows;
	int cols;
	int rowStart =  Boundaries[0][id];
	int rowStop = Boundaries[1][id];
	int colStart = Boundaries[2][id];
	int colStop = Boundaries[3][id];

	float ErTop; 
	float ErBottom;  
	float ErLeft;
	float ErRight;

	float VLeft; float VRight; float VTop; float VBot; 

	cout<< "Worker RowStart, RowStop, ColStart, ColStop "<<rowStart<<","<<rowStop<<","<<colStart<<","<<colStop<<endl; 

	while(ThreadRun){
		for(rows = rowStart;rows<rowStop;rows++){
			for(cols = colStart;cols<colStop;cols++){
				if(VMaster[rows][cols]<1){
					// if V master is less than 1 we can do some stuff 
					V[rows][cols] = (V[rows+1][cols]+V[rows-1][cols]+V[rows][cols+1]+V[rows][cols-1])/4.0;

					//Er above the current point 
					ErTop = Epsilon[rows+1][cols]; 
					// Er below the current point
					ErBottom = Epsilon[rows-1][cols];

					ErLeft = Epsilon[rows][cols+1];
					ErRight = Epsilon[rows][cols-1]; 

					// Find F kownign the substrate 
				//	VSub[rows][cols] = (VSub[rows+1][cols]*Epsilon[rows+1][cols] + VSub[rows-1][cols]*Epsilon[rows-1][cols] + ((Epsilon[rows+1][cols-1]+Epsilon[rows-1][cols-1])/2)*VSub[rows][cols-1]  + ((Epsilon[rows+1][cols+1]+Epsilon[rows-1][cols+1])/2)*VSub[rows][cols+1]) / (Epsilon[rows+1][cols+1]+Epsilon[rows-1][cols+1]+Epsilon[rows+1][cols-1]+Epsilon[rows-1][cols-1]); 
				//	VSub[rows][cols] = (VSub[rows+1][cols]*ErTop + ErBottom*VSub[rows-1][cols] +((ErBottom+ErTop)/2) * (VSub[rows][cols-1]+VSub[rows][cols+1]))/(2*ErTop+2*ErBottom); 
	

				VTop = VSub[rows+1][cols]*ErTop;
				VBot = VSub[rows-1][cols]*ErBottom; 
				VLeft = VSub[rows][cols+1]*ErLeft;
				VRight = VSub[rows][cols-1]*ErRight;  
				VSub[rows][cols] = (VTop+VBot+VLeft+VRight)/((ErTop+ErBottom+ErLeft+ErRight)); 
				}
			}
		}
		if(threadid== 0){
			// only run this for one thread 
			if(WorkerCounter%500==0){
				// if the thread has looped 500 times check a function to stop it 
				
				getZ(); // this function looks for the impedance; 
			}

			WorkerCounter++; 
		}
	
	}
	cout<<"Done Thread: "<<id<<endl;
//	t[id].join(); 
	//pthread_exit(NULL);

}

void threadStartup(){
	cout<< "Running with: " << numThreads<<" Threads"<<endl; 

	int iSQRTThreads = sqrt(numThreads);
   float fSQRTThreads = sqrt(numThreads);
	cout<<"Setting up Square ThreadLayout " <<endl; 
	int ThreadCount = 0; 
	int RowStart;
	int RowStop; 
	int ColStart; 
	int ColStop;  
	int sqrtThread = sqrt(numThreads);
	for(int rows = 0; rows< sqrtThread;rows++){
		for(int cols = 0; cols < sqrtThread; cols++){
			// start by defining the stop col and the stop row
			// the starts will be defined by the ajacent elements stop rows 

			 /* Index note for the Boundaries Mat 
			 0 -->  Row Start
			 1 --> Row Stop 
			 2 --> Col Start
			 3 --> Col Stop 

			 */ 
			RowStop = (rows+1) * numrows/ sqrtThread; 
			ColStop = (cols+1) * numcols/ sqrtThread; 
			RowStart = (rows) * numrows/ sqrtThread; 
			ColStart = (cols) * numcols/ sqrtThread; 



			Boundaries[1][ThreadCount] = RowStop; 
			Boundaries[3][ThreadCount] = ColStop;   

		
			Boundaries[0][ThreadCount] = RowStart; 
			Boundaries[2][ThreadCount] = ColStart;

			for(int i=0; i<4;i++){
				if(Boundaries[i][ThreadCount]<1){
					Boundaries[i][ThreadCount]=1; 

				}else if(Boundaries[i][ThreadCount]>numrows-1 && i<2){
					// this means we are outside the bounds of rows 
					// the i<2 term makes sure we are looking at only rows 
					Boundaries[i][ThreadCount] = numrows-1; 

				}else if(Boundaries[i][ThreadCount]>numcols-1 && i>1){
					// this means we are outside the bounds of cols 
					// the i<2 term makes sure we are looking at only cols 
					Boundaries[i][ThreadCount] = numcols-1; 
				}
			}


			// at last step the thread count 
			ThreadCount++;
		}
	}


  }

int main(){
	cout<<"Modified Mu Strip Program to Handle Fluid Channel"<<endl; 
	// main function make some threads run them and do things  

	//ask for inputs 
	// choose unit type
	string units; // 1 is thou, 2 is mm 
	while (true){
		cout<< "Enter your desired units: thou or mm"<<endl; 
		cin>>units;
		if(units=="thou"){
			cout<<"You are working in thou"<<endl; 
			break;
		}else if("mm"){
			cout<<"You are working in mm"<<endl;
			break;
		}else{
			cout<<"Enter thou or mm"<<endl; 
		}
	}

	float Er; // relative dieletric constant 
	cout<<"Please enter the desired relative dieletric constant for the substrate"<<endl;
	cin>>Er; 
	cout<<"Workign with Er = "<<Er<<endl; 

	float MeshSize; 
	
	cout<<"Enter desired number of mesh points per "<<units<<endl;
	cin>>MeshSize;
	cout<<"Mesh set at "<<MeshSize<<" points per "<<units<<endl;

	float StripWidth; 
	cout<<"Enter Strip Width"<<endl; 
	cin>>StripWidth;
	cout<<"Working with Strip Width of "<<StripWidth<<units<<endl; 

	float LineHeight; 
	cout<<"Enter Height between the line and ground"<<endl;
	cin>>LineHeight;
	cout<<"Working with Line Height of "<<LineHeight<<units<<endl;  

	float ChannelW;
	float ChannelH; 
	float ChannelEr;
	cout<<"Enter Channel Width: ";
	cin>>ChannelW;
	cout<<endl<<"Working with a channel of width under line: "<<ChannelW<<units<<endl; 

	cout<<"Enter Channel Hieght: ";
	cin>>ChannelH;
	cout<<endl<<"Working with a channel of height under line: "<<ChannelH<<units<<endl; 	

	cout<<"Enter Channel Er: ";
	cin>>ChannelEr; 
	cout<<"Working with Channel Er of "<<ChannelEr<<endl;


	// convert all to one unit set 
	if(units=="mm"){
		//convert to thou 
		float mm2thou = 39.3701; // thou per mm
		MeshSize = MeshSize/mm2thou;
		StripWidth = StripWidth*mm2thou;
		LineHeight = LineHeight*mm2thou; 
		ChannelW = ChannelW*mm2thou;
		ChannelH *=mm2thou; 

	}
	cout<<"mesh points per thou "<<MeshSize<<endl;
	// convert into mesh sizes 
	// round to the nearest int
	LineWidth  = floor(MeshSize*StripWidth+0.5);
	LineRow = floor(MeshSize*LineHeight+0.5); 
	ChannelW = floor(MeshSize*ChannelW+0.5);
	ChannelH = floor(MeshSize*ChannelH+0.5);

	cout<<"Channel Width Mesh Points: "<<ChannelW<<" Channel Height Mesh Points: "<<ChannelH<<endl;

	numrows = 10*LineRow; // for strip line this is simply 2 times the height because the top of the system is ground 
	numcols = 15*LineWidth; 

	cout<<"Setting numrows at "<<numrows<<" Setting Numcols at "<<numcols<<endl; 

	if(numrows>numrowsM || numcols>numcolsM){
		cout<<"mesh set to small this will break everything, exiting"<<endl; 
		exit(0); 
	}
	
   int rows; 
   int cols; 
   cout<< "Zeroing Arrays" <<endl; 
   //set the V matrixies to zero 
   for(rows =0;rows<numrows;rows++){
   	for(cols=0;cols<numcols; cols++){
   		// set the guys to zero 
   		V[rows][cols] = 0.0; 
   		VMaster[rows][cols]=0.0; 
   		VSub[rows][cols] = 0.0; 
   		Epsilon[rows][cols] = 1.0;  
   	}
   }

   
    // set high voltages 

    for(cols = numcols/2 - LineWidth/2; cols < numcols/2 +LineWidth/2;cols++){
	   	VMaster[LineRow][cols] = 1.0; // so we know not to solve here 
	   	V[LineRow][cols] = 1000.0; 
	   	VSub[LineRow][cols] = 1000.0; 
	}

  
	// setup dieletric 
   for(rows = 0; rows<LineRow;rows++){
   		for(cols = 0; cols<numcols;cols++){
	   		// set dielectric in the substrate
	   		Epsilon[rows][cols] = Er; 
	   	}
   }
   // setup the channel 
   for(rows = 0; rows<ChannelH;rows++){
   		for(cols = numcols/2-ChannelW/2; cols<numcols/2+ChannelW/2;cols++){
	   		// set dielectric in the substrate
	   		Epsilon[rows][cols] = ChannelEr; 
	   	}
   }
   // update the list of start and end points 
   // fist set it all to zero 
   for(int rows=0; rows<4;rows++){
	   	for(int cols =0; cols<25; cols++){
	   		Boundaries[rows][cols]=0; 
	   	}
   }

 
   float Z; 
   int WaitVal; 

   threadStartup();

	cout<< "Opening Multiple Threads: "<<numThreads<<endl; 
    for( int i =0; i<numThreads;i++){

   		cout<< "Starting Thread: "<< i<<endl; 
	t[i]= thread(Worker,i);
	}
  

   // this part does the status and waits for each thread to finnish 

	for(int i=0;i<4;i++){
		t[i].join();
		cout<<"Joined "<<i<<endl; 

   	}
   	cout<<"Done all threads for this iteration"<<endl;  
 
	
	Z = getZ(); 

	cout<<" The Impedance is: "<<Z<<" for a line width of "<<LineWidth<<" thou"<<endl; 

	cout<<"Saving the potential matrix"<<endl; 

	ofstream file1; 
	file1.open("MuStripAirV.csv");
	ofstream file2;
	file2.open("MuStripSubV.csv");

	for(rows =0;rows<numrows;rows++){
	   	for(cols=0;cols<numcols; cols++){
	   		// set the guys to zero 
	   		file1<<V[rows][cols] ;
	   		file2<<VSub[rows][cols];
	   		file1<<","; // comma for CSV
	   		file2<<","; // comma for CSV 
	   	}
	   	file1<<"\n"; // new line at end of csv 
	   	file2<<"\n"; //ditto 
   }



	file1.close(); 
	file2.close(); 	

	ofstream file3;
	file3.open("MuStripResults.txt");
	file3<<"Final Value of Impedance was: "<<Z<<endl;
	file3.close();

	cout<<"Final Impedance was "<<Z<<" Ohms for Channel Width of "<<ChannelW<<endl; 
	cout<<"The fractional Phase Velocity was: "<<VP<<endl;
}
