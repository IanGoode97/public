clear all
close all 
clc
%%
% ELEC 854 Controur Plotting
% Ian Goode 
% February 6, 2018 

% a) Microstip 

MuStripAir = csvread('MuStripAirV.csv'); 
MuStripSub = csvread('MuStripSubV.csv'); 

figure(1)
contour(MuStripAir,100)
title('Microstrip Line, Potentials without Substrate')

figure(2)
contour(MuStripSub,100)
title('Microstrip Line, Potentials with Substrate')

%%
% b) StripLine
StripAir = csvread('StripLineAirV.csv');
StripSub = csvread('StripLineSubV.csv');

figure(3)
contour(StripAir,100)
title('Strip Line, Potentials without Substrate')

figure(4)
contour(StripSub,100)
title('StripLine, Potentials with Substrate')

%%
% c) Grounded Co-Planar Wave Guide

GCPWAir = csvread('GCPWAirV.csv'); 
GCPWSub = csvread('GCPWSubV.csv'); 

figure(5)
contour(GCPWAir,100)
title('GCPW, Potentials without Substrate')

figure(6)
contour(GCPWSub,100)
title('GCPW, Potentials with Substrate')

%%
% d) 1 
%       Coupled Lines, Both Lines are the Same Width 

d1AirEven = csvread('CoupledAirVEven.csv');
d1AirOdd = csvread('CoupledAirVOdd.csv'); 
d1SubEven = csvread('CoupledSubVEven.csv');
d1SubOdd = csvread('CoupledSubVOdd.csv'); 

figure(7)
contour(d1AirEven,100)
title('Same Width Coupled Lines, without Substrate, Even Mode')

figure(8)
contour(d1AirOdd,100)
title('Same Width Coupled Lines, without Substrate, Odd Mode')

figure(9)
contour(d1SubEven, 100)
title('Same Width Coupled Lines, with Substrate, Even Mode')

figure(10)
contour(d1SubOdd,100)
title('Same Width Coupled Lines, with Substrate, Odd Mode')





%%
% d) 2
%       Coupled Lines, Lines are Differnt Width 

d2AirEven = csvread('CoupledAirVEven2.csv');
d2AirOdd = csvread('CoupledAirVOdd2.csv'); 
d2SubEven = csvread('CoupledSubVEven2.csv');
d2SubOdd = csvread('CoupledSubVOdd2.csv'); 

figure(11)
contour(d2AirEven,100)
title('Different Width Coupled Lines, without Substrate, Even Mode')

figure(12)
contour(d2AirOdd,100)
title('Differnt Width Coupled Lines, without Substrate, Odd Mode')

figure(13)
contour(d2SubEven, 100)
title('Differnt Width Coupled Lines, with Substrate, Even Mode')

figure(14)
contour(d2SubOdd,100)
title('Differnt Width Coupled Lines, with Substrate, Odd Mode')
